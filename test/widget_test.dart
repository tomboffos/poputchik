// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:poputchik/cubit/app/cubit/app_cubit.dart';
import 'package:poputchik/cubit/chat/cubit/chat_cubit.dart';
import 'package:poputchik/cubit/driver/cubit/driver_cubit.dart';
import 'package:poputchik/cubit/help_phone/cubit/help_phone_cubit.dart';
import 'package:poputchik/cubit/order/cubit/order_cubit.dart';
import 'package:poputchik/cubit/track/cubit/track_cubit.dart';
import 'package:poputchik/cubit/user/cubit/user_cubit.dart';
import 'package:poputchik/data/network_service.dart';
import 'package:poputchik/data/repository.dart';
import 'package:poputchik/presentation/screens/driver/map/index.dart';

void main() {
  testWidgets("foo", (tester) async {
    tester.binding.window.physicalSizeTestValue = Size(42, 42);

    // resets the screen to its original size after the test end
    addTearDown(tester.binding.window.clearPhysicalSizeTestValue);
    Repository repository = Repository(networkService: NetworkService());
    await tester.pumpWidget(MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => HelpPhoneCubit(repository)),
        BlocProvider(create: (context) => UserCubit(repository: repository)),
        BlocProvider(create: (context) => DriverCubit(repository)),
        BlocProvider(create: (context) => OrderCubit(repository)),
        BlocProvider(create: (context) => AppCubit()),
        BlocProvider(create: (context) => ChatCubit(repository)),
        BlocProvider(
          create: (context) => TrackCubit(repository),
        )
      ],
      child: DriverMapIndex(),
    ));
  });
}
