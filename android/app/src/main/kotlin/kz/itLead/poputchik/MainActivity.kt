package kz.itLead.poputchik

import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugins.GeneratedPluginRegistrant
import com.yandex.mapkit.MapKitFactory
import androidx.annotation.NonNull


class MainActivity: FlutterActivity() {
    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
     // Your preferred language. Not required, defaults to system language
    MapKitFactory.setApiKey("a2a57c80-4e44-40ae-9dcb-ad2241c3acd1") // Your generated API key
    super.configureFlutterEngine(flutterEngine)  
  }
}
