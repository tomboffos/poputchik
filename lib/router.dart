import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:poputchik/cubit/driver/cubit/driver_cubit.dart';
import 'package:poputchik/cubit/help_phone/cubit/help_phone_cubit.dart';
import 'package:poputchik/cubit/order/cubit/order_cubit.dart';
import 'package:poputchik/cubit/order_request/cubit/order_request_cubit.dart';
import 'package:poputchik/cubit/track/cubit/track_cubit.dart';
import 'package:poputchik/cubit/user/cubit/user_cubit.dart';
import 'package:poputchik/data/network_service.dart';
import 'package:poputchik/data/repository.dart';
import 'package:poputchik/presentation/screens/auth/check.dart';
import 'package:poputchik/presentation/screens/driver/auth/register.dart';
import 'package:poputchik/presentation/screens/driver/auth/register_second.dart';
import 'package:poputchik/presentation/screens/driver/balance/index.dart';
import 'package:poputchik/presentation/screens/driver/map/index.dart';
import 'package:poputchik/presentation/screens/start/loading.dart';
import 'package:poputchik/presentation/screens/start/security.dart';
import 'package:poputchik/presentation/screens/start/start.dart';
import 'package:poputchik/presentation/screens/track/index.dart';
import 'package:poputchik/presentation/screens/user/auth/become_driver.dart';
import 'package:poputchik/presentation/screens/user/auth/login.dart';
import 'package:poputchik/presentation/screens/user/auth/register.dart';
import 'package:poputchik/presentation/screens/user/auth/set_phone.dart';
import 'package:poputchik/presentation/screens/user/home/index.dart';
import 'package:poputchik/presentation/screens/user/home/map.dart';
import 'package:poputchik/presentation/screens/user/orders/index.dart';
import 'package:poputchik/presentation/screens/user/profile/edit.dart';
import 'package:poputchik/presentation/screens/user/profile/new_profile.dart';
import 'package:poputchik/presentation/webview_payment.dart';

import 'cubit/chat/cubit/chat_cubit.dart';
import 'cubit/order_user/cubit/order_user_cubit.dart';
import 'presentation/screens/driver/orders/card_drivers.dart';
import 'presentation/screens/user/orders/carduser.dart';

class AppRouter {
  Repository repository;

  AppRouter() {
    repository = new Repository(networkService: new NetworkService());
  }

  Route generateRouter(RouteSettings settings) {
    final arguments = settings.arguments;

    switch (settings.name) {
      case '/':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) => UserCubit(repository: repository),
                  child: Loading(),
                ));
      case '/start':
        return MaterialPageRoute(builder: (_) => StartScreen());
      case '/user/register':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) => UserCubit(repository: repository),
                  child: RegisterPage(),
                ));
      case '/user/login':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) => UserCubit(repository: repository),
                  child: LoginPage(
                    driver: false,
                  ),
                ));
      case '/driver/login':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) => UserCubit(repository: repository),
                  child: LoginPage(
                    driver: true,
                  ),
                ));
      case '/driver/check':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) => UserCubit(repository: repository),
                  child: CheckCode(
                    driver: true,
                    user: arguments,
                  ),
                ));
      case '/user/check':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) => UserCubit(repository: repository),
                  child: CheckCode(
                    driver: false,
                    user: arguments,
                  ),
                ));
      case '/user/home':
        return MaterialPageRoute(
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider(
                      create: (context) => UserCubit(repository: repository)),
                  BlocProvider(create: (context) => HelpPhoneCubit(repository)),
                  BlocProvider(
                      create: (context) =>
                          OrderRequestCubit(repository: repository)),
                  BlocProvider(create: (context) => ChatCubit(repository)),
                  BlocProvider(create: (context) => OrderUserCubit(repository)),
                  BlocProvider(
                    create: (context) => TrackCubit(repository),
                  )
                ], child: UserMapIndex()));
      case '/user/orders':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) =>
                      OrderRequestCubit(repository: repository),
                  child: UserOrderHistory(
                    driver: false,
                  ),
                ));
      case '/driver/orders':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) => OrderCubit(repository),
                  child: UserOrderHistory(
                    driver: true,
                  ),
                ));
      case '/user/edit':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) => UserCubit(repository: repository),
                  child: ProfileEdit(),
                ));
      case '/tracks':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) => TrackCubit(repository),
                  child: TrackIndex(),
                ));
      case '/security':
        return MaterialPageRoute(builder: (_) => SecurityScreen());
      case '/driver/register':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) => DriverCubit(repository),
                  child: DriverRegisterPage(),
                ));
      case '/driver/map/index':
        return MaterialPageRoute(
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider(create: (context) => HelpPhoneCubit(repository)),
                  BlocProvider(
                      create: (context) => UserCubit(repository: repository)),
                  BlocProvider(create: (context) => DriverCubit(repository)),
                  BlocProvider(create: (context) => OrderCubit(repository)),
                  BlocProvider(create: (context) => ChatCubit(repository)),
                  BlocProvider(
                    create: (context) => TrackCubit(repository),
                  )
                ], child: DriverMapIndex()));
      case '/driver/balance':
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(providers: [
            BlocProvider(create: (context) => DriverCubit(repository)),
            BlocProvider(
              create: (context) => UserCubit(repository: repository),
            )
          ], child: BalanceIndex()),
        );
      case '/type/phone/forget':
        return MaterialPageRoute(builder: (_) => RequestSmsPhone());
      case '/payment':
        return MaterialPageRoute(
            builder: (_) => WebViewPayment(
                  link: arguments,
                ));
      case '/become/driver':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) => DriverCubit(repository),
                  child: BecomeDriver(),
                ));
      case '/final/point/choose':
        return MaterialPageRoute(builder: (_) => OrdrMapPointChoooseScreen());
      case '/history/user/order':
        return MaterialPageRoute(builder: (_) => UserOrders());
      case '/history/driver/order':
        return MaterialPageRoute(builder: (_) => CardDriver());
      case '/driver/register2':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) => DriverCubit(repository),
                  child: DriverRegisterPage2(),
                ));
      case '/user/newprofile':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) => UserCubit(repository: repository),
                  child: NewProfileUser(),
                ));
      default:
        return null;
    }
  }
}
