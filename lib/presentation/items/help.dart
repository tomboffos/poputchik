import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poputchik/data/models/help_phone.dart';
import 'package:url_launcher/url_launcher.dart';

class HelpItem extends StatelessWidget {
  final HelpPhone helpPhone;
  const HelpItem({Key key, this.helpPhone}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => launch('tel:${helpPhone.phone}'),
      child: Row(
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 5, horizontal: 6),
            child: Icon(Icons.phone_outlined),
            decoration: BoxDecoration(
                color: Color(0xffBCFF7A),
                borderRadius: BorderRadius.circular(100)),
          ),
          SizedBox(
            width: 10,
          ),
          Text('${helpPhone.phone} - ${helpPhone.name}',
              style: GoogleFonts.montserrat(
                  fontSize: 15, fontWeight: FontWeight.w400))
        ],
      ),
    );
  }
}
