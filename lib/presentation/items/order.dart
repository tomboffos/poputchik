import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poputchik/data/models/order_request.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class OrderItem extends StatelessWidget {
  final OrderRequest orderRequest;
  const OrderItem({Key key, this.orderRequest}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 4),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                margin: EdgeInsets.only(bottom: 9),
                child: CircleAvatar(
                  minRadius: 24,
                  foregroundImage: NetworkImage(
                      'https://img2.freepng.ru/20180920/yko/kisspng-computer-icons-portable-network-graphics-avatar-ic-5ba3c66df14d32.3051789815374598219884.jpg'),
                ),
              ),
              Column(
                children: [
                  Text('Имя водителя',
                      style: GoogleFonts.montserrat(
                          fontSize: 16, fontWeight: FontWeight.w500)),
                  SmoothStarRating(
                    color: Colors.yellow,
                    borderColor: Colors.yellow,
                    isReadOnly: false,
                    size: 20,
                  ),
                  Text(
                    '${DateTime.parse(orderRequest.createdAt)}',
                    style: GoogleFonts.montserrat(
                        fontSize: 10,
                        fontWeight: FontWeight.w500,
                        color: Color(0xffBBBBBBB)),
                  )
                ],
                crossAxisAlignment: CrossAxisAlignment.start,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '${orderRequest.price} тг',
                    style: GoogleFonts.montserrat(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff313131)),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    '${orderRequest.track.startName} - ${orderRequest.track.endName}',
                    style: GoogleFonts.montserrat(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff313131)),
                  )
                ],
              )
            ],
          ),
          Divider(
            color: Color(0xff313131),
          )
        ],
      ),
    );
  }
}
