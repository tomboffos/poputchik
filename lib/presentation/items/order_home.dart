import 'package:flutter/material.dart';
import 'package:poputchik/data/constant.dart';
import 'package:poputchik/data/models/order_request.dart';

class OrderHome extends StatelessWidget {
  final OrderRequest orderRequest;
  const OrderHome({Key key, this.orderRequest}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      margin: EdgeInsets.only(right: 10),
      decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [primaryColor, secondColor],
          ),
          // border: Border.all(color: Colors.black),
          borderRadius: BorderRadius.circular(10)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            constraints: BoxConstraints(maxWidth: 140),
            child: Text(
              '${orderRequest.track.startName} - ${orderRequest.track.endName}',
              style: TextStyle(fontSize: 15, color: Colors.white),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                '${orderRequest.price} тг',
                textAlign: TextAlign.end,
                style: TextStyle(color: Colors.white),
              )
            ],
          )
        ],
      ),
    );
  }
}
