import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:poputchik/cubit/track/cubit/track_cubit.dart';

class TrackIndex extends StatefulWidget {
  TrackIndex({Key key}) : super(key: key);

  @override
  State<TrackIndex> createState() => _TrackIndexState();
}

class _TrackIndexState extends State<TrackIndex> {
  TextEditingController searchController = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<TrackCubit>(context).getTracks(query: '');
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        toolbarHeight: 60.0,
        iconTheme: IconThemeData(color: Colors.black),
        title: TextField(
          cursorColor: Colors.black,
          controller: searchController,
          decoration: InputDecoration(
              hintText: "Введите маршрут",
              border: InputBorder.none,
              suffixIcon: IconButton(
                icon: Icon(Icons.search),
                color: Color.fromRGBO(93, 25, 72, 1),
                onPressed: () => BlocProvider.of<TrackCubit>(context)
                    .getTracks(query: searchController.text.toString()),
              )),
          style:
              TextStyle(color: Colors.black.withOpacity(0.7), fontSize: 20.0),
        ),
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: BlocBuilder<TrackCubit, TrackState>(
          builder: (context, state) {
            if (state is TrackInitial) return SizedBox();
            final tracks = (state as TrackLoaded).tracks;
            return ListView.builder(
              itemBuilder: (context, index) => GestureDetector(
                onTap: () => Navigator.pop(context, tracks[index]),
                child: Container(
                  decoration:
                      BoxDecoration(border: Border.all(color: Colors.black)),
                  child: ListTile(
                    trailing:
                        Text('Стартовая цена: ${tracks[index].startPrice} тг'),
                    title: Text(
                        '${tracks[index].startName} - ${tracks[index].endName}'),
                  ),
                ),
              ),
              itemCount: tracks.length,
            );
          },
        ),
      ),
    );
  }
}
