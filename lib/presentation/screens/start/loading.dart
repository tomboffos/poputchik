import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poputchik/cubit/user/cubit/user_cubit.dart';
import 'package:poputchik/data/constant.dart';

class Loading extends StatefulWidget {
  Loading({Key key}) : super(key: key);

  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {
  bool loading = true;
  Timer _timer;
  Timer _secondTimer;
  @override
  void initState() {
    super.initState();
    BlocProvider.of<UserCubit>(context).checkUser(context: context);
    _timer = Timer.periodic(Duration(milliseconds: 500), (timer) {
      setState(() {
        loading = !loading;
      });
    });

    // _secondTimer = Timer(
    //     Duration(seconds: 3),
    //     () => Navigator.pushNamedAndRemoveUntil(
    //         context, '/start', (route) => false));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AnimatedContainer(
                  decoration: BoxDecoration(
                      color: primaryColor,
                      borderRadius: BorderRadius.circular(15)),
                  duration: Duration(milliseconds: 500),
                  width: 3,
                  height: loading ? 50 : 40,
                ),
                AnimatedContainer(
                  margin: EdgeInsets.symmetric(horizontal: 2),
                  decoration: BoxDecoration(
                      color: secondColor,
                      borderRadius: BorderRadius.circular(15)),
                  duration: Duration(milliseconds: 500),
                  width: 3,
                  height: !loading ? 50 : 40,
                ),
                AnimatedContainer(
                  decoration: BoxDecoration(
                      color: primaryColor,
                      borderRadius: BorderRadius.circular(15)),
                  duration: Duration(milliseconds: 500),
                  width: 3,
                  height: loading ? 50 : 40,
                ),
              ],
            ),
            Container(
              child: Text('Загрузка',
                  style: GoogleFonts.montserrat(
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                      color: Colors.black)),
              margin: EdgeInsets.only(top: 10),
            )
          ],
        ),
      ),
    );
  }
}
