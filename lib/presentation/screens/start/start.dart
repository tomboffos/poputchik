import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poputchik/data/constant.dart';
import 'package:poputchik/presentation/components/main_button.dart';
import 'package:poputchik/presentation/components/main_gradient_button.dart';

class StartScreen extends StatelessWidget {
  const StartScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(

        decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(
              'assets/images/background.jpg'),
          fit: BoxFit.fill,
        ),
      ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text('Я хочу зайти как',
                  style: GoogleFonts.montserrat(
                    fontSize: 20,
                    fontWeight: FontWeight.w700,
                    color: Colors.white
                  )),
              SizedBox(
                height: 30,
              ),
              MainButton(
                
                  action: () => Navigator.pushNamed(context, '/user/register'),
                  color: Colors.white,
                 side: BorderSide(color:Color.fromRGBO(45, 139, 226, 1)),
                  radius: 8.0,
                  width: MediaQuery.of(context).size.width -40,
                  child: Text('Пассажир',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.montserrat(
                          fontSize: 15,
                          fontWeight: FontWeight.w500,
                          color: Color.fromRGBO(45, 139, 226, 1))),
               ),
               SizedBox(
                height: 30,
              ),
              MainButton(
                  action: () => Navigator.pushNamed(context, '/driver/register2'),
                  color: Colors.white,
                 side: BorderSide(color:Color.fromRGBO(45, 139, 226, 1)),
                  radius: 8.0,
                  width: MediaQuery.of(context).size.width - 40,
                  child: Text('Водитель',
                      style: GoogleFonts.montserrat(
                        
                          fontSize: 15, fontWeight: FontWeight.w500,color: Color.fromRGBO(45, 139, 226, 1)),
                          
                          ),),
            ],
          ),
        ),
      ),
    );
  }
}
