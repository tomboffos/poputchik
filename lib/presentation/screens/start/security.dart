import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poputchik/cubit/app/cubit/app_cubit.dart';

class SecurityScreen extends StatelessWidget {
  const SecurityScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final appCubit = BlocProvider.of<AppCubit>(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(237, 244, 255, 1),
        iconTheme: IconThemeData(
          color: Color.fromRGBO(88, 88, 88, 1), //change your color here
        ),
        elevation: 0,
        title: Text(
          appCubit.getWord('security'),
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
         
          padding: EdgeInsets.only(top: 60),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/background.jpg'),
              fit: BoxFit.cover,
            ),),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 45),
                child: Text(
                  'Для современного мира перспективное планирование способствует подготовке и реализации новых принципов формирования материально-технической и кадровой базы. Предварительные выводы неутешительны: граница обучения кадров создаёт необходимость включения в производственный план целого ряда внеочередных мероприятий с учётом комплекса инновационных методов управления процессами.',
                  style: GoogleFonts.montserrat(fontSize: 20,color: Colors.white),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
