import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poputchik/cubit/app/cubit/app_cubit.dart';
import 'package:poputchik/cubit/user/cubit/user_cubit.dart';
import 'package:poputchik/data/constant.dart';
import 'package:poputchik/presentation/components/icon_menu.dart';

class NewProfileUser extends StatefulWidget {
  const NewProfileUser({Key key}) : super(key: key);

  @override
  State<NewProfileUser> createState() => _NewProfileUserState();
}

class _NewProfileUserState extends State<NewProfileUser> {
  @override
  Widget build(BuildContext context) {
    final appCubit = BlocProvider.of<AppCubit>(context);
    BlocProvider.of<UserCubit>(context).getUser();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(237, 244, 255, 1),
        iconTheme: IconThemeData(
          color: Color.fromRGBO(88, 88, 88, 1), //change your color here
        ),
        elevation: 0,
      ),
      body: BlocBuilder<UserCubit, UserState>(
        builder: (context, state) {
          if (state is UserLoaded)
            return Column(
              children: [
                Container(
                  color: Color.fromRGBO(237, 244, 255, 1),
                  width: MediaQuery.of(context).size.width,
                  height: 150,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      children: [
                        CircleAvatar(
                          radius: 50,
                          foregroundImage: NetworkImage((state).user.avatar !=
                                  null
                              ? (state).user.avatar
                              : 'https://img2.freepng.ru/20180920/yko/kisspng-computer-icons-portable-network-graphics-avatar-ic-5ba3c66df14d32.3051789815374598219884.jpg'),
                        ),
                        SizedBox(
                          width: 25,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text('${state.user.name}',
                                style: GoogleFonts.montserrat(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                )),
                            SizedBox(
                              height: 8,
                            ),
                            Row(
                              children: [
                                Icon(
                                  Icons.star,
                                  color: starColor,
                                  size: 20,
                                ),
                                SizedBox(
                                  width: 2,
                                ),
                                Text(
                                  state.user.rate ?? '5.0',
                                  style: GoogleFonts.montserrat(
                                      fontSize: 12, color: greyText),
                                )
                              ],
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 35,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: IconMenu(
                    action: () => Navigator.pushNamed(context, '/user/edit'),
                    icon: SvgPicture.asset('assets/images/Profile.svg'),
                    text: appCubit.getWord('owner_data'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Divider(
                    color: dividerColor,
                    thickness: 1.0,
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: IconMenu(
                    action: () {},
                    icon: SvgPicture.asset('assets/images/Lock.svg'),
                    text: appCubit.getWord('change_password'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Divider(
                    color: dividerColor,
                    thickness: 1.0,
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: IconMenu(
                    action: () => BlocProvider.of<UserCubit>(context)
                        .logout(context: context),
                    icon: SvgPicture.asset('assets/images/Logout.svg'),
                    text: appCubit.getWord('exit'),
                  ),
                ),
              ],
            );
          return SizedBox.shrink();
        },
      ),
    );
  }
}
