import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:images_picker/images_picker.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:poputchik/cubit/app/cubit/app_cubit.dart';
import 'package:poputchik/cubit/user/cubit/user_cubit.dart';
import 'package:poputchik/presentation/components/icon_menu.dart';
import 'package:poputchik/presentation/components/main_button.dart';
import 'package:poputchik/presentation/components/main_field.dart';

class ProfileEdit extends StatefulWidget {
  const ProfileEdit({Key key}) : super(key: key);

  @override
  _ProfileEditState createState() => _ProfileEditState();
}

class _ProfileEditState extends State<ProfileEdit> {
  bool notification = false;
  var maskFormatter = new MaskTextInputFormatter(
      mask: '+# (###) ###-##-##',
      filter: {"#": RegExp(r'[0-9]')},
      type: MaskAutoCompletionType.lazy);
  TextEditingController phone = new TextEditingController();
  TextEditingController carModel = new TextEditingController();
  TextEditingController carColor = new TextEditingController();

  chooseImage() async {
    final res = await ImagesPicker.pick(count: 1);

    BlocProvider.of<UserCubit>(context)
        .updateUser(form: {'avatar': res[0].path});
  }

  @override
  Widget build(BuildContext context) {
    final appCubit = BlocProvider.of<AppCubit>(context);
    BlocProvider.of<UserCubit>(context).getUser();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(237, 244, 255, 1),
        iconTheme: IconThemeData(
          color: Color.fromRGBO(88, 88, 88, 1), //change your color here
        ),
        elevation: 0,
        title: Text(
          'Мой профиль',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: SingleChildScrollView(
        physics: NeverScrollableScrollPhysics(),
        child: Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/background.jpg'),
              fit: BoxFit.cover,
            ),
          ),
          child: BlocConsumer<UserCubit, UserState>(
            listener: (context, state) {
              if (state is UserLoaded) {
                phone.text = state.user.phone;

                if (state.user.driver != null) {
                  carColor.text = state.user.driver.carColor;
                  carModel.text = state.user.driver.carModel;
                }
              }
            },
            builder: (context, state) {
              if (state is UserInitial) return SizedBox.shrink();
              if (state is UserLoading)
                return Center(
                  child: CircularProgressIndicator(),
                );
              return Container(
                margin: EdgeInsets.symmetric(horizontal: 25, vertical: 30),
                child: Column(
                  children: [
                    Row(
                      children: [
                        GestureDetector(
                          onTap: () => chooseImage(),
                          child: CircleAvatar(
                            minRadius: 24,
                            foregroundImage: NetworkImage((state as UserLoaded)
                                        .user
                                        .avatar !=
                                    null
                                ? '${(state as UserLoaded).user.avatar}'
                                : 'https://img2.freepng.ru/20180920/yko/kisspng-computer-icons-portable-network-graphics-avatar-ic-5ba3c66df14d32.3051789815374598219884.jpg'),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          '${(state as UserLoaded).user.name}',
                          style: GoogleFonts.montserrat(
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              color: Colors.white),
                        )
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 21),
                      child: Column(
                        children: [
                          MainTextField(
                            inputFormatter: [maskFormatter],
                            controller: phone,
                            text: appCubit.getWord('type_phone'),
                          ),
                          (state as UserLoaded).user.driver != null
                              ? MainTextField(
                                  controller: carModel,
                                  text: appCubit.getWord('type_car_model'),
                                )
                              : SizedBox.shrink(),
                          // (state as UserLoaded).user.driver != null
                          //     ? MainTextField(
                          //         controller: carColor,
                          //         text: 'Введите цвет машины',
                          //       )
                          //     : SizedBox.shrink()
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 8),
                            child: Text(
                              appCubit.getWord('disable_notification'),
                              style: GoogleFonts.montserrat(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.white),
                            ),
                          ),
                          Transform.scale(
                            scale: 0.9,
                            child: CupertinoSwitch(
                              value: (state as UserLoaded)
                                  .user
                                  .notificationEnabled,
                              onChanged: (value) {
                                BlocProvider.of<UserCubit>(context).updateUser(
                                    form: {
                                      'notification_enabled': value.toString()
                                    });
                              },
                              trackColor: Colors.red,
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    DropdownButton(
                      dropdownColor: Colors.black,
                      style: TextStyle(color: Colors.black),
                      iconEnabledColor: Colors.white,
                      items: [
                        DropdownMenuItem(
                          child: Text(
                            'Русский',
                            style: TextStyle(color: Colors.white),
                          ),
                          value: 'ru',
                        ),
                        DropdownMenuItem(
                          child: Text(
                            'Казахский',
                            style: TextStyle(color: Colors.white),
                          ),
                          value: 'kz',
                        ),
                      ],
                      onChanged: (value) => {
                        BlocProvider.of<AppCubit>(context).changeLang(value)
                      },
                      isExpanded: true,
                      value: BlocProvider.of<AppCubit>(context).getLang(),
                    ),
                   
                    SizedBox(
                      height: 90,
                    ),
                    MainButton(
                      action: () => BlocProvider.of<UserCubit>(context)
                          .updateUser(
                              form: {'phone': phone.text}, context: context),
                      side: BorderSide(color: Color.fromRGBO(45, 139, 226, 1)),
                      radius: 8.0,
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                        appCubit.getWord('update'),
                        style: GoogleFonts.montserrat(
                            color: Colors.white,
                            fontSize: 15,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    MainButton(
                      action: () => BlocProvider.of<UserCubit>(context)
                            .logout(context: context),
                      side: BorderSide(color: Colors.red),
                      
                      radius: 8.0,
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                       appCubit.getWord(
                          'exit'),
                        style: GoogleFonts.montserrat(
                            color: Colors.red,
                            fontSize: 15,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
