import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:poputchik/cubit/app/cubit/app_cubit.dart';
import 'package:poputchik/data/models/track.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class OrdrMapPointChoooseScreen extends StatefulWidget {
  final Track track;
  const OrdrMapPointChoooseScreen({Key key, this.track}) : super(key: key);

  @override
  State<OrdrMapPointChoooseScreen> createState() =>
      _OrdrMapPointChoooseScreenState();
}

class _OrdrMapPointChoooseScreenState extends State<OrdrMapPointChoooseScreen> {
  List<MapObject> mapObjects = [];

  dynamic casePoint;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final appCubit = BlocProvider.of<AppCubit>(context);
    return Scaffold(
      body: Stack(
        children: [
          YandexMap(
            mapObjects: mapObjects,
            onMapCreated: (YandexMapController controller) async {
              int counter = 0;
              widget.track.points.forEach((element) {
                setState(() {
                  mapObjects.add(
                    PlacemarkMapObject(
                      opacity: 1,
                      icon: PlacemarkIcon.single(PlacemarkIconStyle(
                          image: BitmapDescriptor.fromAssetImage(
                              'assets/images/ellipse.png'))),
                      point: Point(
                        latitude: element[0],
                        longitude: element[1],
                      ),
                      mapId: MapObjectId('track_final_points_$counter'),
                      onTap: (mapObject, point) {
                        if (casePoint == null) {
                          setState(() {
                            casePoint = element;
                          });
                          Navigator.pop(context, element);
                        }
                      },
                    ),
                  );
                  counter += 1;
                });
              });
              Future.delayed(Duration(milliseconds: 500), () {
                controller.moveCamera(
                    CameraUpdate.newCameraPosition(CameraPosition(
                        zoom: 16,
                        target: Point(
                            latitude: widget.track.points.last[0],
                            longitude: widget.track.points.last[1]))),
                    animation: MapAnimation());
              });
            },
          ),
          Positioned(
            left: 20,
            top: 70,
            child: Center(
              child: GestureDetector(
                onTap: () => Navigator.pop(context),
                child: Container(
                    constraints: BoxConstraints(maxWidth: 200),
                    child: Icon(
                      Icons.arrow_back,
                      size: 40,
                    )),
              ),
            ),
          ),
          Positioned(
            left: MediaQuery.of(context).size.width / 3.5,
            top: 70,
            child: Center(
              child: Container(
                constraints: BoxConstraints(maxWidth: 200),
                child: Text(
                  appCubit.getWord('choose_destination'),
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
