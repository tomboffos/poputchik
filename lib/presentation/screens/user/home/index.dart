import 'dart:async';
import 'dart:convert';
import 'dart:developer' as dev;
import 'dart:math';

import 'package:adhara_socket_io/adhara_socket_io.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:location/location.dart';
import 'package:poputchik/cubit/app/cubit/app_cubit.dart';
import 'package:poputchik/cubit/chat/cubit/chat_cubit.dart';
import 'package:poputchik/cubit/help_phone/cubit/help_phone_cubit.dart';
import 'package:poputchik/cubit/order_request/cubit/order_request_cubit.dart';
import 'package:poputchik/cubit/order_user/cubit/order_user_cubit.dart';
import 'package:poputchik/cubit/track/cubit/track_cubit.dart';
import 'package:poputchik/cubit/user/cubit/user_cubit.dart';
import 'package:poputchik/data/constant.dart';
import 'package:poputchik/data/network_service.dart';
import 'package:poputchik/presentation/components/chat_slide.dart';
import 'package:poputchik/presentation/components/icon_menu.dart';
import 'package:poputchik/presentation/components/order_create.dart';
import 'package:poputchik/presentation/items/help.dart';
import 'package:poputchik/presentation/items/order_home.dart';
import 'package:poputchik/presentation/screens/user/orders/carduser.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class UserMapIndex extends StatefulWidget {
  const UserMapIndex({Key key}) : super(key: key);

  @override
  _UserMapIndexState createState() => _UserMapIndexState();
}

class _UserMapIndexState extends State<UserMapIndex> {
  YandexMapController _controller;
  PanelController panelController = new PanelController();
  Location location = new Location();
  PermissionStatus _permissionGranted;

  bool showProduct = false;
  bool chatSlide = false;

  bool help = false;
  closeSlide() async {
    setState(() {
      showProduct = false;
      chatSlide = false;
      BlocProvider.of<OrderRequestCubit>(context).emit(OrderRequestInitial());
    });
    panelController.close();
  }

  openSlide() {
    setState(() {
      showProduct = true;
    });
    panelController.open();
  }

  List<StreamSubscription> streams = [];

  String addressLine;
  int customPrice;
  List<MapObject> mapObjects = [];

  LocationData _locationData;
  chooseTrack(trackMain) async {
    // final trackMain = await Navigator.pushNamed(context, '/tracks');

    BlocProvider.of<OrderRequestCubit>(context)
        .chooseTrack(trackMain, _controller, context)
        .then((value) {
      if (value['track'] != null) {
        setState(() {
          customPrice = value['price'];
        });
        setState(() {
          mapObjects.removeWhere((element) =>
              element.mapId.value == 'track' ||
              element.mapId.value == 'track_final_points');
          animate(value['direction']);
          mapObjects.add(PlacemarkMapObject(
              opacity: 1,
              icon: PlacemarkIcon.single(PlacemarkIconStyle(
                  image: BitmapDescriptor.fromAssetImage(
                      'assets/images/pointred.png'))),
              point: Point(
                latitude: value['point'][0],
                longitude: value['point'][1],
              ),
              mapId: MapObjectId('track_final_points')));

          mapObjects.add(PlacemarkMapObject(
              opacity: 1,
              icon: PlacemarkIcon.single(PlacemarkIconStyle(
                  image: BitmapDescriptor.fromAssetImage(
                      'assets/images/pointred.png'))),
              point: Point(
                latitude: value['minPoint'][0],
                longitude: value['minPoint'][1],
              ),
              mapId: MapObjectId('min_point')));
        });
        openSlide();

        @override
        Widget build(BuildContext context) {
          // TODO: implement build
          throw UnimplementedError();
        }
      } else {
        BlocProvider.of<OrderRequestCubit>(context).emit(OrderRequestInitial());
        closeSlide();
      }
    });
  }

  openChatSlide({context}) {
    Navigator.pop(context);
    setState(() {
      chatSlide = true;
      showProduct = true;
    });
    panelController.open();
  }

  dynamic currentOrderId;

  PlacemarkMapObject placemark;
  @override
  void initState() {
    BlocProvider.of<UserCubit>(context).getUser();
    BlocProvider.of<HelpPhoneCubit>(context).getHelpPhones();
    BlocProvider.of<ChatCubit>(context).linkSocket();
    BlocProvider.of<ChatCubit>(context).getMessage();
    BlocProvider.of<OrderUserCubit>(context).getOrderUsers();
    BlocProvider.of<TrackCubit>(context).getTracks(query: '');

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final appCubit = BlocProvider.of<AppCubit>(context);
    return Scaffold(
      drawerEnableOpenDragGesture: false,
      drawer: Drawer(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(16),
            bottomRight: Radius.circular(16),
          ),
        ),
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(
              top: 40,
              right: 34,
              left: 34,
            ),
            child: help
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                          setState(() => help = false);
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black),
                              borderRadius: BorderRadius.circular(100)),
                          padding: EdgeInsets.all(10),
                          child: Icon(Icons.arrow_back_ios),
                        ),
                      ),
                      SizedBox(
                        height: 53,
                      ),
                      Center(
                        child: Text(appCubit.getWord('support'),
                            style: GoogleFonts.montserrat(
                                fontSize: 16, fontWeight: FontWeight.w700)),
                      ),
                      BlocBuilder<HelpPhoneCubit, HelpPhoneState>(
                        builder: (context, state) {
                          if (!(state is HelpPhoneLoaded))
                            return SizedBox.shrink();
                          final phones = (state as HelpPhoneLoaded).helpPhones;
                          return ListView.builder(
                            itemBuilder: (context, index) => Column(
                              children: [
                                SizedBox(
                                  height: 12,
                                ),
                                HelpItem(
                                  helpPhone: phones[index],
                                ),
                              ],
                            ),
                            itemCount: phones.length,
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                          );
                        },
                      )
                    ],
                  )
                : Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 34,
                      ),
                      BlocBuilder<UserCubit, UserState>(
                        builder: (context, state) {
                          if (state is UserLoaded)
                            return Row(
                              children: [
                                CircleAvatar(
                                  radius: 25,
                                  foregroundImage: NetworkImage((state)
                                              .user
                                              .avatar !=
                                          null
                                      ? (state).user.avatar
                                      : 'https://img2.freepng.ru/20180920/yko/kisspng-computer-icons-portable-network-graphics-avatar-ic-5ba3c66df14d32.3051789815374598219884.jpg'),
                                ),
                                SizedBox(width: 10),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('${state.user.name}',
                                        style: GoogleFonts.montserrat(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600,
                                        )),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    Row(
                                      children: [
                                        Icon(
                                          Icons.star,
                                          color: starColor,
                                          size: 20,
                                        ),
                                        SizedBox(
                                          width: 2,
                                        ),
                                        Text(
                                          state.user.rate ?? '5.0',
                                          style: GoogleFonts.montserrat(
                                              fontSize: 12, color: greyText),
                                        )
                                      ],
                                    ),
                                  ],
                                )
                              ],
                            );
                          return SizedBox.shrink();
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: Divider(
                          color: dividerColor,
                          thickness: 1.0,
                        ),
                      ),
                      Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Как вам удобно оплатить?',
                                style: GoogleFonts.montserrat(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                    color: Color.fromRGBO(43, 43, 43, 1)),
                              ),
                              Text(
                                'Kaspi перевод (безнал)',
                                style: GoogleFonts.montserrat(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: Color.fromRGBO(141, 141, 141, 1)),
                              ),
                            ],
                          )),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: Divider(
                          color: dividerColor,
                          thickness: 1.0,
                        ),
                      ),
                      BlocBuilder<OrderUserCubit, OrderUserState>(
                        builder: (context, state) {
                          return state is OrderUserFetched
                              ? Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 8),
                                  child: IconMenu(
                                    action: () {
                                      Navigator.pop(context);
                                      showModalBottomSheet(
                                          context: context,
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                          ),
                                          builder: (context) => UserOrders(
                                                orderRequests: state.requests,
                                              ));
                                    },
                                    icon: SvgPicture.asset(
                                        'assets/images/document.svg'),
                                    text: appCubit.getWord('history'),
                                  ),
                                )
                              : SizedBox.shrink();
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: Divider(
                          color: dividerColor,
                          thickness: 1.0,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: IconMenu(
                          action: () =>
                              Navigator.pushNamed(context, '/user/newprofile'),
                          icon: SvgPicture.asset('assets/images/setting.svg'),
                          text: appCubit.getWord('settings'),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: Divider(
                          color: dividerColor,
                          thickness: 1.0,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: IconMenu(
                          color: greenColor,
                          action: () =>
                              Navigator.pushNamed(context, '/security'),
                          icon: SvgPicture.asset('assets/images/security.svg'),
                          text: appCubit.getWord('security'),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: Divider(
                          color: dividerColor,
                          thickness: 1.0,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: IconMenu(
                            action: () {
                              openChatSlide(context: context);
                            },
                            icon: SvgPicture.asset('assets/images/Swap.svg'),
                            text: appCubit.getWord('support_online')),
                      ),

                      ///
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: Divider(
                          color: dividerColor,
                          thickness: 1.0,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: IconMenu(
                            action: () {
                              setState(() {
                                help = true;
                              });
                              Scaffold.of(context).openDrawer();
                            },
                            icon: Icon(
                              Icons.help_outline,
                              size: 25,
                            ),
                            text: appCubit.getWord('accident_services')),
                      ),

                      ///
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: Divider(
                          color: dividerColor,
                          thickness: 1.0,
                        ),
                      ),
                      BlocBuilder<UserCubit, UserState>(
                        builder: (context, state) {
                          if (state is UserLoaded)
                            return Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 8.0),
                              child: IconMenu(
                                action: () {
                                  BlocProvider.of<UserCubit>(context)
                                      .becomeDriver(state.user, context,
                                          driverMode: true);
                                },
                                icon: SvgPicture.asset(
                                    'assets/images/Profile.svg'),
                                text: appCubit.getWord('become_driver'),
                              ),
                            );
                          return SizedBox.shrink();
                        },
                      ),
                    ],
                  ),
          ),
        ),
      ),
      body: Stack(
        children: [
          YandexMap(
            onTrafficChanged: (trafficLevel) {},
            mapObjects: mapObjects,
            onMapCreated: (YandexMapController yandexMapController) async {
              _controller = yandexMapController;
              _controller.toggleTrafficLayer(visible: true);
              _controller.getUserCameraPosition();
              _permissionGranted = await location.hasPermission();
              if (_permissionGranted == PermissionStatus.denied) {
                _permissionGranted = await location.requestPermission();
                if (_permissionGranted != PermissionStatus.granted) {
                  return;
                }
              }

              _locationData = await location.getLocation();
              var addresses = await Geocoder.local.findAddressesFromCoordinates(
                  Coordinates(_locationData.latitude, _locationData.longitude));
              setState(() {
                addressLine = addresses[0].thoroughfare;
              });

              // yandexMapController.moveCamera(
              //   CameraUpdate.newCameraPosition(
              //     CameraPosition( target: Point(
              //         latitude: _locationData.latitude,
              //         longitude: _locationData.longitude),
              //     zoom: 17,
              // ));
              _controller.moveCamera(
                  CameraUpdate.newCameraPosition(
                    CameraPosition(
                        zoom: 17,
                        target: Point(
                            latitude: _locationData.latitude,
                            longitude: _locationData.longitude)),
                  ),
                  animation: MapAnimation());

              BlocProvider.of<OrderRequestCubit>(context).locationData =
                  _locationData;

              setState(() {
                mapObjects.removeWhere(
                    (element) => element.mapId.value == 'geolocation');
                mapObjects.add(PlacemarkMapObject(
                    opacity: 1,
                    direction: _locationData.heading,
                    icon: PlacemarkIcon.single(PlacemarkIconStyle(
                        scale: 2.0,
                        rotationType: RotationType.rotate,
                        image: BitmapDescriptor.fromAssetImage(
                            'assets/images/pointred.png'))),
                    point: Point(
                        latitude: _locationData.latitude,
                        longitude: _locationData.longitude),
                    mapId: MapObjectId('geolocation')));
              });
              streams.add(location.onLocationChanged.listen((event) {
                BlocProvider.of<OrderRequestCubit>(context).locationData =
                    _locationData;

                setState(() {
                  mapObjects.removeWhere(
                      (element) => element.mapId.value == 'geolocation');

                  mapObjects.add(PlacemarkMapObject(
                      opacity: 1,
                      direction: event.heading,
                      icon: PlacemarkIcon.single(PlacemarkIconStyle(
                          scale: 2.0,
                          rotationType: RotationType.rotate,
                          image: BitmapDescriptor.fromAssetImage(
                              'assets/images/pointred.png'))),
                      point: Point(
                          latitude: event.latitude, longitude: event.longitude),
                      mapId: MapObjectId('geolocation')));
                });
              }));
            },
          ),
          Positioned(
            left: MediaQuery.of(context).size.width / 2.6,
            top: 70,
            child: Center(
              child: Container(
                constraints: BoxConstraints(maxWidth: 200),
                child: Text(
                  'Выбранная трасса\n${addressLine ?? ''}',
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
          AnimatedPositioned(
            duration: Duration(milliseconds: 100),
            right: 16,
            bottom: !showProduct
                ? MediaQuery.of(context).size.height / 3.05
                : MediaQuery.of(context).size.height / 3.2,
            child: GestureDetector(
              onTap: () async {
                mapObjects.removeWhere(
                    (element) => element.mapId.value == 'geolocation');

                location.getLocation().then((value) async {
                  _controller.moveCamera(
                      CameraUpdate.newCameraPosition(CameraPosition(
                        zoom: 17,
                        target: Point(
                            latitude: value.latitude,
                            longitude: value.longitude),
                      )),
                      animation: MapAnimation());
                  var addresses = await Geocoder.local
                      .findAddressesFromCoordinates(
                          Coordinates(value.latitude, value.longitude));
                  setState(() {
                    addressLine = addresses[0].thoroughfare;
                  });
                  setState(() {
                    mapObjects.add(PlacemarkMapObject(
                        opacity: 1,
                        direction: value.heading,
                        icon: PlacemarkIcon.single(PlacemarkIconStyle(
                            scale: 2.0,
                            rotationType: RotationType.rotate,
                            image: BitmapDescriptor.fromAssetImage(
                                'assets/images/pointred.png'))),
                        point: Point(
                            latitude: value.latitude,
                            longitude: value.longitude),
                        mapId: MapObjectId('geolocation')));
                  });
                });
              },
              child: Container(
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(0, 0),
                          spreadRadius: 2.0,
                          blurRadius: 5.0,
                          color: Colors.black.withOpacity(0.2))
                    ],
                    borderRadius: BorderRadius.circular(100),
                    gradient:
                        LinearGradient(colors: [primaryColor, primaryColor])),
                child: Icon(
                  Icons.navigation_outlined,
                  color: Colors.white,
                  size: 30,
                ),
              ),
            ),
          ),
          Visibility(
            visible: false,
            child: BlocConsumer<OrderUserCubit, OrderUserState>(
              listener: (context, mainState) {
                if (mainState is OrderUserInitial) {
                  streams.forEach((element) {
                    element.cancel();
                  });
                }
              },
              builder: (context, mainState) {
                return Positioned(
                  bottom: mainState is OrderUserFetched &&
                          (mainState).requests.length > 0
                      ? 80
                      : 150,
                  left: 45,
                  right: 45,
                  child: GestureDetector(
                    // onTap: () => chooseTrack(),
                    child: Container(
                      width: MediaQuery.of(context).size.width - 90,
                      height: mainState is OrderUserFetched &&
                              (mainState).requests.length > 0
                          ? 210
                          : 110,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                                spreadRadius: 1,
                                blurRadius: 1,
                                offset: Offset(0, 0),
                                color: Colors.black.withOpacity(0.2))
                          ]),
                      alignment: Alignment.bottomCenter,
                      child: BlocBuilder<OrderRequestCubit, OrderRequestState>(
                        builder: (context, state) {
                          return Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                padding: EdgeInsets.only(
                                    top: 12, left: 12, right: 12, bottom: 6),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 5, vertical: 5),
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(100),
                                          border: Border.all(
                                              color: Colors.black, width: 4)),
                                    ),
                                    SizedBox(
                                      width: 8,
                                    ),
                                    Text(
                                        state is OrderRequestTrackChoosed
                                            ? state.track.startName
                                            : addressLine ?? '',
                                        style: GoogleFonts.montserrat(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500))
                                  ],
                                ),
                              ),
                              Divider(),
                              Container(
                                padding: EdgeInsets.only(
                                    top: 6, left: 12, right: 12, bottom: 6),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      // child: Center(
                                      //   child: Text('B',
                                      //       style: GoogleFonts.montserrat(
                                      //           fontSize: 14, color: Colors.white)),
                                      // ),
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 5, vertical: 5),
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(100),
                                          border: Border.all(
                                              color: primaryColor, width: 4)),
                                    ),
                                    SizedBox(width: 8),
                                    Text(
                                        state is OrderRequestTrackChoosed
                                            ? state.track.endName
                                            : appCubit.getWord('where'),
                                        style: GoogleFonts.montserrat(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500))
                                  ],
                                ),
                              ),
                              mainState is OrderUserFetched &&
                                      (mainState).requests.length > 0
                                  ? Container(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 10),
                                      margin: EdgeInsets.only(top: 10),
                                      height: 100,
                                      child: Row(
                                        children: [
                                          GestureDetector(
                                            onTap: () => chooseFromHistoy(
                                                mainState.requests[0].track),
                                            child: OrderHome(
                                              orderRequest:
                                                  mainState.requests[0],
                                            ),
                                          ),
                                          mainState.requests.length > 1
                                              ? GestureDetector(
                                                  onTap: () => chooseFromHistoy(
                                                      mainState
                                                          .requests[1].track),
                                                  child: OrderHome(
                                                    orderRequest:
                                                        mainState.requests[1],
                                                  ),
                                                )
                                              : SizedBox.shrink(),
                                        ],
                                      ),
                                    )
                                  : SizedBox.shrink()
                            ],
                          );
                        },
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
          Builder(
              builder: (_) => Container(
                    alignment: Alignment.topLeft,
                    margin: EdgeInsets.only(top: 60, left: 45),
                    child: GestureDetector(
                      onTap: () => Scaffold.of(_).openDrawer(),
                      child: Container(
                        padding: EdgeInsets.symmetric(
                          vertical: 11,
                          horizontal: 11,
                        ),
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                  offset: Offset(0, 0),
                                  spreadRadius: 2.0,
                                  blurRadius: 5.0,
                                  color: Colors.black.withOpacity(0.2))
                            ],
                            borderRadius: BorderRadius.circular(100),
                            gradient: LinearGradient(colors: [
                              Colors.white,
                              Colors.white,
                            ])),
                        child: Icon(
                          Icons.menu,
                          color: Colors.black,
                          size: 30,
                        ),
                      ),
                    ),
                  )),
          BlocConsumer<OrderRequestCubit, OrderRequestState>(
            listener: (context, state) async {
              if (state is OrderRequestTrackEnded) {
                showDialog(
                    context: context,
                    builder: (modalContext) => AlertDialog(
                          content: Container(
                            height: 200,
                            width: 200,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Center(
                                  child: Text(appCubit.getWord('rate_order')),
                                ),
                                SmoothStarRating(
                                  color: Colors.yellow,
                                  size: 30,
                                  borderColor: Colors.yellow,
                                  onRated: (rating) {
                                    BlocProvider.of<OrderRequestCubit>(context)
                                        .rateOrder(
                                            rating: rating.abs().toInt(),
                                            modalContext: modalContext,
                                            driverId: state.driverId);
                                    closeSlide();

                                    showDialog(
                                      context: context,
                                      builder: (newContext) => AlertDialog(
                                        content: Text(
                                            'Вы оценили на ${rating.toInt()}'),
                                      ),
                                    );

                                    Future.delayed(Duration(seconds: 1),
                                        () => Navigator.pop(context));
                                  },
                                )
                              ],
                            ),
                          ),
                        ));
              }
              if (state is OrderRequestInitial) {
                mapObjects.removeWhere(
                    (element) => element.mapId.value != 'geolocation');

                streams.forEach((element) async {
                  await element.cancel();
                });
              }
              if (state is OrderRequestCreated && state.request.order != null) {
                SocketIO socket = await SocketIOManager().createInstance(
                    SocketOptions('https://socket.it-lead.net',
                        enableLogging: false));
                setState(() {
                  currentOrderId = state.request.order.driver.id;
                });
                await socket.connectSync();
                streams.add(socket
                    .on('poputchik.order.driver.location.change.${state.request.order.id}')
                    .listen((event) {
                  dev.log('RECEIVED' + event.toString());
                  setState(() {
                    mapObjects.removeWhere(
                        (element) => element.mapId.value == 'driver_location');

                    mapObjects.add(PlacemarkMapObject(
                      opacity: 1,
                      icon: PlacemarkIcon.single(PlacemarkIconStyle(
                          scale: 1.7,
                          rotationType: RotationType.rotate,
                          image: BitmapDescriptor.fromAssetImage(
                              'assets/images/whitecar.png'))),
                      point: Point(
                        latitude: event[0]['position'][0],
                        longitude: event[0]['position'][1],
                      ),
                      mapId: MapObjectId('driver_location'),
                    ));
                  });
                }));
                streams.add(
                    location.onLocationChanged.listen((newLocationData) async {
                  await post(
                      Uri.parse('${NetworkService().apiUrl}/orders/socket'),
                      body: {
                        'event':
                            "poputchik.order.user.location.change.${(state).request.order.id}",
                        "data": jsonEncode({
                          "position": [
                            newLocationData.latitude,
                            newLocationData.longitude
                          ],
                          "id": state.request.id.toString()
                        })
                      });
                }));
              }
            },
            builder: (context, orderState) {
              return SlidingUpPanel(
                // maxHeight: MediaQuery.of(context).size.height / 1.75,

                isDraggable: orderState is! OrderRequestCreated,

                onPanelClosed: closeSlide,
                controller: panelController,
                minHeight: MediaQuery.of(context).size.height / 3.5,
                maxHeight: !(orderState is OrderRequestChoosing)
                    ? orderState is OrderRequestCreated &&
                            orderState.request.order != null
                        ? MediaQuery.of(context).size.height / 1.7
                        : orderState is OrderRequestCreated &&
                                orderState.request.order == null
                            ? MediaQuery.of(context).size.height / 2.6
                            : MediaQuery.of(context).size.height / 1.2
                    : MediaQuery.of(context).size.height / 1.3,
                // margin: EdgeInsets.symmetric(horizontal: 20),
                borderRadius: BorderRadius.circular(15),

                backdropEnabled: orderState is OrderRequestChoosing,
                panel: Visibility(
                  child: chatSlide
                      ? BlocBuilder<ChatCubit, ChatState>(
                          builder: (context, state) {
                            return ChatSlide(
                              messages:
                                  state is ChatLoaded ? state.messages : [],
                            );
                          },
                        )
                      : BlocBuilder<OrderRequestCubit, OrderRequestState>(
                          builder: (context, state) {
                            return OrderCreate(
                                // constructMap : chooseTrack()
                                panelController: panelController,
                                chooseMainHistory: chooseFromHistoy,
                                addressLine: addressLine,
                                track: state is OrderRequestTrackChoosed
                                    ? state.track
                                    : null,
                                chooseTrack: chooseTrack,
                                customPrice: customPrice,

                                // action: () => chooseTrack(),
                                controller: _controller);
                          },
                        ),
                  visible: true,
                ),
                renderPanelSheet: true,
              );
            },
          )
        ],
      ),
    );
  }

  chooseFromHistoy(mainTrack) {
    BlocProvider.of<OrderRequestCubit>(context)
        .chooseTrack(mainTrack, _controller, context)
        .then((value) {
      setState(() {
        mapObjects.removeWhere((element) =>
            element.mapId.value == 'track' ||
            element.mapId.value == 'track_final_points');
        mapObjects.add(PolylineMapObject(
            strokeColor: Colors.black,
            mapId: MapObjectId('track'),
            polyline: Polyline(
                points: value['direction']
                    .map((e) => Point(latitude: e[0], longitude: e[1]))
                    .toList()
                    .cast<Point>())));

        mapObjects.add(PlacemarkMapObject(
            opacity: 1,
            icon: PlacemarkIcon.single(PlacemarkIconStyle(
                image: BitmapDescriptor.fromAssetImage(
                    'assets/images/pointred.png'))),
            point: Point(
              latitude: value['point'][0],
              longitude: value['point'][1],
            ),
            mapId: MapObjectId('track_final_points')));
      });
    });
    openSlide();
  }

  List animate(List coords) {
    int loopTime = 5;
    int overAllTime = 250;

    List prevElem = coords[0];
    dynamic overAllDistance = 0;
    coords.forEach((element) {
      overAllDistance += getDistance(element, prevElem);

      prevElem = element;
    });

    dynamic animationInterval = overAllDistance / overAllTime * loopTime;

    List smoothCords = generateSmoothCoords(coords, animationInterval);
    int loopTimer = 0;
    int index = 0;

    Timer.periodic(Duration(milliseconds: loopTime), (timer) {
      if (index <= smoothCords.length) {
        List currentMain = smoothCords.take(index).toList();

        setState(() {
          mapObjects.removeWhere((element) => element.mapId.value == 'track');
          mapObjects.add(PolylineMapObject(
              strokeColor: Theme.of(context).accentColor,
              mapId: MapObjectId('track'),
              arcApproximationStep: 1.0,
              polyline: Polyline(
                  points: currentMain
                      .map((e) => Point(latitude: e[0], longitude: e[1]))
                      .toList()
                      .cast<Point>())));
        });
        index++;
        loopTimer += loopTime;
      } else {
        timer.cancel();
      }
    });
  }

  generateSmoothCoords(List coords, interval) {
    List smoothCords = [];
    smoothCords.add(coords[0]);

    for (int i = 1; i < coords.length; i++) {
      dynamic difference = [
        coords[i][0] - coords[i - 1][0],
        coords[i][1] - coords[i - 1][1]
      ];

      num maxAmount = max(
          (difference[0] / interval).abs(), (difference[1] / interval).abs());

      dynamic minDifference = [
        difference[0] / maxAmount,
        difference[1] / maxAmount
      ];

      dynamic lastCoord = coords[i - 1];

      while (maxAmount > 1) {
        lastCoord = [
          lastCoord[0] + minDifference[0],
          lastCoord[1] + minDifference[1]
        ];

        smoothCords.add(lastCoord);
        maxAmount--;
      }
      smoothCords.add(coords[i]);
    }

    return smoothCords;
  }

  getDistance(point1, point2) {
    return sqrt(pow(point2[0] - point1[0], 2) + pow(point2[1] - point1[1], 2));
  }

  @override
  void dispose() {
    streams.forEach((element) {
      element.cancel();
    });
    super.dispose();
  }
}
