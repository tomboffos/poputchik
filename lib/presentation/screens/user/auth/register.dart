import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:images_picker/images_picker.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:poputchik/cubit/user/cubit/user_cubit.dart';
import 'package:poputchik/data/constant.dart';
import 'package:poputchik/presentation/components/main_button.dart';
import 'package:poputchik/presentation/components/main_field.dart';
import 'package:poputchik/presentation/components/main_gradient_button.dart';
import 'package:poputchik/presentation/components/modal/camera_choose.dart';
import 'package:poputchik/presentation/components/upload_images.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController name = new TextEditingController();
  TextEditingController phone = new TextEditingController();
  TextEditingController surname = new TextEditingController();
  TextEditingController email = new TextEditingController();
  TextEditingController password = new TextEditingController();
  TextEditingController passwordConfirmation = new TextEditingController();
  bool checked = false;
  List<Media> medias = [];
  List<Media> newMedias = [];
  Media media;
  Future chooseImage({size, multiple, id}) async {
    final res = await ImagesPicker.pick(
        count: size, pickType: PickType.image, language: Language.System);

    if (res != null) {
      setState(() {
        id != null ? newMedias = res : medias = res;
      });
    }
  }

  Future takeImage({size, multiple, id}) async {
    final res = await ImagesPicker.openCamera();
    if (res != null)
      setState(() {
        id != null ? newMedias = res : medias = res;
      });
  }

  var maskFormatter = new MaskTextInputFormatter(
      mask: '+7 (###) ###-##-##',
      filter: {"#": RegExp(r'[0-9]')},
      type: MaskAutoCompletionType.lazy);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          MainButton(
            color: Colors.white,
            action: () => checked
                ? BlocProvider.of<UserCubit>(context).registerUser(form: {
                    'name': name.text,
                    'phone': phone.text,
                    'password': password.text,
                    'surname': surname.text,
                    'token': 'asd',
                    'email': email.text,
                  }, context: context)
                : {},
            side: BorderSide(color: Color.fromRGBO(45, 139, 226, 1)),
            radius: 8.0,
            width: MediaQuery.of(context).size.width - 40,
            child: Text(
              'Зарегистрироваться',
              textAlign: TextAlign.center,
              style: GoogleFonts.montserrat(
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(45, 139, 226, 1)),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Center(
            child: TextButton(
                onPressed: () => Navigator.pushNamed(context, '/user/login'),
                child: Text(
                  'Войти',
                  style: GoogleFonts.montserrat(
                      fontSize: 15,
                      fontWeight: FontWeight.w500,
                      color: Colors.white),
                )),
          ),
        ],
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/background.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: SingleChildScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          child: BlocBuilder<UserCubit, UserState>(
            builder: (context, state) {
              if (state is UserLoading)
                return Container(
                  height: MediaQuery.of(context).size.height,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Center(
                        child: CircularProgressIndicator(),
                      ),
                    ],
                  ),
                );
              return Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                constraints: BoxConstraints(
                    minHeight: MediaQuery.of(context).size.height),
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        child: Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(bottom: 13),
                              child: IconButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                icon: Icon(Icons.arrow_back_ios),
                                color: Colors.white,
                                iconSize: 18,
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Регистрация',
                                  style: GoogleFonts.montserrat(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.white),
                                ),
                                Text(
                                  'Личные данные заказчика',
                                  style: GoogleFonts.montserrat(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.white),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      MainTextField(
                        controller: name,
                        text: 'Введите имя',
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      MainTextField(
                        controller: surname,
                        text: 'Введите фамилию',
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      MainTextField(
                        inputFormatter: [maskFormatter],
                        controller: phone,
                        text: 'Введите номер',
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      MainTextField(
                        controller: email,
                        text: 'Введите почту',
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      MainTextField(
                        obscureText: true,
                        controller: password,
                        text: 'Придумайте пароль',
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      MainTextField(
                        obscureText: true,
                        controller: passwordConfirmation,
                        text: 'Повторите пароль',
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      GestureDetector(
                        onTap: () => showDialog(
                            context: context,
                            builder: (_) => AlertDialog(
                                    content: DialogCameraChoose(
                                  firstAction: () {
                                    takeImage(size: 3, multiple: true)
                                        .then((value) => Navigator.pop(_));
                                  },
                                  secondAction: () {
                                    chooseImage(size: 3, multiple: true)
                                        .then((value) => Navigator.pop(_));
                                  },
                                ))),
                        child: UploadImages(
                          medias: medias,
                          margin: EdgeInsets.only(bottom: 13),
                          text: Text(
                            'Загрузите ваше фото (необязательно)',
                            style: GoogleFonts.montserrat(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: Colors.white),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            checked = !checked;
                          });
                        },
                        child: Container(
                          margin: EdgeInsets.symmetric(horizontal: 15),
                          width: MediaQuery.of(context).size.width - 90,
                          child: Row(
                            children: [
                              Container(
                                width: 25,
                                height: 25,
                                child: checked
                                    ? Center(
                                        child: Icon(
                                          Icons.check,
                                          color: Colors.white,
                                        ),
                                      )
                                    : SizedBox.shrink(),
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(45, 161, 226, 1),
                                    border: Border.all(
                                      color: Color.fromRGBO(45, 161, 226, 1),
                                    ),
                                    borderRadius: BorderRadius.circular(5)),
                              ),
                              SizedBox(
                                width: 15,
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width / 2,
                                child: Text(
                                  'Я соглашаюсь с Политикой конфиденциальностью',
                                  style: GoogleFonts.montserrat(
                                      fontSize: 14,
                                      height: 1.5,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.white),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 120,
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
