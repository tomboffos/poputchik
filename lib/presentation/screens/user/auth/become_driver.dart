import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:images_picker/images_picker.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:poputchik/cubit/driver/cubit/driver_cubit.dart';
import 'package:poputchik/presentation/components/main_button.dart';
import 'package:poputchik/presentation/components/main_field.dart';
import 'package:poputchik/presentation/components/main_gradient_button.dart';
import 'package:poputchik/presentation/components/modal/camera_choose.dart';
import 'package:poputchik/presentation/components/upload_images.dart';

class BecomeDriver extends StatefulWidget {
  const BecomeDriver({Key key}) : super(key: key);

  @override
  State<BecomeDriver> createState() => _BecomeDriverState();
}

class _BecomeDriverState extends State<BecomeDriver> {
  TextEditingController name = new TextEditingController();
  TextEditingController surname = new TextEditingController();
  TextEditingController phone = new TextEditingController();
  TextEditingController password = new TextEditingController();
  TextEditingController car_model = new TextEditingController();
  TextEditingController car_id = new TextEditingController();
  TextEditingController car_color = new TextEditingController();
  TextEditingController password_confirmation = new TextEditingController();
  TextEditingController car_year = new TextEditingController();
  TextEditingController iin = new TextEditingController();
  TextEditingController seats = new TextEditingController();
  var maskFormatter = new MaskTextInputFormatter(
      mask: '+7 (###) ###-##-##',
      filter: {"#": RegExp(r'[0-9]')},
      type: MaskAutoCompletionType.lazy);
  List<Media> medias = [];
  List<Media> newMedias = [];
  Media media;
  chooseImage({size, multiple, id}) async {
    final res = await ImagesPicker.pick(
        count: size, pickType: PickType.image, language: Language.System);

    if (res != null) {
      setState(() {
        id != null ? newMedias = res : medias = res;
      });
    }
  }

  takeImage({size, multiple, id}) async {
    final res = await ImagesPicker.openCamera();
    if (res != null)
      setState(() {
        id != null ? newMedias = res : medias = res;
      });
  }

  bool checked = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(
              'assets/images/background.jpg'),
              fit: BoxFit.cover
          )
        ),
        child: SingleChildScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 35),
            constraints:
                BoxConstraints(minHeight: MediaQuery.of(context).size.height),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Text('Стать водителем',
                        style: GoogleFonts.montserrat(
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                           color: Colors.white),),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  MainTextField(
                    controller: car_model,
                    text: 'Введите модель машины',
                  ),
                  MainTextField(
                    controller: car_color,
                    text: 'Введите цвет машины',
                  ),
                  MainTextField(
                    controller: car_year,
                    text: 'Введите год машины',
                  ),
                  MainTextField(
                    controller: iin,
                    text: 'Введите ваш ИИН',
                  ),
                  MainTextField(
                    controller: car_id,
                    text: 'Введите гос номер машины',
                  ),
                  GestureDetector(
                    onTap: () => showDialog(
                        context: context,
                        builder: (_) => AlertDialog(
                              content: DialogCameraChoose(
                                  firstAction: () =>
                                      takeImage(size: 3, multiple: true),
                                  secondAction: () =>
                                      chooseImage(size: 3, multiple: true)),
                            )),
                    child: UploadImages(
                      medias: medias,
                      margin: EdgeInsets.only(bottom: 13),
                      text: Text(
                        'Загрузите фото водительских прав',
                        style: GoogleFonts.montserrat(
                            fontSize: 13,
                            fontWeight: FontWeight.w400,
                            color: Colors.white),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () => showDialog(
                        context: context,
                        builder: (_) => AlertDialog(
                              content: DialogCameraChoose(
                                  firstAction: () => takeImage(
                                      size: 3, multiple: true, id: true),
                                  secondAction: () => chooseImage(
                                      size: 3, multiple: true, id: true)),
                            )),
                    child: UploadImages(
                      medias: newMedias,
                      margin: EdgeInsets.only(bottom: 13),
                      text: Text(
                        'Загрузите водителя с машиной',
                        style: GoogleFonts.montserrat(
                            fontSize: 13,
                            fontWeight: FontWeight.w400,
                            color: Colors.white),
                      ),
                    ),
                  ),
                  MainTextField(
                    controller: seats,
                    text: 'Укажите количество мест в транспорте',
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        checked = !checked;
                      });
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width - 144,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                           Container(
                                    width: 25,
                                    height: 25,
                                    child: checked
                                        ? Center(
                                            child: Icon(
                                              Icons.check,
                                              color: Colors.white,
                                            ),
                                          )
                                        : SizedBox.shrink(),
                                    decoration: BoxDecoration(
                                        color: Color.fromRGBO(45, 161, 226, 1),
                                        border: Border.all(
                                          color:
                                              Color.fromRGBO(45, 161, 226, 1),
                                        ),
                                        borderRadius: BorderRadius.circular(5)),
                                  ),
                                  SizedBox(
                                    width: 15,
                                  ),
                          Container(
                            width: MediaQuery.of(context).size.width / 2,
                            child:Text(
                                      'Я соглашаюсь с Политикой конфиденциальностью',
                                      style: GoogleFonts.montserrat(
                                          fontSize: 14,
                                          height: 1.5,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.white),
                                    ),
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  MainButton(
                      action: () => checked
                          ? BlocProvider.of<DriverCubit>(context)
                              .becomDriver(form: {
                              'car_color': car_color.text,
                              'car_year': car_year.text,
                              'car_model': car_model.text,
                              'iin': iin.text,
                              'seats': seats.text,
                              'driver_id_images':
                                  medias.length != 0 ? medias : [],
                              'images': newMedias.length != 0 ? newMedias : [],
                              'car_id': car_id.text
                            }, context: context)
                          : {},
                      color: Colors.white,
                      radius: 8.0, side: BorderSide(color: Color.fromRGBO(45, 139, 226, 1)),
                      width: MediaQuery.of(context).size.width - 40,
                      child: Text('Зарегистрироваться',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.montserrat(
                              fontSize: 15,
                              fontWeight: FontWeight.w500,
                              color: Color.fromRGBO(45, 139, 226, 1))),
                      ),
                  SizedBox(
                    height: 10,
                  ),
                  MainButton(
                      action: () => Navigator.pushNamed(context, '/user/login'),
                   side: BorderSide(color:Color.fromRGBO(45, 139, 226, 1)),
                      radius: 8.0,
                      width: MediaQuery.of(context).size.width - 40,
                      child: Text('Войти',
                          textAlign: TextAlign.center,
                           style: GoogleFonts.montserrat(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500,
                                  color:  Color.fromRGBO(45, 139, 226, 1)),)),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
