import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:poputchik/cubit/user/cubit/user_cubit.dart';
import 'package:poputchik/data/constant.dart';
import 'package:poputchik/presentation/components/main_button.dart';
import 'package:poputchik/presentation/components/main_field.dart';
import 'package:poputchik/presentation/components/main_gradient_button.dart';

class LoginPage extends StatefulWidget {
  final driver;
  const LoginPage({Key key, this.driver}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController phone = new TextEditingController();
  TextEditingController password = new TextEditingController();
  var maskFormatter = new MaskTextInputFormatter(
      mask: '+7 (###) ###-##-##',
      filter: {"#": RegExp(r'[0-9]')},
      type: MaskAutoCompletionType.lazy);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    floatingActionButton: Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
         MainButton(
                      action: () => BlocProvider.of<UserCubit>(context).loginUser(
                          form: {'phone': phone.text, 'password': password.text},
                          context: context),
                      color: Colors.white,
                     side: BorderSide(color:Color.fromRGBO(45, 139, 226, 1)),
                      radius: 8.0,
                      width: MediaQuery.of(context).size.width - 40,
                      child: Text('Войти',
                          textAlign: TextAlign.center,
                           style: GoogleFonts.montserrat(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500,
                                  color:  Color.fromRGBO(45, 139, 226, 1)),)
                    
      ),
      SizedBox(height: 10,),
      TextButton(onPressed: ()=>  Navigator.popAndPushNamed(context, '/user/register'), child: Text('Регистрация', style: GoogleFonts.montserrat(
                                fontSize: 15,
                                fontWeight: FontWeight.w500,
                                color:  Colors.white),)),
      ],
    ),
               
      body: Container(
        decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(
              'assets/images/background.jpg'),
          fit: BoxFit.cover,
        ),
      ),
        child: SingleChildScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            constraints:
                BoxConstraints(minHeight: MediaQuery.of(context).size.height),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                 SizedBox(
                        height: 70,
                      ),
                Row(
                  children: [
                    Padding(
                            padding: const EdgeInsets.only(bottom: 5),
                            child: IconButton(onPressed: (){
                              Navigator.pop(context);
                            }, icon: Icon(Icons.arrow_back_ios),color: Colors.white,iconSize: 18,),
                          ),
                    Container(
                      child: Text('Авторизация',
                          style: GoogleFonts.montserrat(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.white),),
                    ),
                  ],
                ),
                SizedBox(
                  height: 14,
                ),
                MainTextField(
                  inputFormatter: [maskFormatter],
                  text: 'Введите номер',
                  controller: phone,
                ),
                SizedBox(
                  height: 24,
                ),
                MainTextField(
                  controller: password,
                  obscureText: true,
                  text: 'Введите пароль',
                ),
                SizedBox(
                  height: 10,
                ),
                GestureDetector(
                  onTap: () =>
                      Navigator.pushNamed(context, '/type/phone/forget'),
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Text('Забыли пароль?',
                        style: GoogleFonts.montserrat(fontSize: 14,color: Colors.white,fontWeight: FontWeight.w500)),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
               
                
              ],
            ),
          ),
        ),
      ),
    );
  }
}


// floatingActionButton:  Column(
//       children: [
//         MainButton(
//                         action: () => BlocProvider.of<UserCubit>(context).loginUser(
//                             form: {'phone': phone.text, 'password': password.text},
//                             context: context),
//                         color: Colors.white,
//                        side: BorderSide(color:Color.fromRGBO(45, 139, 226, 1)),
//                         radius: 8.0,
//                         width: MediaQuery.of(context).size.width - 40,
//                         child: Text('Войти',
//                             textAlign: TextAlign.center,
//                              style: GoogleFonts.montserrat(
//                                     fontSize: 15,
//                                     fontWeight: FontWeight.w500,
//                                     color:  Color.fromRGBO(45, 139, 226, 1)),)
//                       ),
                       
                      
//       ],
//     ),