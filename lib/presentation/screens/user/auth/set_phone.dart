import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poputchik/presentation/components/main_field.dart';
import 'package:poputchik/presentation/components/main_gradient_button.dart';

class RequestSmsPhone extends StatefulWidget {
  const RequestSmsPhone({Key key}) : super(key: key);

  @override
  _RequestSmsPhoneState createState() => _RequestSmsPhoneState();
}

class _RequestSmsPhoneState extends State<RequestSmsPhone> {
  bool sended = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 72),
          constraints:
              BoxConstraints(minHeight: MediaQuery.of(context).size.height),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  child: Center(
                    child: Text('Введите телефон для отправки sms кода ',
                        style: GoogleFonts.montserrat(
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                            color: Colors.black),
                        textAlign: TextAlign.center),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                MainTextField(
                  text: 'Введите номер',
                ),
                SizedBox(
                  height: 30,
                ),
                sended
                    ? Column(
                        children: [
                          Container(
                            child: Center(
                              child: Text(
                                  'Введите sms полученный для того чтобы войти ',
                                  style: GoogleFonts.montserrat(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w700,
                                      color: Colors.black),
                                  textAlign: TextAlign.center),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          MainTextField(
                            text: 'Введите sms код',
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          MainTextField(
                            text: 'Введите новый пароль',
                          ),
                          SizedBox(
                            height: 30,
                          ),
                        ],
                      )
                    : SizedBox.shrink(),
                MainGradientButton(
                    action: () => sended
                        ? Navigator.pushNamed(context, '/user/home')
                        : setState(() => sended = true),
                    colors: [Colors.red, Colors.black],
                    side: Border.all(width: 0),
                    radius: 8.0,
                    width: MediaQuery.of(context).size.width - 144,
                    child: Text(sended ? 'Подтвердить sms' : 'Отправить sms',
                        textAlign: TextAlign.center,
                        style: GoogleFonts.montserrat(
                            fontSize: 15,
                            fontWeight: FontWeight.w500,
                            color: Colors.white)),
                    padding: EdgeInsets.symmetric(vertical: 15)),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
