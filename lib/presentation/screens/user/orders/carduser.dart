import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poputchik/data/constant.dart';
import 'package:poputchik/data/models/order_request.dart';
import 'package:poputchik/presentation/components/icon_menu.dart';

class UserOrders extends StatefulWidget {
  final List<OrderRequest> orderRequests;
  const UserOrders({
    Key key,
    this.orderRequests,
  }) : super(key: key);

  @override
  State<UserOrders> createState() => _UserOrdersState();
}

class _UserOrdersState extends State<UserOrders> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.only(
          top: 30,
        ),
        child: Center(
          child: SizedBox(
            child: ListView.builder(
                itemCount: widget.orderRequests.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () => showModalBottomSheet(
                      context: context,
                      builder: (context) => Historyoforder(
                        orderRequest: widget.orderRequests[index],
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Text(
                            '${DateTime.parse(widget.orderRequests[index].createdAt).day}.${DateTime.parse(widget.orderRequests[index].createdAt).month}.${DateTime.parse(widget.orderRequests[index].createdAt).year}',
                            style: GoogleFonts.montserrat(
                              textStyle: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  color: Color.fromRGBO(66, 66, 66, 1),
                                  fontSize: 16),
                            ),
                          ),
                        ),
                        Container(
                          width: 450,
                          child: Card(
                            elevation: 4,
                            child: Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 25.0, left: 15, right: 15),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          'Время заказа',
                                          style: GoogleFonts.montserrat(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                color: Color.fromRGBO(
                                                    66, 66, 66, 1),
                                                fontSize: 16),
                                          ),
                                        ),
                                        Text(
                                          '${DateTime.parse(widget.orderRequests[index].createdAt).hour.toString().padLeft(2, '0')}:${DateTime.parse(widget.orderRequests[index].createdAt).minute.toString().padLeft(2, '0')}',
                                          style: GoogleFonts.montserrat(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                color: Color.fromRGBO(
                                                    66, 66, 66, 1),
                                                fontSize: 16),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(15.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          'Трасса',
                                          style: GoogleFonts.montserrat(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.w500,
                                                color: Color.fromRGBO(
                                                    66, 66, 66, 1),
                                                fontSize: 16),
                                          ),
                                        ),
                                        Text(
                                          '${widget.orderRequests[index].track?.startName ?? ''} - ${widget.orderRequests[index].track?.endName ?? ''}',
                                          style: GoogleFonts.montserrat(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                color: Color.fromRGBO(
                                                    66, 66, 66, 1),
                                                fontSize: 16),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 15.0, left: 15, right: 15),
                                    child: Row(
                                      children: [
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              children: [
                                                IconMenu(
                                                  action: () => {},
                                                  icon: SvgPicture.asset(
                                                      'assets/images/pointblue.svg'),
                                                  text: '',
                                                ),
                                                Text(
                                                  '${widget.orderRequests[index].track?.startName ?? ''}',
                                                  style: GoogleFonts.montserrat(
                                                    textStyle: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        color: Color.fromRGBO(
                                                            66, 66, 66, 1),
                                                        fontSize: 16),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Container(
                                              margin: EdgeInsets.symmetric(
                                                  horizontal: 3.5),
                                              child: IconMenu(
                                                action: () => {},
                                                icon: SvgPicture.asset(
                                                    'assets/images/lineblue.svg'),
                                                text: '',
                                              ),
                                            ),
                                            Row(
                                              children: [
                                                IconMenu(
                                                  action: () => {},
                                                  icon: SvgPicture.asset(
                                                      'assets/images/point.svg'),
                                                  text: '',
                                                ),
                                                Text(
                                                  '${widget.orderRequests[index].track?.endName ?? ''}',
                                                  style: GoogleFonts.montserrat(
                                                    textStyle: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        color: Color.fromRGBO(
                                                            66, 66, 66, 1),
                                                        fontSize: 16),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Container(
                                              width: 320,
                                              child: Divider(
                                                thickness: 1,
                                                color: Color.fromRGBO(
                                                    203, 203, 203, 1),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 15.0, left: 15, right: 15),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          'Статус',
                                          style: GoogleFonts.montserrat(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.w500,
                                                color: Color.fromRGBO(
                                                    66, 66, 66, 1),
                                                fontSize: 16),
                                          ),
                                        ),
                                        Text(
                                          '${widget.orderRequests[index].orderRequestStatus.name}',
                                          style: GoogleFonts.montserrat(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                color: Color.fromRGBO(
                                                    66, 66, 66, 1),
                                                fontSize: 16),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(15.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          'Общая стоимость',
                                          style: GoogleFonts.montserrat(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.w500,
                                                color: Color.fromRGBO(
                                                    66, 66, 66, 1),
                                                fontSize: 16),
                                          ),
                                        ),
                                        Text(
                                          '${widget.orderRequests[index].price} Т',
                                          style: GoogleFonts.montserrat(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                color: Color.fromRGBO(
                                                    66, 66, 66, 1),
                                                fontSize: 16),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            margin: EdgeInsets.all(20),
                            shape: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(color: Colors.white)),
                          ),
                        ),
                      ],
                    ),
                  );
                }),
          ),
        ),
      ),
    );
  }
}

//  ТАП Выводится это
class Historyoforder extends StatelessWidget {
  final OrderRequest orderRequest;

  const Historyoforder({
    Key key,
    this.orderRequest,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        width: 450,
        child: Card(
          elevation: 4,
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding:
                      const EdgeInsets.only(top: 25.0, left: 15, right: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Время заказа',
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: Color.fromRGBO(66, 66, 66, 1),
                              fontSize: 16),
                        ),
                      ),
                      Text(
                        '${DateTime.parse(orderRequest.createdAt).hour.toString().padLeft(2, '0')}:${DateTime.parse(orderRequest.createdAt).minute.toString().padLeft(2, '0')}',
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: Color.fromRGBO(66, 66, 66, 1),
                              fontSize: 16),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Трасса',
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                              fontWeight: FontWeight.w500,
                              color: Color.fromRGBO(66, 66, 66, 1),
                              fontSize: 16),
                        ),
                      ),
                      Text(
                        '${orderRequest.track?.startName ?? ''} - ${orderRequest.track?.endName ?? ''}',
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: Color.fromRGBO(66, 66, 66, 1),
                              fontSize: 16),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 15.0, left: 15, right: 15),
                  child: Row(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              IconMenu(
                                action: () => {},
                                icon: SvgPicture.asset(
                                    'assets/images/pointblue.svg'),
                                text: '',
                              ),
                              Text(
                                '${orderRequest.track?.startName ?? ''}',
                                style: GoogleFonts.montserrat(
                                  textStyle: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      color: Color.fromRGBO(66, 66, 66, 1),
                                      fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 3.5),
                            child: IconMenu(
                              action: () => {},
                              icon: SvgPicture.asset(
                                  'assets/images/lineblue.svg'),
                              text: '',
                            ),
                          ),
                          Row(
                            children: [
                              IconMenu(
                                action: () => {},
                                icon:
                                    SvgPicture.asset('assets/images/point.svg'),
                                text: '',
                              ),
                              Text(
                                '${orderRequest.track?.endName ?? ''}',
                                style: GoogleFonts.montserrat(
                                  textStyle: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      color: Color.fromRGBO(66, 66, 66, 1),
                                      fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                          Container(
                            width: 320,
                            child: Divider(
                              thickness: 1,
                              color: Color.fromRGBO(203, 203, 203, 1),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 15.0, left: 15, right: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Оплата',
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                              fontWeight: FontWeight.w500,
                              color: Color.fromRGBO(66, 66, 66, 1),
                              fontSize: 16),
                        ),
                      ),
                      Text(
                        'Kaspi перевод',
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: Color.fromRGBO(66, 66, 66, 1),
                              fontSize: 16),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 15.0, left: 15, right: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Cтоимость',
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                              fontWeight: FontWeight.w500,
                              color: Color.fromRGBO(66, 66, 66, 1),
                              fontSize: 16),
                        ),
                      ),
                      Text(
                        '${orderRequest.comfort == false ? 'Комфорт' : 'Эконом'} - ${orderRequest.price / orderRequest.seats} Т',
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: Color.fromRGBO(66, 66, 66, 1),
                              fontSize: 16),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 15.0, left: 15, right: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Общая стоимость',
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                              fontWeight: FontWeight.w500,
                              color: Color.fromRGBO(66, 66, 66, 1),
                              fontSize: 16),
                        ),
                      ),
                      Text(
                        '${orderRequest.price} Т',
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: Color.fromRGBO(66, 66, 66, 1),
                              fontSize: 16),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 15.0, left: 15, right: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Кол - во мест',
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                              fontWeight: FontWeight.w500,
                              color: Color.fromRGBO(66, 66, 66, 1),
                              fontSize: 16),
                        ),
                      ),
                      Text(
                        '${orderRequest.seats}',
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: Color.fromRGBO(66, 66, 66, 1),
                              fontSize: 16),
                        ),
                      ),
                    ],
                  ),
                ),
                if (orderRequest.order?.driver != null)
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Container(
                      padding: EdgeInsets.only(
                        top: 20,
                      ),
                      margin: EdgeInsets.only(bottom: 20),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(15),
                        ),
                        color: Color.fromRGBO(237, 244, 255, 1),
                      ),
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        children: [
                          Row(
                            children: [
                              SizedBox(
                                width: 25,
                              ),
                              CircleAvatar(
                                radius: 23,
                                foregroundImage: NetworkImage(
                                    'https://img2.freepng.ru/20180920/yko/kisspng-computer-icons-portable-network-graphics-avatar-ic-5ba3c66df14d32.3051789815374598219884.jpg'),
                              ),
                              SizedBox(
                                width: 25,
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text('${orderRequest.order.driver.name}',
                                      style: GoogleFonts.montserrat(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600,
                                      )),
                                  SizedBox(
                                    height: 8,
                                  ),
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.star,
                                        color: starColor,
                                        size: 20,
                                      ),
                                      SizedBox(
                                        width: 2,
                                      ),
                                      Text(
                                        '${orderRequest.order.driver.rating.toString().padRight(2, '.0')}',
                                        style: GoogleFonts.montserrat(
                                            fontSize: 12, color: greyText),
                                      )
                                    ],
                                  ),
                                ],
                              )
                            ],
                          ),
                          Container(
                            width: 290,
                            child: Divider(
                              thickness: 1,
                              color: Color.fromRGBO(200, 221, 255, 1),
                            ),
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Text(
                                    '${orderRequest.order.driver.carColor} ${orderRequest.order.driver.carModel}'),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 15, vertical: 8),
                                  decoration: BoxDecoration(
                                    color: greyCarNumber,
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child:
                                      Text('${orderRequest.order.driver.carId}',
                                          style: GoogleFonts.montserrat(
                                            fontSize: 14,
                                            fontWeight: FontWeight.normal,
                                          )),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
