import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poputchik/cubit/app/cubit/app_cubit.dart';
import 'package:poputchik/cubit/order/cubit/order_cubit.dart';
import 'package:poputchik/cubit/order_request/cubit/order_request_cubit.dart';
import 'package:poputchik/data/models/order_request.dart';
import 'package:poputchik/presentation/components/calculate_orders.dart';
import 'package:poputchik/presentation/items/order.dart';
import 'package:poputchik/presentation/items/order_main.dart';

class UserOrderHistory extends StatelessWidget {
  final bool driver;
  const UserOrderHistory({Key key, this.driver}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final appCubit = BlocProvider.of<AppCubit>(context);
    if (driver == false)
      BlocProvider.of<OrderRequestCubit>(context).getRequests();
    else
      BlocProvider.of<OrderCubit>(context).getOrders();
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 45),
          padding: EdgeInsets.only(top: 60),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(100)),
                      padding: EdgeInsets.all(5),
                      child: Icon(Icons.arrow_back),
                    ),
                  ),
                  SizedBox(
                    width: 80,
                  ),
                  Text(
                    appCubit.getWord('history'),
                    style: GoogleFonts.montserrat(
                        fontSize: 16, fontWeight: FontWeight.w700),
                    textAlign: TextAlign.center,
                  ),
                  Container()
                ],
              ),
              SizedBox(
                height: 42,
              ),
              Builder(
                builder: (BuildContext context) {
                  if (!driver)
                    return BlocBuilder<OrderRequestCubit, OrderRequestState>(
                      builder: (context, state) {
                        if (state is OrderRequestsLoaded) {
                          final orderRequests = (state)
                              .requests
                              .where((element) => element.track != null)
                              .cast<OrderRequest>()
                              .toList();
                          return ListView.builder(
                            itemBuilder: (_, index) => OrderItem(
                              orderRequest: orderRequests[index],
                            ),
                            itemCount: orderRequests.length,
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                          );
                        }

                        return SizedBox.shrink();
                      },
                    );
                  return BlocBuilder<OrderCubit, OrderState>(
                    builder: (context, state) {
                      if (state is OrdersLoaded)
                        return ListView.builder(
                          itemBuilder: (_, index) => OrderMainItem(
                            order: state.orders[index],
                          ),
                          itemCount: state.orders.length,
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                        );
                      return SizedBox.shrink();
                    },
                  );
                },
              ),
              SizedBox(
                height: 14,
              ),
              driver == true
                  ? BlocBuilder<OrderCubit, OrderState>(
                      builder: (context, state) {
                        if (state is OrdersLoaded)
                          return CalculateOrder(orders: state.orders);
                        return SizedBox.shrink();
                      },
                    )
                  : BlocBuilder<OrderRequestCubit, OrderRequestState>(
                      builder: (context, state) {
                        if (state is OrderRequestsLoaded)
                          return Row(children: [
                            Text(
                                '${appCubit.getWord('overall')}: ${state.requests.where((element) => element.track != null).fold(0, (previousValue, element) => previousValue + element.price)} тг',
                                style: GoogleFonts.montserrat(
                                    fontSize: 16, color: Color(0xff313131)))
                          ]);
                        return SizedBox.shrink();
                      },
                    ),
              SizedBox(
                height: 44,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
