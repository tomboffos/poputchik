import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poputchik/data/constant.dart';
import 'package:poputchik/presentation/components/icon_menu.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class CardDriver extends StatefulWidget {
  const CardDriver({
    Key key,
  }) : super(key: key);

  @override
  State<CardDriver> createState() => _CardDriverState();
}

class _CardDriverState extends State<CardDriver> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Center(
            child: Text("Здесь Карта"),
          ),
          SlidingUpPanel(
            maxHeight: MediaQuery.of(context).size.height / 1.2,
            minHeight: MediaQuery.of(context).size.height / 1.4,
            panel: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: SingleChildScrollView(
                child: Center(
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 10, bottom: 10),
                        width: 75,
                        height: 5,
                        decoration: BoxDecoration(
                            color: Colors.grey[300],
                            borderRadius:
                                BorderRadius.all(Radius.circular(12.0))),
                      ),
                      SingleChildScrollView(
                        child: Center(
                          child: Column(
                            children: [
                              Center(
                                child: Text(
                                  'История заказов',
                                  style: GoogleFonts.montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        color: Color.fromRGBO(66, 66, 66, 1),
                                        fontSize: 16),
                                  ),
                                ),
                              ),

                              DefaultTabController(
                                length: 2,
                                child: Column(
                                  children: [
                                    Container(
                                      child: TabBar(
                                        indicatorPadding:
                                            const EdgeInsets.symmetric(
                                                horizontal: 55),
                                        indicatorWeight: 0,
                                        indicator: UnderlineTabIndicator(
                                          borderSide: BorderSide(
                                            width: 2,
                                            color:
                                                Color.fromRGBO(45, 139, 226, 1),
                                          ),
                                        ),
                                        labelStyle: GoogleFonts.montserrat(
                                            color: Colors.black),
                                        unselectedLabelStyle:
                                            GoogleFonts.montserrat(
                                                color: Colors.grey),
                                        tabs: [
                                          Tab(
                                            child: Text(
                                              'История заказов',
                                              style: GoogleFonts.montserrat(
                                                textStyle: TextStyle(
                                                    fontWeight: FontWeight.w400,
                                                    color: Color.fromRGBO(
                                                        45, 139, 226, 1),
                                                    fontSize: 16),
                                              ),
                                            ),
                                          ),
                                          Tab(
                                            child: Text(
                                              'Общая статистика',
                                              style: GoogleFonts.montserrat(
                                                textStyle: TextStyle(
                                                    fontWeight: FontWeight.w500,
                                                    color: Color.fromRGBO(
                                                        45, 139, 226, 1),
                                                    fontSize: 16),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      height:
                                          MediaQuery.of(context).size.height,
                                      child: TabBarView(children: [
                                        MultipleDriver(),
                                      
                                        StaticsDriver(),

                                          // HistoryoforderDriver() TAP история 
                                      ]),
                                    ),
                                  ],
                                ),
                              ),

                             ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class StaticsDriver extends StatelessWidget {
  const StaticsDriver({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context)
          .size
          .height,
      child: ListView.builder(
          itemCount: 1,
          itemBuilder: (context, index) {
            return Column(
              crossAxisAlignment:
                  CrossAxisAlignment.start,
              children: [
                Container(
                  width: 450,
                  height:
                      MediaQuery.of(context)
                              .size
                              .height /
                          2,
                  child: Card(
                    elevation: 4,
                    child: Container(
                      child: Column(
                        mainAxisAlignment:
                            MainAxisAlignment
                                .start,
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets
                                        .only(
                                    top:
                                        25.0,
                                    left:
                                        15,
                                    right:
                                        15),
                            child: Align(
                              alignment:
                                  Alignment
                                      .centerLeft,
                              child: Text(
                                'Общая статистика доходов',
                                style: GoogleFonts
                                    .montserrat(
                                  textStyle:
                                      TextStyle(
                                    fontWeight:
                                        FontWeight.w600,
                                    color: Color.fromRGBO(
                                        66,
                                        66,
                                        66,
                                        1),
                                    fontSize:
                                        16,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets
                                        .all(
                                    15.0),
                            child: Row(
                              children: [
                                IconMenu(
                                  action:
                                      () =>
                                          {},
                                  icon: SvgPicture
                                      .asset(
                                          'assets/images/Calendar.svg'),
                                  text: '',
                                ),
                                Text(
                                  '10.10.2021 - 10.12.2021',
                                  style: GoogleFonts
                                      .montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight
                                            .w400,
                                        color: Color.fromRGBO(
                                            66,
                                            66,
                                            66,
                                            1),
                                        fontSize:
                                            16),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 320,
                            child: Divider(
                              thickness: 1,
                              color: Color
                                  .fromRGBO(
                                      203,
                                      203,
                                      203,
                                      1),
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets
                                        .only(
                                    top:
                                        15.0,
                                    left:
                                        15,
                                    right:
                                        15),
                            child: Row(
                              mainAxisAlignment:
                                  MainAxisAlignment
                                      .spaceBetween,
                              children: [
                                Text(
                                  'Количество поездок',
                                  style: GoogleFonts
                                      .montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight
                                            .w500,
                                        color: Color.fromRGBO(
                                            66,
                                            66,
                                            66,
                                            1),
                                        fontSize:
                                            16),
                                  ),
                                ),
                                Text(
                                  '10',
                                  style: GoogleFonts
                                      .montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight
                                            .w400,
                                        color: Color.fromRGBO(
                                            66,
                                            66,
                                            66,
                                            1),
                                        fontSize:
                                            16),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets
                                        .only(
                                    top:
                                        15.0,
                                    left:
                                        15,
                                    right:
                                        15),
                            child: Row(
                              mainAxisAlignment:
                                  MainAxisAlignment
                                      .spaceBetween,
                              children: [
                                Text(
                                  'Чаевые',
                                  style: GoogleFonts
                                      .montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight
                                            .w500,
                                        color: Color.fromRGBO(
                                            66,
                                            66,
                                            66,
                                            1),
                                        fontSize:
                                            16),
                                  ),
                                ),
                                Text(
                                  '650 Т',
                                  style: GoogleFonts
                                      .montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight
                                            .w400,
                                        color: Color.fromRGBO(
                                            66,
                                            66,
                                            66,
                                            1),
                                        fontSize:
                                            16),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets
                                        .only(
                                    top:
                                        15.0,
                                    left:
                                        15,
                                    right:
                                        15),
                            child: Row(
                              mainAxisAlignment:
                                  MainAxisAlignment
                                      .spaceBetween,
                              children: [
                                Text(
                                  'Оплата наличными',
                                  style: GoogleFonts
                                      .montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight
                                            .w500,
                                        color: Color.fromRGBO(
                                            66,
                                            66,
                                            66,
                                            1),
                                        fontSize:
                                            16),
                                  ),
                                ),
                                Text(
                                  '1250 Т',
                                  style: GoogleFonts
                                      .montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight
                                            .w400,
                                        color: Color.fromRGBO(
                                            66,
                                            66,
                                            66,
                                            1),
                                        fontSize:
                                            16),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets
                                        .only(
                                    top:
                                        15.0,
                                    left:
                                        15,
                                    right:
                                        15),
                            child: Row(
                              mainAxisAlignment:
                                  MainAxisAlignment
                                      .spaceBetween,
                              children: [
                                Text(
                                  'Оплата переводом',
                                  style: GoogleFonts
                                      .montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight
                                            .w500,
                                        color: Color.fromRGBO(
                                            66,
                                            66,
                                            66,
                                            1),
                                        fontSize:
                                            16),
                                  ),
                                ),
                                Text(
                                  '31 250 Т',
                                  style: GoogleFonts
                                      .montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight
                                            .w400,
                                        color: Color.fromRGBO(
                                            66,
                                            66,
                                            66,
                                            1),
                                        fontSize:
                                            16),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets
                                        .only(
                                    top:
                                        15.0,
                                    left:
                                        15,
                                    right:
                                        15),
                            child: Row(
                              mainAxisAlignment:
                                  MainAxisAlignment
                                      .spaceBetween,
                              children: [
                                Text(
                                  'Комиссия',
                                  style: GoogleFonts
                                      .montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight
                                            .w500,
                                        color: Color.fromRGBO(
                                            66,
                                            66,
                                            66,
                                            1),
                                        fontSize:
                                            16),
                                  ),
                                ),
                                Text(
                                  '2 250 Т',
                                  style: GoogleFonts
                                      .montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight
                                            .w400,
                                        color: Color.fromRGBO(
                                            66,
                                            66,
                                            66,
                                            1),
                                        fontSize:
                                            16),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets
                                        .only(
                                    top:
                                        15.0,
                                    left:
                                        15,
                                    right:
                                        15),
                            child: Row(
                              mainAxisAlignment:
                                  MainAxisAlignment
                                      .spaceBetween,
                              children: [
                                Text(
                                  'Задолженность',
                                  style: GoogleFonts
                                      .montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight
                                            .w500,
                                        color: Color.fromRGBO(
                                            66,
                                            66,
                                            66,
                                            1),
                                        fontSize:
                                            16),
                                  ),
                                ),
                                Text(
                                  '0 Т',
                                  style: GoogleFonts
                                      .montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight
                                            .w400,
                                        color: Color.fromRGBO(
                                            66,
                                            66,
                                            66,
                                            1),
                                        fontSize:
                                            16),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets
                                        .all(
                                    15.0),
                            child: Row(
                              mainAxisAlignment:
                                  MainAxisAlignment
                                      .spaceBetween,
                              children: [
                                Text(
                                  'Итого',
                                  style: GoogleFonts
                                      .montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight
                                            .w600,
                                        color: Color.fromRGBO(
                                            66,
                                            66,
                                            66,
                                            1),
                                        fontSize:
                                            20),
                                  ),
                                ),
                                Text(
                                  '35 300 Т',
                                  style: GoogleFonts
                                      .montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight
                                            .w500,
                                        color: Color.fromRGBO(
                                            66,
                                            66,
                                            66,
                                            1),
                                        fontSize:
                                            16),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    margin:
                        EdgeInsets.all(20),
                    shape: OutlineInputBorder(
                        borderRadius:
                            BorderRadius
                                .circular(
                                    10),
                        borderSide:
                            BorderSide(
                                color: Colors
                                    .white)),
                  ),
                ),
              ],
            );
          }),
    );
  }
}

class MultipleDriver extends StatelessWidget {
  const MultipleDriver({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height,
      child: ListView.builder(
          itemCount: 5,
          itemBuilder: (context, index) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 450,
                  height: MediaQuery.of(context).size.height / 1.8,
                  child: Card(
                    elevation: 4,
                    child: Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 25.0, left: 15, right: 15),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Время заказа',
                                  style: GoogleFonts.montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        color: Color.fromRGBO(66, 66, 66, 1),
                                        fontSize: 16),
                                  ),
                                ),
                                Text(
                                  '09:06',
                                  style: GoogleFonts.montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        color: Color.fromRGBO(66, 66, 66, 1),
                                        fontSize: 16),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Трасса',
                                  style: GoogleFonts.montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        color: Color.fromRGBO(66, 66, 66, 1),
                                        fontSize: 16),
                                  ),
                                ),
                                Text(
                                  'Каскелен - Алматы',
                                  style: GoogleFonts.montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        color: Color.fromRGBO(66, 66, 66, 1),
                                        fontSize: 16),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 15.0, left: 15, right: 15),
                            child: Row(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        IconMenu(
                                          action: () => {},
                                          icon: SvgPicture.asset(
                                              'assets/images/pointblue.svg'),
                                          text: '',
                                        ),
                                        Text(
                                          'ул. Абая Масанчи 124А',
                                          style: GoogleFonts.montserrat(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                color: Color.fromRGBO(
                                                    66, 66, 66, 1),
                                                fontSize: 16),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 3.5),
                                      child: IconMenu(
                                        action: () => {},
                                        icon: SvgPicture.asset(
                                            'assets/images/lineblue.svg'),
                                        text: '',
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        IconMenu(
                                          action: () => {},
                                          icon: SvgPicture.asset(
                                              'assets/images/point.svg'),
                                          text: '',
                                        ),
                                        Text(
                                          'Казыбек би 163/17',
                                          style: GoogleFonts.montserrat(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                color: Color.fromRGBO(
                                                    66, 66, 66, 1),
                                                fontSize: 16),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 3.5),
                                      child: IconMenu(
                                        action: () => {},
                                        icon: SvgPicture.asset(
                                            'assets/images/linegrey.svg'),
                                        text: '',
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        IconMenu(
                                          action: () => {},
                                          icon: SvgPicture.asset(
                                              'assets/images/point.svg'),
                                          text: '',
                                        ),
                                        Text(
                                          'Казыбек би 122',
                                          style: GoogleFonts.montserrat(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                color: Color.fromRGBO(
                                                    66, 66, 66, 1),
                                                fontSize: 16),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 3.5),
                                      child: IconMenu(
                                        action: () => {},
                                        icon: SvgPicture.asset(
                                            'assets/images/linegrey.svg'),
                                        text: '',
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        IconMenu(
                                          action: () => {},
                                          icon: SvgPicture.asset(
                                              'assets/images/point.svg'),
                                          text: '',
                                        ),
                                        Text(
                                          'Толе би 132/15',
                                          style: GoogleFonts.montserrat(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                color: Color.fromRGBO(
                                                    66, 66, 66, 1),
                                                fontSize: 16),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 3.5),
                                      child: IconMenu(
                                        action: () => {},
                                        icon: SvgPicture.asset(
                                            'assets/images/linegrey.svg'),
                                        text: '',
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        IconMenu(
                                          action: () => {},
                                          icon: SvgPicture.asset(
                                              'assets/images/point.svg'),
                                          text: '',
                                        ),
                                        Text(
                                          'Гоголя 12А',
                                          style: GoogleFonts.montserrat(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                color: Color.fromRGBO(
                                                    66, 66, 66, 1),
                                                fontSize: 16),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      width: 320,
                                      child: Divider(
                                        thickness: 1,
                                        color: Color.fromRGBO(203, 203, 203, 1),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 15.0, left: 15, right: 15),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Оплата',
                                  style: GoogleFonts.montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        color: Color.fromRGBO(66, 66, 66, 1),
                                        fontSize: 16),
                                  ),
                                ),
                                Text(
                                  'Kaspi перевод',
                                  style: GoogleFonts.montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        color: Color.fromRGBO(66, 66, 66, 1),
                                        fontSize: 16),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Общая стоимость',
                                  style: GoogleFonts.montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        color: Color.fromRGBO(66, 66, 66, 1),
                                        fontSize: 16),
                                  ),
                                ),
                                Text(
                                  '1650 Т',
                                  style: GoogleFonts.montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        color: Color.fromRGBO(66, 66, 66, 1),
                                        fontSize: 16),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    margin: EdgeInsets.all(20),
                    shape: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(color: Colors.white)),
                  ),
                ),
              ],
            );
          }),
    );
  }
}

//  ТАП Выводится это
class HistoryoforderDriver extends StatelessWidget {
  const HistoryoforderDriver({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Container(
          width: 250,
          height: MediaQuery.of(context).size.height / 2,
          child: Card(
            elevation: 8,
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                   Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(15),
                        ),
                        color: Color.fromRGBO(237, 244, 255, 1),
                      ),
                      width: MediaQuery.of(context).size.width,
                      height: 80,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(
                            children: [
                              SizedBox(
                                width: 25,
                              ),
                              CircleAvatar(
                                radius: 23,
                                foregroundImage: NetworkImage(
                                    'https://img2.freepng.ru/20180920/yko/kisspng-computer-icons-portable-network-graphics-avatar-ic-5ba3c66df14d32.3051789815374598219884.jpg'),
                              ),
                              SizedBox(
                                width: 25,
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                   SizedBox(
                                    height: 8,
                                  ),
                                  Text('Сара',
                                      style: GoogleFonts.montserrat(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600,
                                      )),
                                  SizedBox(
                                    height: 8,
                                  ),
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.star,
                                        color: starColor,
                                        size: 20,
                                      ),
                                      SizedBox(
                                        width: 2,
                                      ),
                                      Text(
                                        '5.0',
                                        style: GoogleFonts.montserrat(
                                            fontSize: 12, color: greyText),
                                      )
                                    ],
                                  ),
                                ],
                              )
                            ],
                          ),
                          
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 25.0, left: 15, right: 15),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Маршрут',
                        style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: Color.fromRGBO(66, 66, 66, 1),
                              fontSize: 16),
                        ),
                      ),
                    ),
                  ),
                 
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 15.0, left: 15, right: 15),
                    child: Row(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                           
                            Row(
                              children: [
                                IconMenu(
                                  action: () => {},
                                  icon:
                                      SvgPicture.asset('assets/images/point.svg'),
                                  text: '',
                                ),
                                Text(
                                  'Казыбек би 163/17',
                                  style: GoogleFonts.montserrat(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        color: Color.fromRGBO(66, 66, 66, 1),
                                        fontSize: 16),
                                  ),
                                ),
                              ],
                            ),
                          
                            
                          ],
                        ),
                      ],
                    ),
                  ),
                   Padding(
                    padding:
                        const EdgeInsets.only(top: 15.0, left: 15, right: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Cтоимость',
                          style: GoogleFonts.montserrat(
                            textStyle: TextStyle(
                                fontWeight: FontWeight.w500,
                                color: Color.fromRGBO(66, 66, 66, 1),
                                fontSize: 16),
                          ),
                        ),
                        Text(
                          'Эконом - 1550 Т',
                          style: GoogleFonts.montserrat(
                            textStyle: TextStyle(
                                fontWeight: FontWeight.w400,
                                color: Color.fromRGBO(66, 66, 66, 1),
                                fontSize: 16),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                              width: 350,
                              child: Divider(
                                thickness: 1,
                                color: Color.fromRGBO(203, 203, 203, 1),
                              ),
                            ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 15.0, left: 15, right: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Оплата',
                          style: GoogleFonts.montserrat(
                            textStyle: TextStyle(
                                fontWeight: FontWeight.w500,
                                color: Color.fromRGBO(66, 66, 66, 1),
                                fontSize: 16),
                          ),
                        ),
                        Text(
                          'Kaspi перевод',
                          style: GoogleFonts.montserrat(
                            textStyle: TextStyle(
                                fontWeight: FontWeight.w400,
                                color: Color.fromRGBO(66, 66, 66, 1),
                                fontSize: 16),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 15.0, left: 15, right: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Cтоимость',
                          style: GoogleFonts.montserrat(
                            textStyle: TextStyle(
                                fontWeight: FontWeight.w500,
                                color: Color.fromRGBO(66, 66, 66, 1),
                                fontSize: 16),
                          ),
                        ),
                        Text(
                          'Эконом - 1550 Т',
                          style: GoogleFonts.montserrat(
                            textStyle: TextStyle(
                                fontWeight: FontWeight.w400,
                                color: Color.fromRGBO(66, 66, 66, 1),
                                fontSize: 16),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 15.0, left: 15, right: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Поднятие цены',
                          style: GoogleFonts.montserrat(
                            textStyle: TextStyle(
                                fontWeight: FontWeight.w500,
                                color: Color.fromRGBO(66, 66, 66, 1),
                                fontSize: 16),
                          ),
                        ),
                        Text(
                          '100 Т',
                          style: GoogleFonts.montserrat(
                            textStyle: TextStyle(
                                fontWeight: FontWeight.w400,
                                color: Color.fromRGBO(66, 66, 66, 1),
                                fontSize: 16),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 15.0, left: 15, right: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Общая стоимость',
                          style: GoogleFonts.montserrat(
                            textStyle: TextStyle(
                                fontWeight: FontWeight.w500,
                                color: Color.fromRGBO(66, 66, 66, 1),
                                fontSize: 16),
                          ),
                        ),
                        Text(
                          '1650 Т',
                          style: GoogleFonts.montserrat(
                            textStyle: TextStyle(
                                fontWeight: FontWeight.w400,
                                color: Color.fromRGBO(66, 66, 66, 1),
                                fontSize: 16),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 15.0, left: 15, right: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Кол - во мест',
                          style: GoogleFonts.montserrat(
                            textStyle: TextStyle(
                                fontWeight: FontWeight.w500,
                                color: Color.fromRGBO(66, 66, 66, 1),
                                fontSize: 16),
                          ),
                        ),
                        Text(
                          '2',
                          style: GoogleFonts.montserrat(
                            textStyle: TextStyle(
                                fontWeight: FontWeight.w400,
                                color: Color.fromRGBO(66, 66, 66, 1),
                                fontSize: 16),
                          ),
                        ),
                      ],
                    ),
                  ),
                 
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
