import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poputchik/cubit/driver/cubit/driver_cubit.dart';
import 'package:poputchik/cubit/user/cubit/user_cubit.dart';
import 'package:poputchik/presentation/components/main_button.dart';
import 'package:poputchik/presentation/components/main_field.dart';
import 'package:poputchik/presentation/components/main_gradient_button.dart';

class BalanceIndex extends StatefulWidget {
  const BalanceIndex({Key key}) : super(key: key);

  @override
  State<BalanceIndex> createState() => _BalanceIndexState();
}

class _BalanceIndexState extends State<BalanceIndex> {
  TextEditingController balance = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<UserCubit>(context).getUser();
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 45),
          padding: EdgeInsets.only(top: 60),
          child: Column(
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                GestureDetector(
                  onTap: () => Navigator.pop(context),
                  child: Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black),
                        borderRadius: BorderRadius.circular(100)),
                    padding: EdgeInsets.all(5),
                    child: Icon(Icons.arrow_back),
                  ),
                ),
                SizedBox(
                  width: 80,
                ),
              ]),
              SizedBox(
                height: 39,
              ),
              Center(
                child: Text(
                  'Баланс',
                  style: Theme.of(context).textTheme.headline5,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              BlocBuilder<UserCubit, UserState>(
                builder: (context, state) {
                  if (state is UserLoaded)
                    return Center(
                      child: Text(
                        'Ваш баланс составляет: ${(state).user.driver.balance}тг',
                        style: GoogleFonts.montserrat(
                            fontSize: 14, fontWeight: FontWeight.w600),
                      ),
                    );
                  return SizedBox.shrink();
                },
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                child: MainButton(
                  action: () => showDialog(
                      context: context,
                      builder: (_) => AlertDialog(
                            content: Container(
                              height: 200,
                              width: 400,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text('Введите сумму желаемое для пополнение'),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Container(
                                    child: MainTextField(
                                      text: 'Сумма',
                                      controller: balance,
                                    ),
                                    width: 350,
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  MainButton(
                                    action: () {
                                      BlocProvider.of<DriverCubit>(context)
                                          .fillBalance(
                                              context: context,
                                              price: balance.text);
                                      Navigator.pop(_);
                                    },
                                    width: 350.0,
                                    child: Center(
                                        child: Text(
                                      'Пополнить',
                                      style: TextStyle(
                                          color:
                                              Theme.of(context).primaryColor),
                                    )),
                                    radius: 8.0,
                                    side: BorderSide(
                                      color: Theme.of(context).primaryColor,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          )),
                  side: BorderSide(
                    color: Theme.of(context).primaryColor,
                  ),
                  radius: 8.0,
                  width: MediaQuery.of(context).size.width / 2,
                  child: Text('Пополнить',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.montserrat(
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        color: Theme.of(context).primaryColor,
                      )),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
