import 'dart:async';
import 'dart:convert';
import 'dart:developer' as dev;
import 'dart:math';
import 'dart:math' as math;

import 'package:adhara_socket_io/manager.dart';
import 'package:adhara_socket_io/options.dart';
import 'package:adhara_socket_io/socket.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:location/location.dart';
import 'package:poputchik/cubit/app/cubit/app_cubit.dart';
import 'package:poputchik/cubit/chat/cubit/chat_cubit.dart';
import 'package:poputchik/cubit/help_phone/cubit/help_phone_cubit.dart';
import 'package:poputchik/cubit/order/cubit/order_cubit.dart';
import 'package:poputchik/cubit/track/cubit/track_cubit.dart';
import 'package:poputchik/cubit/user/cubit/user_cubit.dart';
import 'package:poputchik/data/constant.dart';
import 'package:poputchik/data/network_service.dart';
import 'package:poputchik/presentation/components/car_id_widget.dart';
import 'package:poputchik/presentation/components/user_balance.dart';
import 'package:poputchik/presentation/components/chat_slide.dart';
import 'package:poputchik/presentation/components/create_direction.dart';
import 'package:poputchik/presentation/components/icon_menu.dart';
import 'package:poputchik/presentation/items/help.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class DriverMapIndex extends StatefulWidget {
  const DriverMapIndex({Key key}) : super(key: key);

  @override
  _DriverMapIndexState createState() => _DriverMapIndexState();
}

class _DriverMapIndexState extends State<DriverMapIndex> {
  YandexMapController _controller;
  PanelController panelController = new PanelController();
  Location location = new Location();
  PermissionStatus _permissionGranted;
  bool showProduct = false;
  dynamic driverId;
  closeSlide() async {
    setState(() {
      showProduct = false;
      chatSlide = false;
    });
    panelController.close();
    BlocProvider.of<OrderCubit>(context).emit(OrderInitial());
  }

  openSlide() {
    setState(() {
      showProduct = true;
    });
    panelController.open();
  }

  bool value = false;
  bool help = false;

  List<MapObject> mapObjects = [];

  @override
  void initState() {
    BlocProvider.of<UserCubit>(context).getUser();
    BlocProvider.of<HelpPhoneCubit>(context).getHelpPhones();
    BlocProvider.of<ChatCubit>(context).getMessage();
    BlocProvider.of<ChatCubit>(context).linkSocket();
    BlocProvider.of<TrackCubit>(context).getTracks(query: '');

    super.initState();
  }

  constructTrack(track) async {
    BlocProvider.of<OrderCubit>(context)
        .chooseTrack(
      track: track,
      controller: _controller,
      context: context,
    )
        .then((value) {
      if (value['track'] != null) {
        animate(value['direction']);
        setState(() {
          mapObjects.removeWhere((element) =>
              element.mapId.value == 'track' ||
              element.mapId.value == 'track_final_points');

          mapObjects.add(PlacemarkMapObject(
              opacity: 1,
              icon: PlacemarkIcon.single(PlacemarkIconStyle(
                  scale: 2.0,
                  image: BitmapDescriptor.fromAssetImage(
                      'assets/images/pointred.png'))),
              point: Point(
                latitude: value['point'][0],
                longitude: value['point'][1],
              ),
              mapId: MapObjectId('track_final_points')));
        });
        setState(() {
          showProduct = true;
        });
      } else {
        BlocProvider.of<OrderCubit>(context).emit(OrderInitial());
        closeSlide();
      }
    });

    openSlide();
  }

  List<StreamSubscription> streams = [];

  bool chatSlide = false;

  openChatSlide({context}) {
    Navigator.pop(context);
    setState(() {
      chatSlide = true;
      showProduct = true;
    });
    panelController.open();
  }

  String addressLine;

  LocationData _locationData;
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<OrderCubit, OrderState>(
      listener: (context, state) async {
        if (state is OrderInitial) {
          setState(() {
            mapObjects
                .removeWhere((element) => element.mapId.value != 'geolocation');
          });

          streams.forEach((element) async {
            await element.cancel();
          });
        }
        if (state is OrderTrackCreated) {
          SocketIO socket = await SocketIOManager().createInstance(
              SocketOptions('https://socket.it-lead.net',
                  enableLogging: false));
          await socket.connectSync();

          streams.add(socket
              .on('poputchik.order.user.location.change.${(state).order.id}')
              .listen((event) {
            dev.log('RECEIVED' + event.toString());
            setState(() {
              mapObjects.removeWhere((element) =>
                  element.mapId.value == 'user_location_${event[0]['id']}');

              mapObjects.add(PlacemarkMapObject(
                  opacity: 1,
                  icon: PlacemarkIcon.single(PlacemarkIconStyle(
                      scale: 0.3,
                      rotationType: RotationType.rotate,
                      image: BitmapDescriptor.fromAssetImage(
                          'assets/images/human.png'))),
                  point: Point(
                    latitude: event[0]['position'][0],
                    longitude: event[0]['position'][1],
                  ),
                  mapId: MapObjectId('user_location_${event[0]['id']}')));
            });
          }));
          streams
              .add(location.onLocationChanged.listen((newLocationData) async {
            await post(Uri.parse('${NetworkService().apiUrl}/orders/socket'),
                body: {
                  'event':
                      "poputchik.order.driver.location.change.${(state).order.id}",
                  "data": jsonEncode({
                    "position": [
                      newLocationData.latitude,
                      newLocationData.longitude
                    ]
                  })
                });
          }));
        }
      },
      builder: (context, state) {
        final appCubit = BlocProvider.of<AppCubit>(context);
        return Scaffold(
          drawerEnableOpenDragGesture: false,
          drawer: Drawer(
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.only(
                  left: 32,
                  right: 32,
                ),
                child: help
                    ? Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 40,
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                              setState(() => help = false);
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  border: Border.all(color: primaryColor),
                                  borderRadius: BorderRadius.circular(100)),
                              padding: EdgeInsets.all(10),
                              child: Icon(Icons.arrow_back),
                            ),
                          ),
                          SizedBox(
                            height: 53,
                          ),
                          Center(
                            child: Text(appCubit.getWord('support'),
                                style: GoogleFonts.montserrat(
                                    fontSize: 16, fontWeight: FontWeight.w700)),
                          ),
                          BlocBuilder<HelpPhoneCubit, HelpPhoneState>(
                            builder: (context, state) {
                              if (!(state is HelpPhoneLoaded))
                                return SizedBox.shrink();
                              final phones =
                                  (state as HelpPhoneLoaded).helpPhones;
                              return ListView.builder(
                                itemBuilder: (context, index) => Column(
                                  children: [
                                    SizedBox(
                                      height: 12,
                                    ),
                                    HelpItem(
                                      helpPhone: phones[index],
                                    ),
                                  ],
                                ),
                                itemCount: phones.length,
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                              );
                            },
                          )
                        ],
                      )
                    : Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 53,
                          ),
                          BlocBuilder<UserCubit, UserState>(
                            builder: (context, state) {
                              return Row(
                                children: [
                                  CircleAvatar(
                                    minRadius: 25,
                                    foregroundImage: NetworkImage((state
                                                    as UserLoaded)
                                                .user
                                                .avatar !=
                                            null
                                        ? (state as UserLoaded).user.avatar
                                        : 'https://img2.freepng.ru/20180920/yko/kisspng-computer-icons-portable-network-graphics-avatar-ic-5ba3c66df14d32.3051789815374598219884.jpg'),
                                  ),
                                  SizedBox(width: 10),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text('${(state as UserLoaded).user.name}',
                                          style: GoogleFonts.montserrat(
                                              fontSize: 17)),
                                      SizedBox(
                                        height: 8,
                                      ),
                                      Row(
                                        children: [
                                          Icon(
                                            Icons.star,
                                            color: starColor,
                                            size: 20,
                                          ),
                                          SizedBox(
                                            width: 2,
                                          ),
                                          Text(
                                            (state as UserLoaded).user.rate ??
                                                '5.0',
                                            style: GoogleFonts.montserrat(
                                                fontSize: 12, color: greyText),
                                          )
                                        ],
                                      ),
                                    ],
                                  )
                                ],
                              );
                            },
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: Divider(
                              color: dividerSecondaryColor,
                              thickness: 1.0,
                            ),
                          ),
                          BlocBuilder<UserCubit, UserState>(
                              builder: (context, state) {
                            return state is UserLoaded
                                ? Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        '${colors[state.user.driver.carColor]} ${state.user.driver.carModel}',
                                        style: GoogleFonts.montserrat(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500,
                                            color:
                                                Color.fromRGBO(66, 66, 66, 1)),
                                      ),
                                      SizedBox(
                                        height: 8,
                                      ),
                                      CarIdWidget(
                                        carId: state.user.driver.carId,
                                      ),
                                    ],
                                  )
                                : SizedBox.shrink();
                          }),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: Divider(
                              color: dividerColor,
                              thickness: 1.0,
                            ),
                          ),
                          BlocBuilder<UserCubit, UserState>(
                            builder: (context, state) {
                              return state is UserLoaded
                                  ? Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 8.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Text(
                                            'Активный',
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline5,
                                          ),
                                          CupertinoSwitch(
                                            value: state.user.driver.isActive,
                                            onChanged: (v) =>
                                                setState(() => value = v),
                                            trackColor: Colors.grey,
                                            activeColor:
                                                Color.fromRGBO(45, 161, 226, 1),
                                          ),
                                        ],
                                      ),
                                    )
                                  : SizedBox.shrink();
                            },
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: Divider(
                              color: dividerColor,
                              thickness: 1.0,
                            ),
                          ),
                          BlocBuilder<UserCubit, UserState>(
                            builder: (context, state) {
                              return state is UserLoaded
                                  ? InkWell(
                                      onTap: () => Navigator.pushNamed(
                                          context, '/driver/balance'),
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 8.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Text(
                                              appCubit.getWord(
                                                'balance',
                                              ),
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline5,
                                            ),
                                            UserBalance(
                                              balance: state.user.driver.balance
                                                  .toString(),
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  : SizedBox.shrink();
                            },
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 5),
                            child: Divider(
                              color: dividerColor,
                              thickness: 1.0,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: IconMenu(
                              action: () {
                                Navigator.pop(context);
                                openSlide();
                              },
                              icon: SvgPicture.asset(
                                  'assets/images/Activity.svg'),
                              text: 'Созданить маршрут',
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: Divider(
                              color: dividerColor,
                              thickness: 1.0,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: IconMenu(
                              action: () => Navigator.pushNamed(
                                  context, '/driver/orders',
                                  arguments: true),
                              icon: SvgPicture.asset(
                                  'assets/images/document.svg'),
                              text: appCubit.getWord('history'),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: Divider(
                              color: dividerColor,
                              thickness: 1.0,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: IconMenu(
                              action: () =>
                                  Navigator.pushNamed(context, '/user/edit'),
                              icon:
                                  SvgPicture.asset('assets/images/setting.svg'),
                              text: appCubit.getWord('settings'),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: Divider(
                              color: dividerColor,
                              thickness: 1.0,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: IconMenu(
                              action: () =>
                                  Navigator.pushNamed(context, '/security'),
                              icon: SvgPicture.asset(
                                  'assets/images/security.svg'),
                              text: appCubit.getWord('security'),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: Divider(
                              color: dividerColor,
                              thickness: 1.0,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: IconMenu(
                              action: () {
                                openChatSlide(context: context);
                              },
                              icon: SvgPicture.asset('assets/images/Swap.svg'),
                              text: appCubit.getWord('support_online'),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: Divider(
                              color: dividerColor,
                              thickness: 1.0,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: IconMenu(
                              action: () {
                                setState(() {
                                  help = true;
                                });
                                Scaffold.of(context).openDrawer();
                              },
                              icon: Icon(
                                Icons.help_outline,
                                size: 25,
                              ),
                              text: appCubit.getWord('accident_services'),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: Divider(
                              color: dividerColor,
                              thickness: 1.0,
                            ),
                          ),
                          BlocBuilder<UserCubit, UserState>(
                            builder: (context, state) {
                              if (state is UserLoaded)
                                return IconMenu(
                                  action: () {
                                    BlocProvider.of<UserCubit>(context)
                                        .becomeDriver(state.user, context,
                                            driverMode: false);
                                  },
                                  icon: SvgPicture.asset(
                                      'assets/images/Profile.svg'),
                                  text: appCubit.getWord('become_user'),
                                );
                              return SizedBox.shrink();
                            },
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: Divider(
                              color: dividerColor,
                              thickness: 1.0,
                            ),
                          ),
                        ],
                      ),
              ),
            ),
          ),
          body: Stack(
            children: [
              YandexMap(
                mapObjects: mapObjects,
                onTrafficChanged: (trafficLevel) {},
                onMapCreated: (YandexMapController yandexMapController) async {
                  _controller = yandexMapController;
                  _controller.toggleTrafficLayer(visible: true);
                  _controller.getUserCameraPosition();

                  _permissionGranted = await location.hasPermission();
                  if (_permissionGranted == PermissionStatus.denied) {
                    _permissionGranted = await location.requestPermission();
                    if (_permissionGranted != PermissionStatus.granted) {
                      return;
                    }
                  }

                  _locationData = await location.getLocation();
                  location.enableBackgroundMode(enable: true);

                  // yandexMapController.move(
                  //     point: Point(
                  //         latitude: _locationData.latitude,
                  //         longitude: _locationData.longitude),
                  //     zoom: 17);

                  // _controller.addPlacemark(Placemark(
                  //     point: Point(
                  //         latitude: _locationData.latitude,
                  //         longitude: _locationData.longitude),
                  //     style: PlacemarkStyle(
                  //         opacity: 1,
                  //         scale: 1.2,
                  //         iconName: 'assets/images/marker.png')));
                  var addresses = await Geocoder.local
                      .findAddressesFromCoordinates(Coordinates(
                          _locationData.latitude, _locationData.longitude));
                  setState(() {
                    addressLine = addresses[0].thoroughfare;
                  });
                  _controller.moveCamera(
                      CameraUpdate.newCameraPosition(
                        CameraPosition(
                            zoom: 17,
                            target: Point(
                                latitude: _locationData.latitude,
                                longitude: _locationData.longitude)),
                      ),
                      animation: MapAnimation());

                  streams.add(location.onLocationChanged
                      .listen((LocationData currentLocation) async {
                    // Use current location

                    BlocProvider.of<OrderCubit>(context).locationData =
                        currentLocation;
                    setState(() {
                      mapObjects.removeWhere(
                          (element) => element.mapId.value == 'geolocation');
                      mapObjects.add(PlacemarkMapObject(
                          opacity: 1,
                          direction: currentLocation.heading,
                          icon: PlacemarkIcon.single(PlacemarkIconStyle(
                              scale: 1.7,
                              rotationType: RotationType.rotate,
                              image: BitmapDescriptor.fromAssetImage(
                                  'assets/images/whitecar.png'))),
                          point: Point(
                              latitude: currentLocation.latitude,
                              longitude: currentLocation.longitude),
                          mapId: MapObjectId('geolocation')));
                    });
                  }));
                },
              ),
              Positioned(
                left: MediaQuery.of(context).size.width / 2.6,
                top: 70,
                child: Center(
                  child: Container(
                    constraints: BoxConstraints(maxWidth: 200),
                    child: Text(
                      'Ваш адрес \n ${addressLine ?? ''}',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
              // Builder(
              //   builder: (BuildContext context) => AnimatedPositioned(
              //     duration: Duration(milliseconds: 100),
              //     left: 45,
              //     top: !showProduct
              //         ? MediaQuery.of(context).size.height / 1.65
              //         : MediaQuery.of(context).size.height / 3,
              //     child: GestureDetector(
              //       onTap: () {
              //         openChatSlide(context: context);
              //       },
              //       child: Container(
              //           width: 50,
              //           height: 50,
              //           decoration: BoxDecoration(
              //               borderRadius: BorderRadius.circular(100),
              //               gradient: LinearGradient(
              //                   colors: [Colors.red, Colors.black])),
              //           child: Center(
              //               child: Text(
              //             'SOS',
              //             style: GoogleFonts.montserrat(color: Colors.white),
              //           ))),
              //     ),
              //   ),
              // ),
              AnimatedPositioned(
                duration: Duration(milliseconds: 100),
                right: 16,
                bottom: !showProduct
                    ? MediaQuery.of(context).size.height / 3.05
                    : MediaQuery.of(context).size.height / 3,
                child: InkWell(
                  onTap: () async {
                    _locationData = await location.getLocation();

                    var addresses = await Geocoder.local
                        .findAddressesFromCoordinates(Coordinates(
                            _locationData.latitude, _locationData.longitude));
                    setState(() {
                      addressLine = addresses[0].thoroughfare;
                    });
                    location.getLocation().then((value) {
                      _controller.moveCamera(
                          CameraUpdate.newCameraPosition(CameraPosition(
                            zoom: 17,
                            target: Point(
                                latitude: value.latitude,
                                longitude: value.longitude),
                          )),
                          animation: MapAnimation());

                      setState(() {
                        mapObjects.removeWhere(
                            (element) => element.mapId.value == 'geolocation');
                        mapObjects.add(PlacemarkMapObject(
                            opacity: 1,
                            direction: value.heading,
                            icon: PlacemarkIcon.single(PlacemarkIconStyle(
                                scale: 1.7,
                                rotationType: RotationType.rotate,
                                image: BitmapDescriptor.fromAssetImage(
                                    'assets/images/whitecar.png'))),
                            point: Point(
                                latitude: value.latitude,
                                longitude: value.longitude),
                            mapId: MapObjectId('geolocation')));
                      });
                    });

                    streams.add(location.onLocationChanged.listen((event) {
                      setState(() {
                        mapObjects.removeWhere(
                            (element) => element.mapId.value == 'geolocation');

                        mapObjects.add(PlacemarkMapObject(
                            opacity: 1,
                            direction: event.heading,
                            icon: PlacemarkIcon.single(PlacemarkIconStyle(
                                scale: 1.7,
                                rotationType: RotationType.rotate,
                                image: BitmapDescriptor.fromAssetImage(
                                    'assets/images/whitecar.png'))),
                            point: Point(
                                latitude: event.latitude,
                                longitude: event.longitude),
                            mapId: MapObjectId('geolocation')));
                      });
                    }));
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        gradient: LinearGradient(
                            colors: [primaryColor, primaryColor])),
                    child: Icon(
                      Icons.navigation_outlined,
                      color: Colors.white,
                      size: 30,
                    ),
                  ),
                ),
              ),

              Builder(
                  builder: (_) => Container(
                        alignment: Alignment.topLeft,
                        margin: EdgeInsets.only(top: 60, left: 45),
                        child: GestureDetector(
                          onTap: () => Scaffold.of(_).openDrawer(),
                          child: Container(
                            padding: EdgeInsets.symmetric(
                              vertical: 11,
                              horizontal: 11,
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(100),
                                gradient: LinearGradient(
                                    colors: [Colors.white, Colors.white])),
                            child: Icon(
                              Icons.menu,
                              color: Colors.black,
                              size: 30,
                            ),
                          ),
                        ),
                      )),

              BlocConsumer<OrderCubit, OrderState>(
                listener: (context, state) {
                  if (state is OrderInitial) {
                    streams.forEach((element) {
                      element.cancel();
                    });
                  }
                },
                builder: (context, state) {
                  return SlidingUpPanel(
                    isDraggable: state is! OrderTrackCreated,
                    minHeight: MediaQuery.of(context).size.height / 4.0,
                    maxHeight: !(state is OrderTrackChoosing)
                        ? (state is OrderTrackCreated &&
                                state.order.requests.length < 1)
                            ? MediaQuery.of(context).size.height / 2.8
                            : MediaQuery.of(context).size.height / 1.8
                        : MediaQuery.of(context).size.height / 1.3,
                    backdropEnabled: state is OrderTrackChoosing,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(16),
                      topRight: Radius.circular(16),
                    ),
                    onPanelClosed: closeSlide,
                    controller: panelController,
                    onPanelOpened: () {
                      setState(() {
                        showProduct = true;
                      });
                    },
                    panel: Visibility(
                      child: chatSlide
                          ? ChatSlide()
                          : CreateDirection(
                              panelController: panelController,
                              addressLine: addressLine,
                              constructMapTrack: constructTrack,
                              track: null,
                              controller: _controller,
                            ),
                      visible: true,
                    ),
                    renderPanelSheet: true,
                  );
                },
              )
            ],
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    streams.forEach((element) {
      element.cancel();
    });
    super.dispose();
  }

  List animate(List coords) {
    int loopTime = 5;
    int overAllTime = 250;

    List prevElem = coords[0];
    dynamic overAllDistance = 0;
    coords.forEach((element) {
      overAllDistance += getDistance(element, prevElem);

      prevElem = element;
    });

    dynamic animationInterval = overAllDistance / overAllTime * loopTime;

    List smoothCords = generateSmoothCoords(coords, animationInterval);
    int loopTimer = 0;
    int index = 0;

    Timer.periodic(Duration(milliseconds: loopTime), (timer) {
      if (index <= smoothCords.length) {
        List currentMain = smoothCords.take(index).toList();

        setState(() {
          mapObjects.removeWhere((element) => element.mapId.value == 'track');
          mapObjects.add(PolylineMapObject(
              strokeColor: Theme.of(context).accentColor,
              mapId: MapObjectId('track'),
              arcApproximationStep: 1.0,
              polyline: Polyline(
                  points: currentMain
                      .map((e) => Point(latitude: e[0], longitude: e[1]))
                      .toList()
                      .cast<Point>())));
        });
        index++;
        loopTimer += loopTime;
      } else {
        timer.cancel();
      }
    });
  }

  generateSmoothCoords(List coords, interval) {
    List smoothCords = [];
    smoothCords.add(coords[0]);

    for (int i = 1; i < coords.length; i++) {
      dynamic difference = [
        coords[i][0] - coords[i - 1][0],
        coords[i][1] - coords[i - 1][1]
      ];

      num maxAmount = max(
          (difference[0] / interval).abs(), (difference[1] / interval).abs());

      dynamic minDifference = [
        difference[0] / maxAmount,
        difference[1] / maxAmount
      ];

      dynamic lastCoord = coords[i - 1];

      while (maxAmount > 1) {
        lastCoord = [
          lastCoord[0] + minDifference[0],
          lastCoord[1] + minDifference[1]
        ];

        smoothCords.add(lastCoord);
        maxAmount--;
      }
      smoothCords.add(coords[i]);
    }

    return smoothCords;
  }

  getDistance(point1, point2) {
    return sqrt(pow(point2[0] - point1[0], 2) + pow(point2[1] - point1[1], 2));
  }
}
