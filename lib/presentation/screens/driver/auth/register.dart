import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:images_picker/images_picker.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:poputchik/cubit/driver/cubit/driver_cubit.dart';
import 'package:poputchik/presentation/components/main_button.dart';
import 'package:poputchik/presentation/components/main_field.dart';
import 'package:poputchik/presentation/components/modal/camera_choose.dart';
import 'package:poputchik/presentation/components/upload_images.dart';

class DriverRegisterPage extends StatefulWidget {
  const DriverRegisterPage({Key key}) : super(key: key);

  @override
  _DriverRegisterPageState createState() => _DriverRegisterPageState();
}

class _DriverRegisterPageState extends State<DriverRegisterPage> {
  TextEditingController name = new TextEditingController();
  TextEditingController surname = new TextEditingController();
  TextEditingController phone = new TextEditingController();
  TextEditingController password = new TextEditingController();
  TextEditingController car_model = new TextEditingController();
  TextEditingController car_id = new TextEditingController();
  TextEditingController car_color = new TextEditingController();
  TextEditingController password_confirmation = new TextEditingController();
  TextEditingController car_year = new TextEditingController();
  TextEditingController iin = new TextEditingController();
  TextEditingController email = new TextEditingController();
  TextEditingController seats = new TextEditingController();
  var maskFormatter = new MaskTextInputFormatter(
      mask: '+# (###) ###-##-##',
      filter: {"#": RegExp(r'[0-9]')},
      type: MaskAutoCompletionType.lazy);
  List<Media> medias = [];
  List<Media> newMedias = [];
  Media media;
  Future chooseImage({size, multiple, id}) async {
    final res = await ImagesPicker.pick(
        count: size, pickType: PickType.image, language: Language.System);
    if (res != null) {
      setState(() {
        id != null ? newMedias = res : medias = res;
      });
    }
  }

  List<dynamic> colors = [
    {'color': Colors.red, 'name': 'Красный'},
    {'color': Colors.blue, 'name': 'Синий'},
    {'color': Colors.black, 'name': 'Черный'},
    {'color': Colors.white, 'name': 'Белый'},
    {'color': Colors.orange, 'name': 'Оранжевый'},
    {'color': Colors.grey, 'name': 'Серый'},
  ];

  String checkedColor;

  bool checked = false;

  Future takeImage({size, multiple, id}) async {
    final res = await ImagesPicker.openCamera();
    if (res != null)
      setState(() {
        id != null ? newMedias = res : medias = res;
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          MainButton(
            color: Colors.white,
            action: () => checked
                ? BlocProvider.of<DriverCubit>(context).registerDriver(form: {
                    'name': name.text,
                    'password': password.text,
                    'surname': surname.text,
                    'phone': phone.text,
                    'car_color': car_color.text,
                    'car_year': car_year.text,
                    'car_model': car_model.text,
                    'iin': iin.text,
                    'seats': seats.text,
                    'driver_id_images': medias.length != 0 ? medias : [],
                    'images': newMedias.length != 0 ? newMedias : [],
                    'car_id': car_id.text
                  }, context: context)
                : {},
            side: BorderSide(color: Color.fromRGBO(45, 139, 226, 1)),
            radius: 8.0,
            width: MediaQuery.of(context).size.width - 40,
            child: Text(
              'Дальше',
              textAlign: TextAlign.center,
              style: GoogleFonts.montserrat(
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(45, 139, 226, 1)),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Center(
            child: TextButton(
                onPressed: () => Navigator.pushNamed(context, '/user/login'),
                child: Text(
                  'Вход',
                  style: GoogleFonts.montserrat(
                      fontSize: 15,
                      fontWeight: FontWeight.w500,
                      color: Colors.white),
                )),
          ),
        ],
      ),
      body: BlocBuilder<DriverCubit, DriverState>(
        builder: (context, state) {
          return Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/background.jpg'),
                fit: BoxFit.cover,
              ),
            ),
            child: SingleChildScrollView(
              physics: NeverScrollableScrollPhysics(),
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 40),
                constraints: BoxConstraints(
                    minHeight: MediaQuery.of(context).size.height + 200),
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        child: Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(bottom: 13),
                              child: IconButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                icon: Icon(Icons.arrow_back_ios),
                                color: Colors.white,
                                iconSize: 18,
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Регистрация',
                                  style: GoogleFonts.montserrat(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.white),
                                ),
                                Text(
                                  'Личные данные водителя',
                                  style: GoogleFonts.montserrat(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.white),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      MainTextField(
                        controller: name,
                        text: 'Введите имя',
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      MainTextField(
                        controller: surname,
                        text: 'Введите фамилию',
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      MainTextField(
                        controller: phone,
                        inputFormatter: [maskFormatter],
                        text: 'Введите номер телефона',
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      MainTextField(
                        controller: iin,
                        text: 'Введите ваш ИИН',
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      MainTextField(
                        obscureText: true,
                        controller: password,
                        text: 'Придумайте пароль',
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      MainTextField(
                        obscureText: true,
                        controller: password_confirmation,
                        text: 'Повторите пароль',
                      ),
                      SizedBox(
                        height: 15,
                      ),

                      GestureDetector(
                        onTap: () => showDialog(
                            context: context,
                            builder: (_) => AlertDialog(
                                    content: DialogCameraChoose(
                                  firstAction: () {
                                    takeImage(size: 3, multiple: true)
                                        .then((value) => Navigator.pop(_));
                                  },
                                  secondAction: () {
                                    chooseImage(size: 3, multiple: true)
                                        .then((value) => Navigator.pop(_));
                                  },
                                ))),
                        child: UploadImages(
                          medias: medias,
                          margin: EdgeInsets.only(bottom: 13),
                          text: Text(
                            'Загрузите ваше фото',
                            style: GoogleFonts.montserrat(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: Colors.white),
                          ),
                        ),
                      ),

                      // MainTextField(
                      //   controller: car_model,
                      //   text: 'Введите модель машины',
                      // ),

                      // MainTextField(
                      //   controller: car_year,
                      //   text: 'Введите год машины',
                      // ),
                      // MainTextField(
                      //   controller: seats,
                      //   text: 'Укажите количество мест в транспорте',
                      // ),

                      // MainTextField(
                      //   controller: car_id,
                      //   text: 'Введите гос номер машины',
                      // ),
                      // GestureDetector(
                      //   onTap: () => showDialog(
                      //       context: context,
                      //       builder: (_) => AlertDialog(
                      //               content: DialogCameraChoose(
                      //             firstAction: () {
                      //               takeImage(size: 3, multiple: true)
                      //                   .then((value) => Navigator.pop(_));
                      //             },
                      //             secondAction: () {
                      //               chooseImage(size: 3, multiple: true)
                      //                   .then((value) => Navigator.pop(_));
                      //             },
                      //           ))),
                      //   child: UploadImages(
                      //     medias: medias,
                      //     margin: EdgeInsets.only(bottom: 13),
                      //     text: Text(
                      //       'Загрузите фото водительских прав',
                      //     style: GoogleFonts.montserrat(
                      //         fontSize: 16,
                      //         fontWeight: FontWeight.w400,
                      //         color: Colors.white)
                      //     ),
                      //   ),
                      // ),
                      // GestureDetector(
                      //   onTap: () => showDialog(
                      //       context: context,
                      //       builder: (_) => AlertDialog(
                      //             content: DialogCameraChoose(firstAction: () {
                      //               takeImage(size: 3, multiple: true, id: true)
                      //                   .then((value) => Navigator.pop(_));
                      //             }, secondAction: () {
                      //               chooseImage(size: 3, multiple: true, id: true)
                      //                   .then((value) => Navigator.pop(_));
                      //             }),
                      //           )),
                      //   child: UploadImages(
                      //     medias: newMedias,
                      //     margin: EdgeInsets.only(bottom: 13),
                      //     text: Text(
                      //       'Загрузите фото машины',
                      //     style: GoogleFonts.montserrat(
                      //         fontSize: 16,
                      //         fontWeight: FontWeight.w400,
                      //         color: Colors.white)
                      //     ),
                      //   ),
                      // ),

                      // SizedBox(
                      //   height: 20,
                      // ),
                      // GestureDetector(
                      //   onTap: () {
                      //     setState(() {
                      //       checked = !checked;
                      //     });
                      //   },
                      //   child: Container(
                      //     margin: EdgeInsets.symmetric(horizontal: 10),
                      //     width: MediaQuery.of(context).size.width -90,
                      //     child: Row(

                      //       children: [
                      //         Container(
                      //           width: 25,
                      //           height: 25,
                      //            child: checked
                      //               ? Center(
                      //                   child: Icon(Icons.check,color: Colors.white,),
                      //                 )
                      //               : SizedBox.shrink(),
                      //           decoration: BoxDecoration(
                      //             color:Color.fromRGBO(45, 161, 226, 1),
                      //               border: Border.all(color: Color.fromRGBO(45, 161, 226, 1),),
                      //               borderRadius: BorderRadius.circular(5)),
                      //         ),
                      //         SizedBox(
                      //   width: 15,
                      // ),
                      //         Container(
                      //           width: MediaQuery.of(context).size.width / 2,
                      //           child: Text(

                      //               'Я соглашаюсь с Политикой конфиденциальностью', style: GoogleFonts.montserrat(
                      //           fontSize: 14,
                      //           height: 1.5,
                      //           fontWeight: FontWeight.w400,
                      //           color: Colors.white),
                      //     ),
                      //         )
                      //       ],
                      //     ),
                      //   ),
                      // ),
                      SizedBox(
                        height: 400,
                      ),
                      // MainGradientButton(
                      //     action: () => checked && (state is DriverInitial)
                      //         ? BlocProvider.of<DriverCubit>(context)
                      //             .registerDriver(form: {
                      //             'name': name.text,
                      //             'password': password.text,
                      //             'surname': surname.text,
                      //             'phone': phone.text,
                      //             'car_color': car_color.text,
                      //             'car_year': car_year.text,
                      //             'car_model': car_model.text,
                      //             'iin': iin.text,
                      //             'seats': seats.text,
                      //             'driver_id_images':
                      //                 medias.length != 0 ? medias : [],
                      //             'images':
                      //                 newMedias.length != 0 ? newMedias : [],
                      //             'car_id': car_id.text
                      //           }, context: context)
                      //         : {},
                      //     colors: checked && (state is DriverInitial)
                      //         ? [secondColor, primaryColor]
                      //         : [secondColor[100], primaryColor],
                      //     side: Border.all(width: 0),
                      //     radius: 8.0,
                      //     width: MediaQuery.of(context).size.width - 144,
                      //     child: Text('Зарегистрироваться',
                      //         textAlign: TextAlign.center,
                      //         style: GoogleFonts.montserrat(
                      //             fontSize: 15,
                      //             fontWeight: FontWeight.w500,
                      //             color: Colors.white)),
                      //     padding: EdgeInsets.symmetric(vertical: 15)),
                      // SizedBox(
                      //   height: 10,
                      // ),
                      // MainButton(
                      //     action: () =>
                      //         Navigator.pushNamed(context, '/driver/login'),
                      //     side: BorderSide(color: Colors.black),
                      //     radius: 8.0,
                      //     width: MediaQuery.of(context).size.width - 144,
                      //     child: Text('Войти',
                      //         style: GoogleFonts.montserrat(
                      //             fontSize: 15, fontWeight: FontWeight.w500))),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
