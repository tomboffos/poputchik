import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poputchik/cubit/user/cubit/user_cubit.dart';
import 'package:poputchik/data/constant.dart';
import 'package:poputchik/data/models/user.dart';
import 'package:poputchik/presentation/components/main_field.dart';
import 'package:poputchik/presentation/components/main_gradient_button.dart';

class CheckCode extends StatefulWidget {
  final bool driver;
  final User user;
  const CheckCode({Key key, this.driver, this.user}) : super(key: key);

  @override
  State<CheckCode> createState() => _CheckCodeState();
}

class _CheckCodeState extends State<CheckCode> {
  TextEditingController code = new TextEditingController();

  bool checked = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        child: BlocBuilder<UserCubit, UserState>(
          builder: (context, state) {
            if (state is UserLoading)
              return Container(
                height: MediaQuery.of(context).size.height,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Center(
                      child: CircularProgressIndicator(),
                    ),
                  ],
                ),
              );
            return Container(
              margin: EdgeInsets.symmetric(horizontal: 72),
              constraints:
                  BoxConstraints(minHeight: MediaQuery.of(context).size.height),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      child: Text('Введите четырех значный код ',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.montserrat(
                              fontSize: 20,
                              fontWeight: FontWeight.w700,
                              color: Colors.black)),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    MainTextField(
                      controller: code,
                      text: 'Введите код',
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    MainGradientButton(
                        action: () => BlocProvider.of<UserCubit>(context)
                            .checkCode(
                                code.text, widget.user, context, widget.driver),
                        colors: code.text != ''
                            ? [Colors.white, Colors.white]
                            : [Colors.white, Colors.white],
                        side: Border.all(width: 1, color: primaryColor),
                        radius: 8.0,
                        width: MediaQuery.of(context).size.width - 144,
                        child: Text(
                          'Далее',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.montserrat(
                              fontSize: 15,
                              fontWeight: FontWeight.w500,
                              color: primaryColor),
                        ),
                        padding: EdgeInsets.symmetric(vertical: 15)),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
