import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poputchik/cubit/app/cubit/app_cubit.dart';
import 'package:poputchik/data/constant.dart';
import 'package:poputchik/data/models/order.dart';
import 'package:poputchik/presentation/components/buttons/social_button.dart';
import 'package:poputchik/presentation/components/passenger_item.dart';
import 'package:poputchik/presentation/components/passenger_star_rating.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:sizer/sizer.dart';

class OrderDriverInfo extends StatelessWidget {
  final Order order;
  const OrderDriverInfo({
    Key key,
    this.order,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final appCubit = BlocProvider.of<AppCubit>(context);
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: 16,
      ),
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        color: userBackground,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        children: [
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: CircleAvatar(
                        radius: 22,
                        backgroundImage: NetworkImage(
                            'https://img2.freepng.ru/20180920/yko/kisspng-computer-icons-portable-network-graphics-avatar-ic-5ba3c66df14d32.3051789815374598219884.jpg'),
                      ),
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '${order.driver.name}',
                          style: GoogleFonts.openSans(
                              fontSize: 16, fontWeight: FontWeight.w800),
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        PassengerStarRating(
                          rate: double.parse(order.driver.rating.toString())
                              .toInt()
                              .toString()
                              .padRight(2, '.0'),
                        ),
                      ],
                    ),
                  ],
                ),
                Row(
                  children: [
                    IconButton(
                      color: Color(0xff56C883),
                      onPressed: () async {
                        await launch(
                            'https://wa.me/${order.driver.phone.replaceAll('+', '').replaceAll('(', '').replaceAll(')', '').replaceAll(' ', '').replaceAll('-', '')}');
                      },
                      icon: SvgPicture.asset('assets/images/whatsapp.svg'),
                    ),
                    IconButton(
                      onPressed: () async {
                        await launch(
                            'tel:${order.driver.phone.replaceAll('(', '').replaceAll(')', '').replaceAll(' ', '').replaceAll('-', '')}');
                      },
                      color: Color(0xff5676C8),
                      icon: SvgPicture.asset('assets/images/call.svg'),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 16),
            child: Divider(
              color: dividerSecondaryColor,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Text(
                    '${order.driver.carColor}',
                    style: GoogleFonts.openSans(
                        fontSize: 13,
                        fontWeight: FontWeight.w300,
                        color: Color(0xff313131)),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Text(
                    '${order.driver.carModel}',
                    style: GoogleFonts.openSans(
                        fontSize: 13,
                        fontWeight: FontWeight.w300,
                        color: Color(0xff313131)),
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 8),
                decoration: BoxDecoration(
                  color: greyCarNumber,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Text(
                  '${order.driver.carId}',
                  style: GoogleFonts.openSans(
                      fontSize: 13,
                      fontWeight: FontWeight.w300,
                      color: Color(0xff313131)),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
