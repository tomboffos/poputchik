import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:images_picker/images_picker.dart';
import 'package:poputchik/cubit/chat/cubit/chat_cubit.dart';
import 'package:poputchik/data/constant.dart';
import 'package:poputchik/data/models/message.dart';

class ChatSlide extends StatefulWidget {
  final List<Message> messages;
  const ChatSlide({Key key, this.messages}) : super(key: key);

  @override
  State<ChatSlide> createState() => _ChatSlideState();
}

class _ChatSlideState extends State<ChatSlide> {
  TextEditingController message = new TextEditingController();

  List<Media> images = [];

  void chooseImages() async {
    final res = await ImagesPicker.pick(
        count: 3, pickType: PickType.image, language: Language.System);

    if (res != null) {
      setState(() {
        images = res;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          BlocBuilder<ChatCubit, ChatState>(
            builder: (context, state) {
              return Container(
                height: images.length != 0
                    ? MediaQuery.of(context).size.height * 0.20
                    : MediaQuery.of(context).size.height * 0.35,
                child: state is ChatLoaded
                    ? ListView.builder(
                        scrollDirection: Axis.vertical,
                        reverse: true,
                        itemBuilder: (context, index) => Container(
                          child: Row(
                            mainAxisAlignment: state.messages[index].isMine
                                ? MainAxisAlignment.end
                                : MainAxisAlignment.start,
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 30),
                                margin: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 10),
                                decoration: BoxDecoration(
                                    color: Colors.red.withOpacity(0.5),
                                    borderRadius: BorderRadius.circular(15)),
                                child: Column(
                                  children: [
                                    state
                                                .messages[index]
                                                .messageContent['images']
                                                .length !=
                                            0
                                        ? Wrap(
                                            children: state.messages[index]
                                                .messageContent['images']
                                                .toList()
                                                .cast<String>()
                                                .map((image) => Container(
                                                    height: 100,
                                                    width: 100,
                                                    margin:
                                                        EdgeInsets.symmetric(
                                                            horizontal: 5),
                                                    decoration: BoxDecoration(
                                                        image: DecorationImage(
                                                            image: NetworkImage(
                                                                image),
                                                            fit:
                                                                BoxFit.cover))))
                                                .toList()
                                                .cast<Widget>(),
                                          )
                                        : SizedBox.shrink(),
                                    state.messages[index]
                                                .messageContent['text'] !=
                                            null
                                        ? Text(state.messages[index]
                                            .messageContent['text']
                                            .toString())
                                        : SizedBox.shrink()
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        itemCount: state.messages.length,
                      )
                    : Container(
                        color: Colors.red,
                      ),
              );
            },
          ),
          if (images.length != 0) ...[
            Row(
              children: [
                Container(
                    height: 100,
                    width: MediaQuery.of(context).size.width - 40,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) => Container(
                        margin: EdgeInsets.all(20),
                        width: 80,
                        height: 80,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: AssetImage(images[index].path))),
                      ),
                      itemCount: images.length,
                    ))
              ],
            )
          ],
          Container(
            padding: EdgeInsets.symmetric(horizontal: 14),
            decoration: BoxDecoration(
                border: Border(
                    top:
                        BorderSide(color: Color(0xff313131).withOpacity(0.5)))),
            height: 50,
            child: TextField(
              onSubmitted: (value) {
                BlocProvider.of<ChatCubit>(context)
                    .sendMessages(images, message.text);
                setState(() {
                  images = [];
                  message.text = '';
                });
              },
              controller: message,
              decoration: InputDecoration(
                  hintText: 'Начать разговор...',
                  hintStyle: GoogleFonts.montserrat(
                      fontSize: 14, fontWeight: FontWeight.w500),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.transparent)),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.transparent))),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 100),
            padding: EdgeInsets.symmetric(horizontal: 25),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Text('Aa',
                        style: GoogleFonts.montserrat(
                            fontSize: 14, color: primaryColor)),
                    SizedBox(
                      width: 10,
                    ),
                    GestureDetector(
                      child:
                          Icon(Icons.picture_in_picture, color: primaryColor),
                      onTap: () => chooseImages(),
                    )
                  ],
                ),
                GestureDetector(
                  onTap: () {
                    BlocProvider.of<ChatCubit>(context)
                        .sendMessages(images, message.text);
                    setState(() {
                      images = [];
                      message.text = '';
                    });
                  },
                  child: Text(
                    'Отправить',
                    style: GoogleFonts.montserrat(
                        fontSize: 17,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff313131).withOpacity(0.2)),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
