import 'package:flutter/material.dart';
import 'package:poputchik/data/constant.dart';

class UserBalance extends StatelessWidget {
  final String balance;
  const UserBalance({Key key, this.balance}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 4,
        horizontal: 16,
      ),
      child: Text(
        balance,
        style: Theme.of(context).textTheme.bodyText1.copyWith(
              color: Colors.white,
            ),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100),
        gradient: LinearGradient(
          colors: [
            Color.fromRGBO(45, 161, 226, 1),
            Color.fromRGBO(87, 178, 229, 1),
            Color.fromRGBO(21, 81, 115, 1),

          
          ],
        ),
      ),
    );
  }
}
