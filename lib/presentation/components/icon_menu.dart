import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class IconMenu extends StatelessWidget {
  final action;
  final Widget icon;
  final String text;
  final Color color;
  const IconMenu({
    Key key,
    this.icon,
    this.text,
    this.action,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: action,
      child: Row(
        children: [
          icon,
          SizedBox(
            width: 16,
          ),
          Text(text,
              style: GoogleFonts.montserrat(
                  fontSize: 17,
                  fontWeight: FontWeight.w400,
                  color: color ?? null))
        ],
      ),
    );
  }
}
