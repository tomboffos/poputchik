import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poputchik/cubit/app/cubit/app_cubit.dart';
import 'package:poputchik/cubit/order/cubit/order_cubit.dart';
import 'package:poputchik/data/constant.dart';
import 'package:poputchik/data/models/order.dart';
import 'package:poputchik/data/models/order_request.dart';
import 'package:poputchik/presentation/components/main_button.dart';
import 'package:poputchik/presentation/components/passenger_star_rating.dart';
import 'package:url_launcher/url_launcher.dart';

class PassengerItem extends StatelessWidget {
  final OrderRequest orderRequest;
  final Order order;

  const PassengerItem({
    Key key,
    @required this.appCubit,
    this.orderRequest,
    this.order,
  }) : super(key: key);

  final AppCubit appCubit;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        color: userBackground,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        children: [
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      child: CircleAvatar(
                        radius: 22,
                        backgroundImage: NetworkImage(orderRequest
                                .user.avatar ??
                            'https://img2.freepng.ru/20180920/yko/kisspng-computer-icons-portable-network-graphics-avatar-ic-5ba3c66df14d32.3051789815374598219884.jpg'),
                      ),
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          child: Text(
                            '${orderRequest.user.name}',
                            style:
                                Theme.of(context).textTheme.headline5.copyWith(
                                      fontWeight: FontWeight.w600,
                                    ),
                          ),
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        PassengerStarRating(
                          rate: orderRequest.user.rate
                              .toInt()
                              .toString()
                              .padRight(2, '.0'),
                        )
                      ],
                    ),
                  ],
                ),
                Row(
                  children: [
                    IconButton(
                      color: Color(0xff56C883),
                      onPressed: () async {
                        await launch(
                            'https://wa.me/${orderRequest.user.phone.replaceAll('+', '').replaceAll('(', '').replaceAll(')', '').replaceAll(' ', '').replaceAll('-', '')}');
                      },
                      icon: SvgPicture.asset('assets/images/whatsapp.svg'),
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    IconButton(
                        onPressed: () async {
                          await launch(
                              'tel:${orderRequest.user.phone.replaceAll('(', '').replaceAll(')', '').replaceAll(' ', '').replaceAll('-', '')}');
                        },
                        color: Color(0xff5676C8),
                        icon: SvgPicture.asset('assets/images/call.svg')),
                  ],
                )
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 16),
            child: Divider(
              color: dividerSecondaryColor,
            ),
          ),
          Row(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    appCubit.getWord('status'),
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: 8,
                    ),
                    child: Row(
                      children: [
                        CircleAvatar(
                          minRadius: 5,
                          backgroundColor: Theme.of(context).dividerColor,
                        ),
                        SizedBox(
                          width: 12,
                        ),
                        Text(
                          orderRequest.orderRequestStatus.name,
                          style: Theme.of(context).textTheme.subtitle1,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                appCubit.getWord('price'),
                style: Theme.of(context).textTheme.headline5,
              ),
              Text(
                "${orderRequest.price} T",
                style: Theme.of(context).textTheme.headline5,
              )
            ],
          ),
          if (orderRequest.comments != null)
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  child: Text(
                    '${orderRequest.comments}',
                    style: GoogleFonts.montserrat(
                      fontSize: 15,
                      fontWeight: FontWeight.w500,
                      color: Color(0xffBBBBBB),
                    ),
                  ),
                ),
              ],
            ),
          SizedBox(
            height: 16,
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                if (orderRequest.orderRequestStatus.id != 8)
                  MainButton(
                      color: Colors.white,
                      action: () => BlocProvider.of<OrderCubit>(context)
                          .updateRequest(orderRequest, order,
                              {'order_request_status_id': '3'}),
                      side: BorderSide(color: Theme.of(context).errorColor),
                      radius: 8.0,
                      width: MediaQuery.of(context).size.width / 2.5,
                      child: Text('${appCubit.getWord('cancel')}',
                          style: GoogleFonts.montserrat(
                            fontSize: 15,
                            fontWeight: FontWeight.w500,
                            color: Theme.of(context).errorColor,
                          ))),
                if (orderRequest.orderRequestStatus.id == 2 ||
                    orderRequest.orderRequestStatus.id == 5 ||
                    orderRequest.orderRequestStatus.id == 7 ||
                    orderRequest.orderRequestStatus.id == 9)
                  MainButton(
                      action: () =>
                          BlocProvider.of<OrderCubit>(context).updateRequest(
                              orderRequest,
                              order,
                              {
                                'order_request_status_id': orderRequest
                                            .orderRequestStatus.id ==
                                        5
                                    ? '2'
                                    : orderRequest.orderRequestStatus.id == 7
                                        ? '9'
                                        : orderRequest.orderRequestStatus.id ==
                                                9
                                            ? '8'
                                            : '7'
                              },
                              context: context,
                              appCubit: BlocProvider.of<AppCubit>(context)),
                      side: BorderSide(color: Theme.of(context).primaryColor),
                      color: Colors.white,
                      radius: 8.0,
                      width: MediaQuery.of(context).size.width / 2.5,
                      child: Text(
                          orderRequest.orderRequestStatus.id == 5
                              ? appCubit.getWord('accept')
                              : orderRequest.orderRequestStatus.id == 7
                                  ? appCubit.getWord('take_him')
                                  : orderRequest.orderRequestStatus.id == 9
                                      ? appCubit.getWord('brought_him')
                                      : appCubit.getWord('arrived'),
                          style: GoogleFonts.montserrat(
                              fontSize: 15,
                              fontWeight: FontWeight.w500,
                              color: Theme.of(context).primaryColor))),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
