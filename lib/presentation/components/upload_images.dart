import 'package:flutter/material.dart';
import 'package:images_picker/images_picker.dart';

class UploadImages extends StatelessWidget {
  final EdgeInsets margin;
  final Text text;
  final List<Media> medias;

  const UploadImages({Key key, this.margin, this.text, this.medias})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width - 80,
      margin: margin,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: text,
            margin: EdgeInsets.only(left: 0, bottom: 10),
          ),
          Container(
            height: 50,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) => Container(
                width: 50,
                margin: EdgeInsets.only(right: 12),
                decoration: medias.length == 0
                    ? BoxDecoration(
                      color: Colors.white,
                         
                        borderRadius: BorderRadius.circular(10))
                    : BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(medias[index].path),
                            fit: BoxFit.cover),
                       
                        borderRadius: BorderRadius.circular(5)),
                child: medias.length == 0 ? Icon(Icons.add,color: Color.fromRGBO(45, 96, 226, 1),) : SizedBox.shrink(),
              ),
              itemCount: medias.length == 0 ? 1 : medias.length,
            ),
          )
        ],
      ),
    );
  }
}
