import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MainTextField extends StatelessWidget {
  final String text;
  final TextEditingController controller;
  final inputFormatter;
  final obscureText;
  const MainTextField(
      {Key key,
      this.text,
      this.controller,
      this.inputFormatter,
      this.obscureText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      child: TextField(
        obscureText: obscureText != null ? obscureText : false,
        inputFormatters: inputFormatter,
        controller: controller,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          focusColor: Colors.white,
          hintText: text,
          hintStyle: GoogleFonts.montserrat(
              fontSize: 14, color: Color(0xff313131).withOpacity(0.5)),
          contentPadding: EdgeInsets.symmetric(horizontal: 16, vertical: 13),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Color(0xff757575)),
              borderRadius: BorderRadius.circular(8)),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Color(0xff757575)),
              borderRadius: BorderRadius.circular(8)),
        ),
      ),
    );
  }
}
