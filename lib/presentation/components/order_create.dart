import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poputchik/cubit/app/cubit/app_cubit.dart';
import 'package:poputchik/cubit/order_request/cubit/order_request_cubit.dart';
import 'package:poputchik/cubit/track/cubit/track_cubit.dart';
import 'package:poputchik/data/constant.dart';
import 'package:poputchik/data/models/order_request.dart';
import 'package:poputchik/data/models/track.dart';
import 'package:poputchik/presentation/components/loading_passengers.dart';
import 'package:poputchik/presentation/components/main_button.dart';
import 'package:poputchik/presentation/components/modal/cancel_modal.dart';
import 'package:poputchik/presentation/components/order_driver_info.dart';
import 'package:poputchik/presentation/components/price_button.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class OrderCreate extends StatefulWidget {
  final Track track;
  final action;
  final addressLine;
  final YandexMapController controller;
  final chooseMainHistory;
  final PanelController panelController;
  final chooseTrack;
  final int customPrice;
  const OrderCreate(
      {Key key,
      this.track,
      this.action,
      this.controller,
      this.addressLine,
      this.chooseMainHistory,
      this.panelController,
      this.chooseTrack,
      this.customPrice,
      constructMap})
      : super(key: key);

  @override
  _OrderCreateState createState() => _OrderCreateState();
}

class _OrderCreateState extends State<OrderCreate> {
  String name = 'Kaspi Gold';
  TextEditingController comments = new TextEditingController();

  bool ordered = false;
  bool custom = false;
  int counter = 0;
  int seats = 1;

  bool comfort = false;

  dialogCancel(context, OrderRequest order) async {
    setState(() {
      ordered = !ordered;
    });
    if (!ordered)
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return CancelDialog(
              action: () => BlocProvider.of<OrderRequestCubit>(context)
                  .cancelOrder(orderId: order.id, context: context),
            );
          });
  }

  Track track;
  int time = 180;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    track = widget.track;
  }

  @override
  Widget build(BuildContext context) {
    final appCubit = BlocProvider.of<AppCubit>(context);
    return Container(
      child: BlocConsumer<OrderRequestCubit, OrderRequestState>(
        listener: (context, state) {
          if (state is OrderRequestCreated && state.time != null) {
            setState(() {
              time = 180;
            });
            Timer.periodic(Duration(seconds: 1), (timer) {
              setState(() {
                time--;
              });
              if (time <= 0) {
                timer.cancel();
              }
            });
          }
        },
        builder: (context, state) {
          return Column(
            children: [
              GestureDetector(
                onTap: () {
                  if (state is! OrderRequestCreated) {
                    BlocProvider.of<OrderRequestCubit>(context)
                        .emit(OrderRequestChoosing());
                    BlocProvider.of<TrackCubit>(context).getTracks(query: '');
                    widget.panelController.open();
                  }
                },
                child: Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(16),
                          topRight: Radius.circular(16),
                        ),
                        color: primaryColor,
                      ),
                      padding: EdgeInsets.only(
                          top: 8, left: 15, right: 15, bottom: 15),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                              top: 4.0,
                            ),
                            child:
                                SvgPicture.asset('assets/icons/pointwhite.svg'),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                appCubit.getWord('geolocation'),
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1
                                    .copyWith(
                                      color: Theme.of(context).hintColor,
                                    ),
                              ),
                              Text(
                                widget.track != null
                                    ? widget.track.startName
                                    : widget.addressLine ?? '',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline5
                                    .copyWith(
                                      color: whiteColor,
                                    ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    if (state is! OrderRequestCreated)
                      Column(
                        children: [
                          Container(
                            margin: EdgeInsets.symmetric(
                              vertical: 10,
                              horizontal: 15,
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                border: Border.all(
                                  color: greyBorder,
                                )),
                            padding: EdgeInsets.all(10),
                            child: Row(
                              children: [
                                SvgPicture.asset('assets/icons/discovery.svg'),
                                SizedBox(width: 8),
                                !(state is OrderRequestChoosing)
                                    ? Text(
                                        (state is OrderRequestTrackChoosed)
                                            ? state.track.endName
                                            : state is OrderRequestCreated
                                                ? state.track.endName
                                                : appCubit.getWord('where'),
                                        style: GoogleFonts.montserrat(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500))
                                    : Container(
                                        height: 20,
                                        child: TextField(
                                          onChanged: (value) {
                                            BlocProvider.of<TrackCubit>(context)
                                                .getTracks(
                                                    query: value.toString());
                                          },
                                          decoration: InputDecoration(
                                              border: InputBorder.none),
                                        ),
                                        constraints: BoxConstraints(
                                            maxWidth: MediaQuery.of(context)
                                                    .size
                                                    .width -
                                                160),
                                      )
                              ],
                            ),
                          ),
                        ],
                      ),
                  ],
                ),
              ),
              if (state is OrderRequestTrackChoosed)
                Column(
                  children: [
                    Divider(
                      height: 10,
                      thickness: 1,
                      indent: 20,
                      endIndent: 20,
                      color: Color(0xffCBCBCB),
                    ),
                    //Tarif
                    Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.symmetric(
                        vertical: 5,
                        horizontal: 15,
                      ),
                      child: Text(
                        'Ваш тариф:',
                        style: GoogleFonts.montserrat(
                            fontSize: 16, fontWeight: FontWeight.w500),
                      ),
                    ),
                  ],
                ),
              if (state is OrderRequestChoosing)
                Container(
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: greyListBorder,
                      ),
                      borderRadius: BorderRadius.circular(8)),
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  height: MediaQuery.of(context).size.height / 2.0,
                  child: BlocBuilder<TrackCubit, TrackState>(
                    builder: (context, trackState) {
                      if (trackState is TrackInitial) return SizedBox();

                      final tracks = (trackState as TrackLoaded).tracks;
                      return Container(
                        height: MediaQuery.of(context).size.height / 2.0,
                        // padding: EdgeInsets.all(5),
                        child: MediaQuery.removePadding(
                          context: context,
                          removeTop: true,
                          child: ListView.builder(
                            itemBuilder: (context, index) => GestureDetector(
                              onTap: () {
                                widget.chooseTrack(tracks[index]);
                              },
                              child: ListTile(
                                contentPadding:
                                    EdgeInsets.symmetric(horizontal: 15),
                                leading: Icon(Icons.location_on_outlined),
                                title: Text('${tracks[index].endName}'),
                              ),
                            ),
                            itemCount: tracks.length,
                          ),
                        ),
                      );
                    },
                  ),
                ),
              if (state is OrderRequestCreated &&
                  state.time != null &&
                  state.time > 0)
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                          time > 0
                              ? appCubit.getWord('waiting')
                              : appCubit.getWord('wait_end'),
                          style: GoogleFonts.openSans(fontSize: 17)),
                      Text(
                          '${(time ~/ 60).toString().padLeft(2, '0')}:${(time % 60).toString().padLeft(2, '0')}',
                          style: GoogleFonts.openSans(
                              fontSize: 17, color: Color(0xff424242))),
                    ],
                  ),
                  margin: EdgeInsets.symmetric(vertical: 20),
                ),
              if (state is OrderRequestTrackChoosed &&
                  widget.customPrice != null)
                Container(
                  margin: EdgeInsets.only(bottom: 15),
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            custom = !custom;
                          });
                        },
                        child: Container(
                          width: 25,
                          height: 25,
                          child: Center(
                            child: custom ? Icon(Icons.check) : null,
                          ),
                          decoration: BoxDecoration(
                            border: Border.all(color: Color(0xff313131)),
                            borderRadius: BorderRadius.circular(14),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Container(
                        child: Text(
                            '${appCubit.getWord('price_main')} + ${widget.customPrice} тг'),
                      )
                    ],
                  ),
                ),
              !(state is OrderRequestTrackChoosed)
                  ? SizedBox.shrink()
                  : Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: 16,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Expanded(
                            child: GestureDetector(
                              onTap: () => setState(() => comfort = false),
                              child: PriceButton(
                                name: appCubit.getWord('econom'),
                                price: state is OrderRequestTrackChoosed
                                    ? ((state.track.startPrice * seats) +
                                            counter +
                                            (custom ? widget.customPrice : 0))
                                        .toString()
                                    : '800',
                                info: !comfort,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 16,
                          ),
                          Expanded(
                            child: GestureDetector(
                              onTap: () => setState(() => comfort = true),
                              child: PriceButton(
                                  name: appCubit.getWord('comfort'),
                                  price: state is OrderRequestTrackChoosed
                                      ? ((state.track.startPriceComfort *
                                                  seats) +
                                              counter +
                                              (custom ? widget.customPrice : 0))
                                          .toString()
                                      : '1200',
                                  info: comfort),
                            ),
                          ),
                        ],
                      ),
                    ),
              SizedBox(
                height: state is OrderRequestInitial
                    ? 0
                    : state is OrderRequestCreated &&
                            state.request.order == null
                        ? 0
                        : 0,
              ),
              if (state is OrderRequestCreated && state.request.order != null)
                Divider(
                  height: 20,
                  thickness: 1,
                  indent: 20,
                  endIndent: 20,
                  color: Color(0xffCBCBCB),
                ),
              state is OrderRequestCreated
                  ? state.request.order != null &&
                          state.request.orderRequestStatus.id != 5
                      ? OrderDriverInfo(
                          order: (state).request.order,
                        )
                      : LoadingPassengers(
                          appCubit: appCubit,
                          request: true,
                        )
                  : Container(
                      margin: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          if (state is OrderRequestTrackChoosed)
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 10, bottom: 10),
                                  child: Text(
                                    appCubit.getWord('type_count_places'),
                                    style: GoogleFonts.montserrat(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        color: Color(0xff313131)),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.symmetric(
                                    vertical: 0,
                                    horizontal: 10,
                                  ),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    border: Border.all(
                                      color: Color(0xffCBCBCB),
                                    ),
                                  ),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      IconButton(
                                        icon: Icon(
                                          Icons.remove,
                                          color: primaryColor,
                                          size: 28,
                                        ),
                                        onPressed: () => setState(() {
                                          if (seats > 1) seats -= 1;
                                        }),
                                        disabledColor: Colors.black,
                                        focusColor: Colors.black,
                                        color: Colors.black,
                                      ),
                                      SizedBox(
                                        width: 15,
                                      ),
                                      Text('$seats',
                                          style: GoogleFonts.montserrat(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500)),
                                      SizedBox(
                                        width: 15,
                                      ),
                                      IconButton(
                                        icon: Icon(
                                          Icons.add,
                                          color: primaryColor,
                                          size: 28,
                                        ),
                                        onPressed: () =>
                                            setState(() => seats += 1),
                                        disabledColor: Colors.black,
                                        focusColor: Colors.black,
                                        color: Colors.black,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          SizedBox(width: 10),
                          if (state is OrderRequestTrackChoosed)
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 10, bottom: 10),
                                  child: Text(
                                    appCubit.getWord('increase_price'),
                                    style: GoogleFonts.montserrat(
                                        fontSize: 10,
                                        fontWeight: FontWeight.w500,
                                        color: Color(0xff313131)),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.symmetric(
                                    vertical: 0,
                                    horizontal: 10,
                                  ),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    border: Border.all(
                                      color: Color(0xffCBCBCB),
                                    ),
                                  ),
                                  child: Row(
                                    children: [
                                      IconButton(
                                        icon: Icon(
                                          Icons.remove,
                                          color: primaryColor,
                                          size: 28,
                                        ),
                                        onPressed: () => setState(() {
                                          if (counter > 0) counter -= 50;
                                        }),
                                        disabledColor: Colors.black,
                                        focusColor: Colors.black,
                                        color: Colors.black,
                                      ),
                                      SizedBox(
                                        width: 15,
                                      ),
                                      Text('$counter',
                                          style: GoogleFonts.montserrat(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500)),
                                      SizedBox(
                                        width: 15,
                                      ),
                                      IconButton(
                                        icon: Icon(
                                          Icons.add,
                                          color: primaryColor,
                                          size: 28,
                                        ),
                                        onPressed: () =>
                                            setState(() => counter += 50),
                                        disabledColor: Colors.black,
                                        focusColor: Colors.black,
                                        color: Colors.black,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                        ],
                      ),
                    ),
              if (state is OrderRequestTrackChoosed)
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 5)
                      .copyWith(bottom: 0),
                  padding: EdgeInsets.all(10),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      border: Border.all(color: Color(0xffCBCBCB)),
                      borderRadius: BorderRadius.circular(10)),
                  child: DropdownButtonFormField(
                      decoration: InputDecoration.collapsed(hintText: ''),
                      icon: Icon(
                        Icons.arrow_forward_ios,
                        color: Color(0xffCBCBCB),
                      ),
                      onChanged: (value) => setState(() => name = value),
                      value: name,
                      items: [appCubit.getWord('cash'), 'Kaspi Gold']
                          .map(
                            (e) => DropdownMenuItem(
                              value: e,
                              child: Text(
                                e,
                                style: GoogleFonts.roboto(
                                  fontSize: 16,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          )
                          .toList()),
                ),
              if (state is OrderRequestTrackChoosed)
                Container(
                  padding: EdgeInsets.all(15).copyWith(top: 10, bottom: 10),
                  margin: EdgeInsets.symmetric(horizontal: 5),
                  child: TextField(
                    controller: comments,
                    decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Color(0xffCBCBCB),
                            ),
                            borderRadius: BorderRadius.circular(10)),
                        suffixIcon: Icon(
                          Icons.arrow_forward_ios,
                          color: Color(0xffCBCBCB),
                        ),
                        contentPadding: EdgeInsets.symmetric(
                          vertical: 5,
                          horizontal: 7,
                        ),
                        hintText: 'Комментарий для водителя',
                        hintStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Color(0xffCBCBCB),
                            ),
                            borderRadius: BorderRadius.circular(10))),
                  ),
                ),
              if (state is OrderRequestTrackChoosed ||
                  state is OrderRequestCreated)
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 21,
                  ),
                  child: MainButton(
                    width: MediaQuery.of(context).size.width,
                    action: () {
                      if (state is OrderRequestTrackChoosed) {
                        BlocProvider.of<OrderRequestCubit>(context)
                            .saveOrderRequest(form: {
                          'seats': seats.toString(),
                          'track_id': (state).track.id.toString(),
                          'comfort': comfort.toString(),
                          'payment_type_id': name == 'Kaspi Gold' ? '2' : '1',
                          'comments': comments.text.toString(),
                          'price': !comfort
                              ? (((state).track.startPrice * seats) +
                                      counter +
                                      (custom ? widget.customPrice : 0))
                                  .toString()
                              : (((state).track.startPriceComfort * seats) +
                                      counter +
                                      (custom ? widget.customPrice : 0))
                                  .toString()
                        }, context: context, track: (state).track);
                        setState(() {
                          ordered = true;
                        });
                      } else {
                        if (state is OrderRequestCreated &&
                            state.request != null) {
                          BlocProvider.of<OrderRequestCubit>(context)
                              .editRequests(state.request,
                                  {'order_request_status_id': '4'});

                          setState(() {
                            ordered = false;
                          });

                          widget.panelController.close();
                        }

                        BlocProvider.of<OrderRequestCubit>(context)
                            .emit(OrderRequestInitial());
                      }
                    },
                    color: Colors.white,
                    side: BorderSide(
                        color: state is OrderRequestTrackChoosed
                            ? Color.fromRGBO(45, 139, 226, 1)
                            : Colors.red),
                    radius: 8.0,
                    child: Text(
                      state is OrderRequestTrackChoosed
                          ? appCubit.getWord('make_order')
                          : appCubit.getWord('cancel'),
                      textAlign: TextAlign.center,
                      style: GoogleFonts.montserrat(
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        color: state is OrderRequestTrackChoosed
                            ? Color.fromRGBO(45, 139, 226, 1)
                            : Colors.red,
                      ),
                    ),
                  ),
                ),
            ],
          );
        },
      ),
    );
  }
}
