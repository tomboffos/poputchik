import 'package:flutter/material.dart';

class SocialButton extends StatelessWidget {
  final Widget icon;
  final onPressed;
  final Color color;
  const SocialButton({Key key, this.icon, this.onPressed, this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 40,
      child: RawMaterialButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        fillColor: color,
        onPressed: () {
          onPressed();
        },
        child: Center(child: icon),
      ),
    );
  }
}
