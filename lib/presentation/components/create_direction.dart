import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poputchik/cubit/app/cubit/app_cubit.dart';
import 'package:poputchik/cubit/order/cubit/order_cubit.dart';
import 'package:poputchik/cubit/track/cubit/track_cubit.dart';
import 'package:poputchik/data/constant.dart';
import 'package:poputchik/data/models/track.dart';
import 'package:poputchik/presentation/components/choose_track_list.dart';
import 'package:poputchik/presentation/components/loading_passengers.dart';
import 'package:poputchik/presentation/components/main_field.dart';
import 'package:poputchik/presentation/components/modal/cancel_modal.dart';
import 'package:poputchik/presentation/components/passenger_item.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

import 'main_button.dart';

class CreateDirection extends StatefulWidget {
  final Track track;
  final YandexMapController controller;
  final addressLine;
  final PanelController panelController;
  final constructMapTrack;
  const CreateDirection({
    Key key,
    this.track,
    this.controller,
    this.addressLine,
    this.panelController,
    this.constructMapTrack,
  }) : super(key: key);

  @override
  _CreateDirectionState createState() => _CreateDirectionState();
}

class _CreateDirectionState extends State<CreateDirection> {
  String name = 'Kaspi Gold';
  TextEditingController comments = new TextEditingController();
  bool automate = false;
  int counter = 1;
  chooseTrack() async {
    await BlocProvider.of<TrackCubit>(context).getTracks(query: '');
    BlocProvider.of<OrderCubit>(context).emit(OrderTrackChoosing());
    widget.panelController.open();
  }

  bool ordered = false;

  dialogCancel(
    context,
  ) async {
    setState(() {
      ordered = !ordered;
    });
    if (!ordered)
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return CancelDialog();
          });
  }

  @override
  Widget build(BuildContext context) {
    final appCubit = BlocProvider.of<AppCubit>(context);
    return SingleChildScrollView(
      child: BlocBuilder<OrderCubit, OrderState>(
        builder: (context, state) {
          return Column(
            children: [
              (state is OrderTrackChoosed) ||
                      (state is OrderInitial) ||
                      (state is OrderTrackChoosing) ||
                      state is OrderTrackCreated
                  ? GestureDetector(
                      onTap: () => chooseTrack(),
                      child: Column(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(16),
                                topRight: Radius.circular(16),
                              ),
                              color: primaryColor,
                            ),
                            padding: EdgeInsets.only(
                                top: 8, left: 15, right: 15, bottom: 15),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(
                                    top: 4.0,
                                  ),
                                  child: SvgPicture.asset(
                                      'assets/icons/pointwhite.svg'),
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      appCubit.getWord('geolocation'),
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle1
                                          .copyWith(
                                            color: Theme.of(context).hintColor,
                                          ),
                                    ),
                                    Text(
                                      widget.track != null
                                          ? widget.track.startName
                                          : widget.addressLine ?? '',
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline5
                                          .copyWith(
                                            color: whiteColor,
                                          ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                          if (state is! OrderTrackCreated)
                            Container(
                              margin: EdgeInsets.symmetric(
                                vertical: 24,
                                horizontal: 15,
                              ),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  border: Border.all(
                                    color: greyBorder,
                                  )),
                              padding: EdgeInsets.all(15),
                              child: Row(
                                children: [
                                  SvgPicture.asset(
                                      'assets/icons/discovery.svg'),
                                  SizedBox(width: 8),
                                  state is OrderTrackChoosing
                                      ? Container(
                                          height: 20,
                                          child: TextField(
                                            onChanged: (value) {
                                              BlocProvider.of<TrackCubit>(
                                                      context)
                                                  .getTracks(
                                                      query: value.toString());
                                            },
                                            decoration: InputDecoration(
                                                border: InputBorder.none),
                                          ),
                                          constraints: BoxConstraints(
                                              maxWidth: MediaQuery.of(context)
                                                      .size
                                                      .width -
                                                  160),
                                        )
                                      : Text(
                                          state.track != null
                                              ? state.track.endName
                                              : appCubit.getWord('where'),
                                          style: GoogleFonts.montserrat(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500,
                                              color: Colors.black))
                                ],
                              ),
                            ),
                        ],
                      ),
                    )
                  : SizedBox.shrink(),
              if (state is OrderTrackChoosing)
                ChooseTrackList(widget: widget, appCubit: appCubit),
              if (state is OrderTrackChoosed)
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  margin: EdgeInsets.symmetric(vertical: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        appCubit.getWord('type_count_places'),
                        style: GoogleFonts.montserrat(
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                            color: Color(0xff616161)),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(
                          vertical: 11,
                          horizontal: 16,
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            border: Border.all(
                              color: inputBorderColor,
                            )),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            InkWell(
                              child: Icon(
                                Icons.add,
                                color: primaryColor,
                                size: 28,
                              ),
                              onTap: () => setState(() => counter += 1),
                            ),
                            Text('$counter',
                                style: GoogleFonts.montserrat(
                                    fontSize: 14, fontWeight: FontWeight.w500)),
                            InkWell(
                              child: Icon(
                                Icons.remove,
                                color: primaryColor,
                                size: 28,
                              ),
                              onTap: () => setState(() {
                                if (counter > 1) counter -= 1;
                              }),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              SizedBox(height: state is OrderTrackCreated ? 0 : 20),
              !(state is OrderTrackChoosed)
                  ? SizedBox.shrink()
                  : Padding(
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      child: MainTextField(
                        controller: comments,
                        text: appCubit.getWord('type_comments'),
                      ),
                    ),
              SizedBox(
                height: state is OrderTrackCreated ? 0 : 6,
              ),
              if (state is OrderTrackCreated && state.order.requests.length < 1)
                LoadingPassengers(appCubit: appCubit),
              if (state is OrderTrackCreated &&
                  state.order.requests.length >= 1)
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 16,
                  ),
                  margin: EdgeInsets.only(top: 24),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Найдено ${state.order.requests.length} попутчиков по маршруту',
                        style: Theme.of(context).textTheme.headline5,
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.3,
                        child: MediaQuery.removePadding(
                          context: context,
                          removeTop: true,
                          child: ListView.builder(
                            itemBuilder: (context, index) => PassengerItem(
                              appCubit: appCubit,
                              orderRequest: state.order.requests[index],
                              order: state.order,
                            ),
                            itemCount: state.order.requests.length,
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              else if (state is OrderTrackChoosed)
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      GestureDetector(
                        onTap: () => setState(() => automate = !automate),
                        child: Container(
                          width: 25,
                          height: 25,
                          child: Center(
                            child: automate
                                ? Icon(
                                    Icons.check,
                                    color: Colors.white,
                                  )
                                : null,
                          ),
                          decoration: BoxDecoration(
                              color: automate ? primaryColor : Colors.white,
                              border: Border.all(color: Color(0xff313131)),
                              borderRadius: BorderRadius.circular(8)),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 14),
                        child: Text(appCubit.getWord('automate')),
                      )
                    ],
                  ),
                ),
              SizedBox(
                height: 15,
              ),
              if (state is OrderTrackChoosed || state is OrderTrackCreated)
                MainButton(
                  action: () {
                    if (state is OrderTrackCreated &&
                        state.order.requests
                                .where((element) =>
                                    element.orderRequestStatus.id != 7)
                                .length ==
                            0) {
                      BlocProvider.of<OrderCubit>(context)
                          .updateOrder(state.order, {'order_status_id': '3'});
                      BlocProvider.of<OrderCubit>(context).cancel();
                      widget.panelController.close();
                    } else if (state is OrderTrackChoosed)
                      BlocProvider.of<OrderCubit>(context).createOrder(form: {
                        'comments': comments.text,
                        'seats': counter.toString(),
                        'track_id': state.track.id,
                        'automated': automate.toString()
                      }, track: state.track);
                    else if (state is OrderTrackCreated) {
                      BlocProvider.of<OrderCubit>(context)
                          .updateOrder(state.order, {'order_status_id': '4'});
                      BlocProvider.of<OrderCubit>(context).cancel();
                      widget.panelController.close();
                    } else {
                      BlocProvider.of<OrderCubit>(context).emit(OrderInitial());
                      widget.panelController.close();
                    }

                    // setState(() => dialogCancel(context));
                  },
                  radius: 8.0,
                  width: MediaQuery.of(context).size.width - 50,
                  child: Text(
                      state is OrderTrackCreated &&
                              state.order.requests
                                      .where((element) =>
                                          element.orderRequestStatus.id != 8)
                                      .length ==
                                  0
                          ? appCubit.getWord('end_order')
                          : state is OrderTrackChoosed
                              ? appCubit.getWord('create')
                              : appCubit.getWord('cancel'),
                      textAlign: TextAlign.center,
                      style: GoogleFonts.montserrat(
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        color: state is OrderTrackChoosed
                            ? primaryColor
                            : Theme.of(context).errorColor,
                      )),
                  side: BorderSide(
                    color: state is OrderTrackChoosed
                        ? primaryColor
                        : Theme.of(context).errorColor,
                  ),
                ),
            ],
          );
        },
      ),
    );
  }
}
