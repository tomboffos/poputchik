import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class PriceButton extends StatelessWidget {
  final String price;
  final String name;
  final bool info;
  const PriceButton({
    Key key,
    this.price,
    this.name,
    this.info,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 10,
        horizontal: 15,
      ),
      decoration: BoxDecoration(
          color: info ? Color(0xffF0F9FE) : Color(0xffEDECEC),
          border: Border.all(
            color: info ? Color(0xff3C7DA1) : Color(0xffEDECEC),
          ),
          borderRadius: BorderRadius.circular(8)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(
            'assets/images/caricon.png',
            height: 30,
          ),
          Row(
            children: [
              Text(name + ':  ',
                  style: GoogleFonts.montserrat(
                      fontSize: 14, fontWeight: FontWeight.w500)),
              Text(
                '$price т',
                style: GoogleFonts.roboto(
                    fontSize: 14, fontWeight: FontWeight.w500),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
