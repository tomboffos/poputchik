import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poputchik/data/constant.dart';
import 'package:poputchik/presentation/components/main_gradient_button.dart';

class DialogCameraChoose extends StatelessWidget {
  final firstAction;
  final secondAction;
  const DialogCameraChoose({Key key, this.firstAction, this.secondAction})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.width / 2,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          MainGradientButton(
              action: firstAction,
              colors: [Colors.white, Colors.white],
              side: Border.all(width: 1),
              radius: 8.0,
              width: MediaQuery.of(context).size.width - 144,
              child: Text('Снять на камеру',
                  textAlign: TextAlign.center,
                  style: GoogleFonts.montserrat(
                      fontSize: 15,
                      fontWeight: FontWeight.w500,
                      color: Colors.black)),
              padding: EdgeInsets.symmetric(vertical: 15)),
          MainGradientButton(
              action: secondAction,
              colors: [Colors.white, Colors.white],
              side: Border.all(width: 1),
              radius: 8.0,
              width: MediaQuery.of(context).size.width - 144,
              child: Text('Выбрать из галереи',
                  textAlign: TextAlign.center,
                  style: GoogleFonts.montserrat(
                      fontSize: 15,
                      fontWeight: FontWeight.w500,
                      color: Colors.black)),
              padding: EdgeInsets.symmetric(vertical: 15)),
        ],
      ),
      width: MediaQuery.of(context).size.height / 2,
    );
  }
}
