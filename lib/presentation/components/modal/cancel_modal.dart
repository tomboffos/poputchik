import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poputchik/presentation/components/main_button.dart';

class CancelDialog extends StatelessWidget {
  final action;
  const CancelDialog({
    Key key,
    this.action,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: EdgeInsets.all(10),
      content: Container(
        height: MediaQuery.of(context).size.height / 8,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
                child: Text(
              'Вы точно хотите отменить \n заказ?',
              style: GoogleFonts.montserrat(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff272727)),
              textAlign: TextAlign.center,
            )),
            SizedBox(
              height: 11,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MainButton(
                  color: Colors.red,
                  width: MediaQuery.of(context).size.width / 4,
                  action: () => Navigator.pop(context),
                  child: Text(
                    'Нет',
                    style: GoogleFonts.montserrat(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        color: Colors.white),
                  ),
                  side: BorderSide(color: Colors.transparent),
                  radius: 0.0,
                ),
                MainButton(
                  color: Colors.transparent,
                  width: MediaQuery.of(context).size.width / 4,
                  action: action,
                  child: Text(
                    'Да',
                    style: GoogleFonts.montserrat(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        color: Colors.black),
                  ),
                  side: BorderSide(color: Colors.transparent),
                  radius: 0.0,
                ),
              ],
            )
          ],
        ),
      ),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5))),
    );
  }
}
