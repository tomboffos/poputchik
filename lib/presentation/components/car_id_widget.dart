import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poputchik/data/constant.dart';

class CarIdWidget extends StatelessWidget {
  final String carId;
  const CarIdWidget({Key key, this.carId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 8),
      decoration: BoxDecoration(
        color: greyCarNumber,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Text(carId,
          style: GoogleFonts.montserrat(
            fontSize: 14,
            fontWeight: FontWeight.normal,
          )),
    );
  }
}
