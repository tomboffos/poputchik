import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MainGradientButton extends StatelessWidget {
  final Widget child;
  final action;
  final List<Color> colors;
  final width;
  final side;
  final radius;
  final padding;
  const MainGradientButton(
      {Key key,
      this.child,
      this.action,
      this.colors,
      this.width,
      this.side,
      this.radius,
      this.padding})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: action,
      child: Container(
        padding: padding,
        constraints: BoxConstraints(
          minWidth: width,
        ),
        child: child,
        decoration: BoxDecoration(
            gradient: LinearGradient(colors: colors),
            border: side,
            borderRadius: BorderRadius.circular(radius)),
      ),
    );
  }
}
