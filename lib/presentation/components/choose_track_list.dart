import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:poputchik/cubit/app/cubit/app_cubit.dart';
import 'package:poputchik/cubit/track/cubit/track_cubit.dart';
import 'package:poputchik/data/constant.dart';
import 'package:poputchik/presentation/components/create_direction.dart';

class ChooseTrackList extends StatelessWidget {
  const ChooseTrackList({
    Key key,
    @required this.widget,
    @required this.appCubit,
  }) : super(key: key);

  final CreateDirection widget;
  final AppCubit appCubit;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TrackCubit, TrackState>(
      builder: (context, trackState) {
        if (trackState is TrackInitial) return SizedBox();

        final tracks = (trackState as TrackLoaded).tracks;
        return Container(
          padding: EdgeInsets.zero,
          decoration: BoxDecoration(
              border: Border.all(
                color: greyListBorder,
              ),
              borderRadius: BorderRadius.circular(8)),
          margin: EdgeInsets.symmetric(horizontal: 16),
          height: MediaQuery.of(context).size.height / 2.0,
          child: MediaQuery.removePadding(
            context: context,
            removeTop: true,
            child: ListView.builder(
              itemBuilder: (context, index) => GestureDetector(
                onTap: () {
                  widget.constructMapTrack(tracks[index]);
                },
                child: ListTile(
                  leading: Icon(Icons.location_on_outlined),
                  title: Text('${tracks[index].endName}'),
                ),
              ),
              itemCount: tracks.length,
            ),
          ),
        );
      },
    );
  }
}
