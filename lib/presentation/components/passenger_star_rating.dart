import 'package:flutter/material.dart';
import 'package:poputchik/data/constant.dart';

class PassengerStarRating extends StatelessWidget {
  final rate;
  const PassengerStarRating({Key key, this.rate}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 2, horizontal: 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Colors.white,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.star,
            color: starColor,
            size: 14,
          ),
          SizedBox(
            width: 7,
          ),
          Text(
            rate,
            style: Theme.of(context).textTheme.bodyText1.copyWith(
                  fontSize: 12,
                ),
          )
        ],
      ),
    );
  }
}
