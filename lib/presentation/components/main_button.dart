import 'package:flutter/material.dart';

class MainButton extends StatelessWidget {
  final Widget child;
  final action;
  final Color color;
  final width;
  final BorderSide side;
  final radius;
  const MainButton({
    Key key,
    this.child,
    this.action,
    this.color,
    this.width,
    this.side,
    this.radius,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
        minWidth: width ?? 0.0,
        child: FlatButton(
            height: MediaQuery.of(context).size.height / 18,
            onPressed: action,
            child: child,
            color: color,
            shape: RoundedRectangleBorder(
                side: side,
                borderRadius: BorderRadius.all(Radius.circular(radius)))));
  }
}
