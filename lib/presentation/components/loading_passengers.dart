import 'dart:async';

import 'package:flutter/material.dart';
import 'package:poputchik/cubit/app/cubit/app_cubit.dart';

class LoadingPassengers extends StatefulWidget {
  final request;

  const LoadingPassengers({
    Key key,
    @required this.appCubit,
    this.request,
  }) : super(key: key);

  final AppCubit appCubit;

  @override
  State<LoadingPassengers> createState() => _LoadingPassengersState();
}

class _LoadingPassengersState extends State<LoadingPassengers> {
  int time = 0;

  Timer mainTimer;

  @override
  void initState() {
    super.initState();

    Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        mainTimer = timer;

        time++;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 16,
      ),
      margin: EdgeInsets.only(
        bottom: widget.request != null ? 0 : 40,
        top: widget.request != null ? 0 : 24,
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                widget.request != null
                    ? 'Поиск машины'
                    : widget.appCubit.getWord('searching_passengers'),
                style: Theme.of(context).textTheme.headline5.copyWith(
                      color: Theme.of(context).selectedRowColor,
                    ),
                textAlign: TextAlign.center,
              ),
              Text(
                  '${(time / 60).toInt().toString().padLeft(2, '0')}:${(time % 60).toString().padLeft(2, '0')}'),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(top: 4),
            child: Text(
              widget.request != null
                  ? 'Сейчас мы подбираем подходящий вам вариант. Подождите еще немного'
                  : widget.appCubit.getWord(
                      'hint_loading',
                    ),
              style: Theme.of(context).textTheme.subtitle1.copyWith(
                    color: Theme.of(context).selectedRowColor,
                  ),
            ),
          ),
          SizedBox(
            height: 12,
          ),
          LinearProgressIndicator(
            color: Theme.of(context).dividerColor,
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    mainTimer.cancel();
  }
}
