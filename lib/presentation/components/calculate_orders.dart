import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poputchik/cubit/app/cubit/app_cubit.dart';
import 'package:poputchik/cubit/order/cubit/order_cubit.dart';
import 'package:poputchik/data/models/order.dart';

class CalculateOrder extends StatefulWidget {
  final List<Order> orders;
  const CalculateOrder({
    Key key,
    this.orders,
  }) : super(key: key);

  @override
  State<CalculateOrder> createState() => _CalculateOrderState();
}

class _CalculateOrderState extends State<CalculateOrder> {
  DateTime startDate;
  DateTime endDate;

  @override
  Widget build(BuildContext context) {
    final appCubit = BlocProvider.of<AppCubit>(context);
    return Container(
      width: MediaQuery.of(context).size.width - 60,
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
          border: Border.all(color: Color(0xff313131)),
          borderRadius: BorderRadius.circular(8)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Center(
          //   child: Text(
          //     ' мой доход',
          //     style: GoogleFonts.montserrat(
          //         fontSize: 15, fontWeight: FontWeight.w400),
          //   ),
          // ),
          SizedBox(
            height: 11,
          ),
          Row(
            children: [
              Text('От'),
              SizedBox(
                width: 5,
              ),
              GestureDetector(
                onTap: () async {
                  DateTime date = await showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(2020, 10, 20),
                      lastDate: DateTime.now());
                  setState(() {
                    startDate = date;
                  });
                  if (startDate != null && endDate != null)
                    BlocProvider.of<OrderCubit>(context)
                        .getOrders(dates: [startDate, endDate]);
                },
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 3, horizontal: 10),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black),
                      borderRadius: BorderRadius.circular(100)),
                  child: Row(
                    children: [
                      Icon(
                        Icons.calendar_today,
                        color: Colors.red,
                        size: 20,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                          startDate != null
                              ? '${startDate.day}.${startDate.month}.${startDate.year}'
                              : '10.10.2021',
                          style: GoogleFonts.montserrat(fontSize: 12)),
                    ],
                  ),
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Text('До'),
              SizedBox(
                width: 5,
              ),
              GestureDetector(
                onTap: () async {
                  DateTime date = await showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(2020, 10, 20),
                      lastDate: DateTime.now().add(Duration(days: 1000)));
                  setState(() {
                    endDate = date;
                  });
                  if (startDate != null && endDate != null)
                    BlocProvider.of<OrderCubit>(context)
                        .getOrders(dates: [startDate, endDate]);
                },
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 3, horizontal: 10),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black),
                      borderRadius: BorderRadius.circular(100)),
                  child: Row(
                    children: [
                      Icon(
                        Icons.calendar_today,
                        color: Colors.red,
                        size: 20,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                          startDate != null
                              ? '${endDate.day}.${endDate.month}.${endDate.year}'
                              : '10.10.2021',
                          style: GoogleFonts.montserrat(fontSize: 12)),
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 13,
          ),
          Center(
            child: Text(
                '${widget.orders.fold(0, (previousValue, element) => element.price != null ? previousValue + element.price : previousValue + 0)} тг',
                style: GoogleFonts.montserrat(
                    fontSize: 15, fontWeight: FontWeight.w500)),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
              child: Text(
                  '${appCubit.getWord('count_passengers')}: ${widget.orders.length}',
                  style: GoogleFonts.montserrat(
                      fontSize: 14, fontWeight: FontWeight.w400))),
          // SizedBox(
          //   height: 5,
          // ),
          // Container(
          //     child: Text('Чаевые: 500 тг',
          //         style: GoogleFonts.montserrat(
          //             fontSize: 14, fontWeight: FontWeight.w400))),
          // SizedBox(
          //   height: 5,
          // ),
          // Container(
          //     child: Text('Оплата наличными: 2 000 тг',
          //         style: GoogleFonts.montserrat(
          //             fontSize: 14, fontWeight: FontWeight.w400))),
          // SizedBox(
          //   height: 5,
          // ),
          // Container(
          //     child: Text('Оплата переводом: 13 654,25 тг',
          //         style: GoogleFonts.montserrat(
          //             fontSize: 14, fontWeight: FontWeight.w400))),
          // SizedBox(
          //   height: 5,
          // ),
          Container(
              child: Text(
                  '${appCubit.getWord('comission')}: ${widget.orders.fold(0, (previousValue, element) => element.price != null ? previousValue + int.parse(element.commission) : previousValue + 0)}  тг',
                  style: GoogleFonts.montserrat(
                      fontSize: 14, fontWeight: FontWeight.w400))),
          SizedBox(
            height: 5,
          ),
          Container(
              child: Text(
                  '${appCubit.getWord('income')} :  ${widget.orders.fold(0, (previousValue, element) => element.price != null ? previousValue + element.price - int.parse(element.commission) : previousValue + 0)}',
                  style: GoogleFonts.montserrat(
                      fontSize: 14, fontWeight: FontWeight.w400))),
        ],
      ),
    );
  }
}
