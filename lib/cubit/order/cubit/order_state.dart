part of 'order_cubit.dart';

@immutable
abstract class OrderState {
  Track track;
}

class OrderInitial extends OrderState {}

class OrderTrackChoosing extends OrderState {}

class OrderTrackChoosed extends OrderState {
  final Track track;

  OrderTrackChoosed({this.track});
}

class OrderTrackCreated extends OrderState {
  final Order order;
  final Track track;

  OrderTrackCreated(this.order, this.track);
}

class OrdersLoaded extends OrderState {
  final List<Order> orders;

  OrdersLoaded({this.orders});
}
