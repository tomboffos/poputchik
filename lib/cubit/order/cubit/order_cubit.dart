import 'dart:async';
import 'dart:developer' as dev;

import 'package:adhara_socket_io/adhara_socket_io.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:location/location.dart';
import 'package:poputchik/cubit/app/cubit/app_cubit.dart';
import 'package:poputchik/data/models/order.dart';
import 'package:poputchik/data/models/order_request.dart';
import 'package:poputchik/data/models/track.dart';
import 'package:poputchik/data/repository.dart';
import 'package:poputchik/presentation/screens/user/home/map.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';
import 'dart:math';

part 'order_state.dart';

class OrderCubit extends Cubit<OrderState> {
  final Repository repository;
  OrderCubit(this.repository) : super(OrderInitial());
  Location location = new Location();
  SocketIO socket;
  LocationData locationData;
  Future<dynamic> chooseTrack({
    Track track,
    YandexMapController controller,
    context,
  }) async {
    emit(OrderTrackChoosed(track: track));

    final value = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => OrdrMapPointChoooseScreen(
                  track: track,
                )));
    if (value == null) {
      return {'track': null};
    }
    Map<String, dynamic> directData =
        await repository.getDirection(locationData, track.points.first);

    List trackMain = directData['lines'];

    List tracks = track.points
        .takeWhile(
            (element) => element[0] != value[0] && element[1] != value[1])
        .toList();

    await controller.moveCamera(
      CameraUpdate.newTiltAzimuthBounds(
        BoundingBox(
          northEast: Point(
              latitude: directData['bounds'][0][0] - 0.07,
              longitude: directData['bounds'][0][1] - 0.01),
          southWest: Point(
              latitude: directData['bounds'][1][0],
              longitude: directData['bounds'][1][1]),
        ),
      ),
    );
    await controller.moveCamera(CameraUpdate.zoomTo(13.0),
        animation: MapAnimation());

    tracks.forEach((element) {
      trackMain.add(element.cast<double>());
    });

    trackMain.add(value.cast<double>());

    return {
      'track': track,
      'point': value,
      'direction': trackMain,
    };
  }

  List<StreamSubscription> streams = [];

  void createOrder({Track track, form}) {
    repository.createProcessorder(form: form).then((value) async {
      socket = await SocketIOManager().createInstance(
          SocketOptions('https://socket.it-lead.net', enableLogging: false));
      await socket.connectSync();
      streams.add(
          socket.on('poputchik.order.request.sent.${value.id}').listen((event) {
        repository
            .fetchCurrentOrder(value)
            .then((newValue) => emit(OrderTrackCreated(newValue, track)));
      }));

      streams.add(socket
          .on('poputchik.order.request.accepted.${value.id}')
          .listen((event) {
        repository
            .fetchCurrentOrder(value)
            .then((newValue) => emit(OrderTrackCreated(newValue, track)));
      }));

      streams.add(socket
          .on('poputchik.order.request.deleted.${value.id}')
          .listen((event) {
        repository
            .fetchCurrentOrder(value)
            .then((newValue) => emit(OrderTrackCreated(newValue, track)));
      }));

      emit(OrderTrackCreated(value, track));
    });
  }

  void getOrders({dates}) {
    repository
        .processOrders(dates: dates)
        .then((value) => emit(OrdersLoaded(orders: value)));
  }

  void showCurrentOrder(Order order) async {
    repository.fetchCurrentOrder(order);
  }

  void updateRequest(OrderRequest orderRequest, Order order, form,
      {BuildContext context, AppCubit appCubit}) {
    repository.updateRequest(orderRequest, form).then((value) async {
      repository.fetchCurrentOrder(order).then((value) {
        emit(OrderTrackCreated(value, order.track));

        if (form['order_request_status_id'] == '8') {
          showDialog(
              context: context,
              builder: (modalContext) => AlertDialog(
                    content: Container(
                      height: 200,
                      width: 200,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Center(
                            child: Text(appCubit.getWord('rate_order')),
                          ),
                          SmoothStarRating(
                            color: Colors.yellow,
                            size: 30,
                            borderColor: Colors.yellow,
                            onRated: (rating) async {
                              await repository.rateOrderUser(
                                  orderRequest.user, rating.toInt());
                              Navigator.pop(modalContext);

                              showDialog(
                                context: context,
                                builder: (newContext) => AlertDialog(
                                  content:
                                      Text('Вы оценили на ${rating.toInt()}'),
                                ),
                              );

                              Future.delayed(Duration(seconds: 1),
                                  () => Navigator.pop(context));
                            },
                          )
                        ],
                      ),
                    ),
                  ));
        }
      });

      await socket.connectSync();
      await socket.emit('poputchik.order.driver.location.change.${value.id}', [
        {
          'position': [
            locationData.latitude,
            locationData.longitude,
          ],
        }
      ]);
    });
  }

  void updateOrder(Order order, form) {
    repository.updateOrder(order, form).then((value) => emit(OrderInitial()));
  }

  calculateHaverSine(needPoint, secondPoint) {
    final r = pi / 180;
    final res = 6371;
    dynamic deltaLat = (secondPoint[0] - needPoint[0]) * r;
    dynamic deltaLong = (secondPoint[1] - needPoint[1]) * r;
    final a = pow(sin(deltaLat / 2), 2) +
        cos(cos(secondPoint[1] * r) * r * needPoint[0]) *
            pow(sin(deltaLong / 2), 2);

    final c = 2 * atan2(pow(a, 0.5), pow((1 - a), 0.5));

    final d = res * c;
    dev.log(d.toString());
    return d;
  }

  cancel() async {
    streams.forEach((element) async {
      await element.cancel();
    });
  }
}
