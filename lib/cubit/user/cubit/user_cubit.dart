import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:poputchik/data/models/user.dart';
import 'package:poputchik/data/repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'user_state.dart';

class UserCubit extends Cubit<UserState> {
  final Repository repository;

  UserCubit({this.repository}) : super(UserInitial());

  registerUser({Map<String, dynamic> form, context}) {
    emit(UserLoading());
    repository.registerUser(form, context).then((value) => emit(UserInitial()));
  }

  checkCode(String code, User user, BuildContext context, bool driver) async {
    await repository.checkCode(code, user, context, driver);
  }

  loginUser({Map<String, dynamic> form, context, bool driver}) {
    emit(UserLoading());
    repository
        .login(form: form, context: context, driver: driver)
        .then((value) => emit(UserInitial()));
  }

  checkUser({context}) {
    repository.checkToken(context: context);
  }

  becomeDriver(User user, BuildContext context, {bool driverMode}) async {
    if (driverMode)
      (await SharedPreferences.getInstance()).setBool('driver', true);
    else
      (await SharedPreferences.getInstance()).setBool('driver', false);

    if (driverMode)
      user.driver != null
          ? Navigator.pushNamedAndRemoveUntil(
              context, '/driver/map/index', (route) => false)
          : Navigator.pushNamed(context, '/become/driver');
    else
      Navigator.pushNamedAndRemoveUntil(
          context, '/user/home', (route) => false);
  }

  getUser() {
    repository.processUser().then((value) => emit(UserLoaded(value)));
  }

  updateUser({form, context}) {
    repository.updateProcessUser(form: form).then((value) => getUser());

    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        content: Text('Успешно обновлено'),
        actions: [
          FlatButton(onPressed: () => Navigator.pop(context), child: Text('Oк'))
        ],
      ),
    );
  }

  logout({context}) {
    repository.logoutUser(context: context);
  }
}
