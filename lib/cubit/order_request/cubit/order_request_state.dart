part of 'order_request_cubit.dart';

@immutable
abstract class OrderRequestState {}

class OrderRequestInitial extends OrderRequestState {}

class OrderRequestChoosing extends OrderRequestState {}

class OrderRequestTrackChoosed extends OrderRequestState {
  final Track track;

  OrderRequestTrackChoosed(this.track);
}

class OrderRequestTrackEnded extends OrderRequestState {
  final dynamic driverId;

  OrderRequestTrackEnded(this.driverId);
}

class OrderRequestsLoaded extends OrderRequestState {
  final List<OrderRequest> requests;

  OrderRequestsLoaded({this.requests});
}

class OrderRequestCreated extends OrderRequestState {
  final OrderRequest request;
  final Track track;
  final int time;

  OrderRequestCreated({this.request, this.track, this.time});
}
