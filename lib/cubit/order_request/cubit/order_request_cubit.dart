import 'dart:developer' as dev;
import 'dart:math';

import 'package:adhara_socket_io/adhara_socket_io.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:poputchik/data/models/order_request.dart';
import 'package:poputchik/data/models/track.dart';
import 'package:poputchik/data/repository.dart';
import 'package:poputchik/presentation/screens/user/home/map.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

part 'order_request_state.dart';

class OrderRequestCubit extends Cubit<OrderRequestState> {
  final Repository repository;
  OrderRequestCubit({this.repository}) : super(OrderRequestInitial());
  Location location = new Location();
  LocationData locationData;
  SocketIO socket;
  Future chooseTrack(
      Track track, YandexMapController controller, BuildContext context) async {
    final value = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => OrdrMapPointChoooseScreen(
                  track: track,
                )));

    if (value == null) {
      return {'track': null};
    }
    // controller.moveCamera(
    //     CameraUpdate.newCameraPosition(CameraPosition(
    //         target: Point(latitude: value[0], longitude: value[1]))),
    //     animation: MapAnimation());
    emit(OrderRequestTrackChoosed(track));
    Map<String, dynamic> directData =
        await repository.getDirection(locationData, track.points.first);

    List trackMain = directData['lines'];
    List tracks = track.points
        .takeWhile(
            (element) => element[0] != value[0] && element[1] != value[1])
        .toList();
    await controller.moveCamera(CameraUpdate.newBounds(BoundingBox(
        northEast: Point(
            latitude: directData['bounds'][0][0] - 0.07,
            longitude: directData['bounds'][0][1] - 0.01),
        southWest: Point(
            latitude: directData['bounds'][1][0],
            longitude: directData['bounds'][1][1]))));

    await controller.moveCamera(CameraUpdate.zoomTo(13.0),
        animation: MapAnimation());
    tracks.forEach((element) {
      trackMain.add(element.cast<double>());
    });
    trackMain.add(value.cast<double>());

    List distances = [];
    tracks.forEach((element) {
      distances.add({
        'latitude': element[0],
        'longitude': element[1],
        'distance': calculateHaverSine(
            [locationData.latitude, locationData.longitude], element)
      });
    });
    final minPoint = distances.reduce((value, element) =>
        value['distance'] < element['distance'] ? value : element);
    int price;
    dev.log('min dist ' + minPoint['distance'].toString());

    if (track.prices != null) {
      track.prices.forEach((element) {
        if (element['kilometers'] != null && element['kilometers'] != '') {
          if (minPoint['distance'] > int.parse(element['kilometers'])) {
            price = int.parse(element['price']);
          }
        }
      });
    }

    dev.log('price main ' + price.toString());
    return {
      'track': track,
      'point': value,
      'direction': trackMain,
      'minPoint': [minPoint['latitude'], minPoint['longitude']],
      'distance': minPoint['distance'],
      'price': price
    };
  }

  void saveOrderRequest({form, Track track, context}) async {
    repository
        .processSaveOrderRequest(form: form, track: track, context: context)
        .then((value) async {
      socket = await SocketIOManager().createInstance(
          SocketOptions('https://socket.it-lead.net', enableLogging: false));
      await socket.connectSync();
      socket.on('poputchik.order.request.accepted.${value.id}').listen((event) {
        repository.fetchCurrentRequest(value).then((newValue) =>
            emit(OrderRequestCreated(request: newValue, track: track)));
      });

      socket.on('poputchik.order.request.deleted.${value.id}').listen((event) {
        emit(OrderRequestInitial());
      });

      socket.on('poputchik.order.request.ended.${value.id}').listen((event) {
        emit(OrderRequestTrackEnded(event[0]['driver']['id']));
      });

      socket.on('poputchik.order.request.arrived.${value.id}').listen((event) {
        repository.fetchCurrentRequest(value).then((newValue) => emit(
            OrderRequestCreated(request: newValue, track: track, time: 180)));
      });

      socket.on('poputchik.order.request.get.${value.id}').listen((event) {
        repository
            .fetchCurrentRequest(value)
            .then((newValue) => emit(OrderRequestCreated(
                  request: newValue,
                  track: track,
                )));
      });

      emit(OrderRequestCreated(request: value, track: track));
    });
  }

  void getRequests() {
    repository
        .processRequests()
        .then((value) => emit(OrderRequestsLoaded(requests: value)));
  }

  Future editRequests(OrderRequest orderRequest, form) {
    repository.updateRequest(orderRequest, form).then((value) {
      repository.fetchCurrentRequest(orderRequest);
    });
  }

  cancelOrder({orderId, context}) {
    repository
        .cancelRequestOrder(orderId: orderId, context: context)
        .then((value) => emit(OrderRequestInitial()));
  }

  rateOrder({int rating, modalContext, driverId}) {
    repository.rateOrder(rating, modalContext, driverId);
  }

  calculateHaverSine(needPoint, secondPoint) {
    final r = pi / 180;
    final res = 6371;
    dynamic deltaLat = (secondPoint[0] - needPoint[0]) * r;
    dynamic deltaLong = (secondPoint[1] - needPoint[1]) * r;
    final a = pow(sin(deltaLat / 2), 2) +
        cos(cos(secondPoint[1] * r) * r * needPoint[0]) *
            pow(sin(deltaLong / 2), 2);

    final c = 2 * atan2(pow(a, 0.5), pow((1 - a), 0.5));

    final d = res * c;
    dev.log(d.toString());
    return d;
  }
}
