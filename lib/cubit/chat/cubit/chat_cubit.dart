
import 'package:bloc/bloc.dart';
import 'package:images_picker/images_picker.dart';
import 'package:meta/meta.dart';
import 'package:poputchik/data/models/message.dart';
import 'package:poputchik/data/repository.dart';
import 'package:adhara_socket_io/manager.dart';
import 'package:adhara_socket_io/options.dart';
import 'package:adhara_socket_io/socket.dart';

part 'chat_state.dart';

class ChatCubit extends Cubit<ChatState> {
  final Repository repository;
  SocketIO socket;
  List<Message> messages = [];
  ChatCubit(this.repository) : super(ChatInitial());

  void linkSocket() async {
    final user = await repository.processUser();
    socket = await SocketIOManager().createInstance(
        SocketOptions('https://socket.it-lead.net', enableLogging: false));
    socket.onConnect.listen((data) {
      print('connected: $data');
    });
    await socket.connectSync();

    socket.on('poputchik.message.sent.${user.id}').listen((event) {
      this.getMessage();
    });
  }

  void getMessage() async {
    messages = await repository.processMessages();
    emit(ChatLoaded(messages));
  }

  void sendMessages(List<Media> images, message) {
    repository.processSendMessage(images, message);
  }
}
