import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'app_state.dart';

class AppCubit extends Cubit<AppState> {
  AppCubit() : super(AppInitial('ru'));

  changeLang(String lang) {
    emit(AppInitial(lang));
  }

  String getLang() {
    return state.lang;
  }

  Map<String, String> kazWords = {
    'update': 'Профильды өңдеу',
    'type_phone': 'Нөмірді теріңіз',
    'type_car_model': 'Көліктің моделін теріңіз',
    'disable_notification': 'Хабарландыруды сөндіру',
    'support': 'Қолдау қызметі',
    'balance': 'Баланс',
    'history': 'Тапсырыстар тарихы',
    'security': 'Қауіпсіздік',
    'settings': 'Орнатулар',
    'support_online': 'Көмек',
    'accident_services': 'Төтенше қызметі',
    'become_user': 'Жолаушы болу',
    'exit': 'Шығу',
    'active': 'Желіде',
    'not_active': 'Желіде емес',
    'count_passengers': 'Жолаушылар саны',
    'comission': 'Комиссия',
    'income': 'Таза табыс',
    'overall': 'Барлық уақытта тапсырыс берілді',
    'where': 'Выберите трассу',
    'start_price': 'Бастапқы бағасы',
    'type_comments': 'Алдағы сапарға түсініктеме',
    'type_count_places': 'Орын санын теріңіз',
    'searching_passengers': 'Жолаушыларды жинап жатырмыз',
    'price': 'Бағасы',
    'seats': 'Орындары',
    'status': 'Күйі',
    'cancel': 'Болдырмау',
    'accept': 'Қабылдау',
    'take_him': 'Жолаушыны алып кету',
    'brought_him': 'Жолаушыны түсіру',
    'arrived': 'Келдім',
    'automate': 'Тапсырыстарды автоматты түрде қабылдау',
    'end_order': 'Тапсырысты аяқтау',
    'create': 'Маршрутты құру',
    'become_driver': 'Жүргізуші болу',
    'rate_order': 'Сапарды бағалаңызшы',
    'choose_destination': 'Нүктеңізді таңдаңыз',
    'waiting': 'Жүргізуші келді. Күту уақыты:',
    'wait_end': 'Күту уақыты аяқталды',
    'searching_driver': 'Жүргізуді таңдап жатырмыз',
    'increase_price': 'Тезірек шығып, бағасын көтер!',
    'econom': 'Эконом',
    'comfort': 'Комфорт',
    'comments': 'Түсініктемелер',
    'cash': 'Қолма-қол төлеу',
    'make_order': 'Тапсырыс беру',
    'text': 'Чат',
    'call': 'Позвонить',
    'owner_data': 'Личные данные',
    'change_password': 'Смена пароля',
    'geolocation': 'Ваш адрес:',
    'hint_loading':
        'Сейчас мы подбираем подходящий вам вариант. Подождите еще немного',
  };

  Map<String, String> ruWords = {
    'update': 'Обновить профиль',
    'type_phone': 'Введите номер',
    'type_car_model': 'Введите модель машины',
    'disable_notification': 'Отключить уведомление',
    'support': 'Служба поддержки',
    'balance': 'Баланс',
    'history': 'История заказов',
    'security': 'Безопасность',
    'settings': 'Настройки',
    'support_online': 'Обратная связь',
    'accident_services': 'Экстренные службы',
    'become_user': 'Стать пассажиром',
    'exit': 'Выход',
    'active': 'Активен',
    'not_active': 'Не активен',
    'count_passengers': 'Количество пассажиров',
    'comission': 'Комиссия',
    'income': 'Чистая прибыль',
    'overall': 'За все время заказали на',
    'where': 'Куда поедете?',
    'start_price': 'Стартовая цена',
    'type_comments': 'Комментарий к предстоящей поездке',
    'type_count_places': 'Количество мест',
    'searching_passengers': 'Подбираем для вас пассажиров',
    'price': 'Цена',
    'seats': 'Места',
    'status': 'Статус',
    'cancel': 'Отменить',
    'accept': 'Принять',
    'take_him': 'Забрать пассажира',
    'brought_him': 'Высадить пассажира',
    'arrived': 'Приехал',
    'automate': 'Автоматический принимать заказы',
    'end_order': 'Закончить заказ',
    'create': 'Создать маршрут',
    'become_driver': 'Стать водителем',
    'rate_order': 'Пожалуйста, оцените поездку',
    'choose_destination': 'Выберите вашу точку',
    'waiting': 'Водитель вас ожидает...',
    'wait_end': 'Время ожидания вышло',
    'searching_driver': 'Ищем водителя',
    'increase_price': 'Поднимите цену',
    'econom': 'Эконом',
    'comfort': 'Комфорт',
    'comments': 'Комментарии',
    'cash': 'Наличными',
    'make_order': 'Заказать',
    'text': 'Написать',
    'price_main': 'Забрать с текущего места',
    'call': 'Позвонить',
    'owner_data': 'Личные данные',
    'change_password': 'Смена пароля',
    'geolocation': 'Ваш адрес:',
    'hint_loading':
        'Сейчас мы подбираем подходящий вам вариант. Подождите еще немного',
  };

  getWord(String key) {
    if (state.lang == 'ru') return ruWords[key] ?? '';
    return kazWords[key] ?? '';
  }
}
