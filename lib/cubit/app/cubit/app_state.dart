part of 'app_cubit.dart';

@immutable
abstract class AppState {
  final String lang;

  AppState(this.lang);
}

class AppInitial extends AppState {
  AppInitial(String lang) : super(lang);
}
