part of 'order_user_cubit.dart';

@immutable
abstract class OrderUserState {}

class OrderUserInitial extends OrderUserState {}

class OrderUserFetched extends OrderUserState {
  final List<OrderRequest> requests;

  OrderUserFetched(this.requests);
}
