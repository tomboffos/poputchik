import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:poputchik/data/models/order_request.dart';
import 'package:poputchik/data/repository.dart';

part 'order_user_state.dart';

class OrderUserCubit extends Cubit<OrderUserState> {
  final Repository repository;
  OrderUserCubit(this.repository) : super(OrderUserInitial());

  void getOrderUsers() {
    repository.processRequests().then((value) => emit(OrderUserFetched(value)));
  }
}
