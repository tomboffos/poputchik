import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:poputchik/data/repository.dart';

part 'driver_state.dart';

class DriverCubit extends Cubit<DriverState> {
  final Repository repository;
  DriverCubit(this.repository) : super(DriverInitial());

  void registerDriver({form, context}) {
    emit(DriverLoading());
    repository
        .registerDriver(form: form, context: context)
        .then((value) => emit(DriverInitial()));
  }

  void fillBalance({price, context}) {
    repository.fillBalance(price: price, context: context);
  }

  Future updateDriver({context, form}) async {
    await repository.updateDriverProcess(context: context, form: form);
  }

  becomDriver({context, form}) async {
    await repository.becomeDriver(context: context, form: form);
  }
}
