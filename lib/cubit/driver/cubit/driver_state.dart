part of 'driver_cubit.dart';

@immutable
abstract class DriverState {}

class DriverInitial extends DriverState {}

class DriverLoading extends DriverState {}
