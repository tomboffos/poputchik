import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:poputchik/data/models/help_phone.dart';
import 'package:poputchik/data/repository.dart';

part 'help_phone_state.dart';

class HelpPhoneCubit extends Cubit<HelpPhoneState> {
  final Repository repository;
  HelpPhoneCubit(this.repository) : super(HelpPhoneInitial());

  getHelpPhones() {
    repository
        .processHelpPhones()
        .then((values) => emit(HelpPhoneLoaded(values)));
  }
}
