part of 'help_phone_cubit.dart';

@immutable
abstract class HelpPhoneState {}

class HelpPhoneInitial extends HelpPhoneState {}

class HelpPhoneLoaded extends HelpPhoneState {
  final List<HelpPhone> helpPhones;

  HelpPhoneLoaded(this.helpPhones);
}
