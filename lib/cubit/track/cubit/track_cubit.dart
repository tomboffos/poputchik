import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:poputchik/data/models/track.dart';
import 'package:poputchik/data/repository.dart';

part 'track_state.dart';

class TrackCubit extends Cubit<TrackState> {
  final Repository repository;
  TrackCubit(this.repository) : super(TrackInitial());

  getTracks({query}) {
    print('query');
    repository
        .processTracks(query: query)
        .then((value) => emit(TrackLoaded(value)));
  }
}
