part of 'track_cubit.dart';

@immutable
abstract class TrackState {}

class TrackInitial extends TrackState {}

class TrackLoaded extends TrackState {
  final List<Track> tracks;

  TrackLoaded(this.tracks);
}
