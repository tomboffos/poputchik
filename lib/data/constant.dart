import 'package:flutter/material.dart';

const primaryColor = Color(0xff2D8BE2);
const secondColor = Colors.yellow;
const dividerColor = Color(0xffEFEFEF);
const greenColor = Color(0xff0DB245);
const starColor = Color(0xffFFD704);
const greyText = Color(0xff8D8D8D);
const borderColor = Color(0xff2DA1E2);
const dividerSecondaryColor = Color(0xffC8DDFF);
const greyCarNumber = Color(0xffDCDCDC);
const gradientEndColor = Color(0xff155173);
const whiteColor = Colors.white;
const greyBorder = Color(0xff757575);
const greyListBorder = Color(0xffABABAB);
const inputBorderColor = Color(0xff757575);
const userBackground = Color(0xffEDF4FF);
const Map<String, String> colors = {
  'black': 'Черный',
};
