import 'package:poputchik/data/models/order.dart';
import 'package:poputchik/data/models/order_request_status.dart';
import 'package:poputchik/data/models/track.dart';
import 'package:poputchik/data/models/user.dart';

class OrderRequest {
  Track track;
  OrderRequestStatus orderRequestStatus;
  int price;
  String id;
  int seats;
  dynamic comfort;
  String comments;
  String createdAt;
  Order order;
  User user;

  OrderRequest.fromJson(Map json)
      : track = json['track'] != null ? Track.fromJson(json['track']) : null,
        orderRequestStatus =
            OrderRequestStatus.fromJson(json['order_request_status']),
        price = json['price'].runtimeType == int
            ? json['price']
            : int.tryParse(json['price']),
        seats = json['seats'].runtimeType == int
            ? json['seats']
            : int.tryParse(json['seats']),
        comfort = json['comfort'],
        comments = json['comments'],
        createdAt = json['created_at'],
        id = json['id'].toString(),
        order = json['order'] != null ? Order.fromJson(json['order']) : null,
        user = json['user'] != null ? User.fromJson(json['user']) : null;
}
