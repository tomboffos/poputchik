import 'package:poputchik/data/models/driver.dart';
import 'package:poputchik/data/models/driver_user.dart';
import 'package:poputchik/data/models/order_request.dart';
import 'package:poputchik/data/models/track.dart';

class Order {
  Track track;
  String comments;
  dynamic seats;
  String automated;
  String status;
  String commission;
  int price;
  DriverUser driver;
  List<OrderRequest> requests;
  dynamic id;

  Order.fromJson(Map json)
      : track = json['track'] != null ? Track.fromJson(json['track']) : null,
        comments = json['comments'],
        seats = json['seats'],
        automated = json['automated'].toString(),
        commission = json['commission'].toString(),
        price = json['price'],
        driver = DriverUser.fromJson(json['driver']),
        requests = json['requests']
            .map((request) => OrderRequest.fromJson(request))
            .toList()
            .cast<OrderRequest>(),
        id = json['id'];
}
