import 'dart:convert';

class Track {
  String startName;
  String endName;
  List points;
  int startPrice;
  int startPriceComfort;
  int customPrice;
  String id;
  List prices;

  Track.fromJson(Map json)
      : startName = json['start_name'] ?? '',
        endName = json['end_name'],
        points = jsonDecode(json['points']),
        startPrice = json['start_price'],
        startPriceComfort = json['start_price_comfort'],
        customPrice = json['custom_price'],
        id = json['id'].toString(),
        prices = json['prices'];
}
