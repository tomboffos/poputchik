class DriverUser {
  String carModel;
  String carColor;
  String carYear;
  String name;
  String carId;
  String phone;
  dynamic rating;
  dynamic id;

  DriverUser.fromJson(Map json)
      : carModel = json['car_model'],
        carColor = json['car_color'],
        carYear = json['car_year'],
        name = json['name'],
        carId = json['car_id'],
        phone = json['phone'],
        rating = json['rating'],
        id = json['id'];
}
