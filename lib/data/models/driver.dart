class Driver {
  int balance;
  bool isActive;
  String carColor;
  String carModel;
  String carId;

  Driver.fromJson(Map json)
      : balance = json['balance'],
        isActive = json['is_active'],
        carColor = json['car_color'],
        carModel = json['car_model'],
        carId = json['car_id'];
}
