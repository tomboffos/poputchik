class OrderRequestStatus {
  String name;
  dynamic id;

  OrderRequestStatus.fromJson(Map json)
      : name = json['name'],
        id = json['id'];
}
