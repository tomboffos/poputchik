
import 'package:poputchik/data/models/user.dart';

class Message {
  User user;
  dynamic support;
  Map<String, dynamic> messageContent;
  bool isMine;

  Message.fromJson(Map json)
      : user = User.fromJson(json['user']),
        support =
            json['support'] != null ? User.fromJson(json['support']) : null,
        messageContent = new Map<String, dynamic>.from(json['message_content']),
        isMine = json['is_mine'];
}
