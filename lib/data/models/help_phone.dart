class HelpPhone {
  String name;
  String phone;

  HelpPhone.fromJson(Map json)
      : name = json['name'],
        phone = json['phone'];
}
