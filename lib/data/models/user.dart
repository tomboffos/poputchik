import 'package:poputchik/data/models/driver.dart';

class User {
  String name;
  String phone;
  String surname;
  bool notificationEnabled;
  Driver driver;
  dynamic id;
  dynamic avatar;
  dynamic rate;
  User.fromJson(Map json)
      : name = json['name'],
        phone = json['phone'],
        surname = json['surname'],
        notificationEnabled = json['notification_enabled'],
        driver =
            json['driver'] != null ? Driver.fromJson(json['driver']) : null,
        id = json['id'],
        avatar = json['avatar'],
        rate = json['rate'];
}
