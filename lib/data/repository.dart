import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:images_picker/images_picker.dart';
import 'package:poputchik/data/models/help_phone.dart';
import 'package:poputchik/data/models/order_request.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:poputchik/data/models/track.dart';
import 'package:poputchik/data/models/user.dart';
import 'package:poputchik/data/network_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'models/message.dart';
import 'models/order.dart';

class Repository {
  final NetworkService networkService;

  Repository({this.networkService});

  Future registerUser(Map<String, dynamic> form, context) async {
    return (await networkService.registerUserRequest(form, context));
  }

  Future checkCode(
      String code, User user, BuildContext context, bool driver) async {
    final response = await post(
        Uri.parse('${networkService.apiUrl}/user/check/${user.id}'),
        body: {'code': code},
        headers: {'Accept': 'application/json'});

    final body = jsonDecode(response.body);
    print(body);
    if (response.statusCode == 200) {
      (await SharedPreferences.getInstance())
          .setString('token', jsonDecode(response.body)['token']);
      if (driver) {
        Navigator.pushNamedAndRemoveUntil(
            context, '/driver/map/index', (route) => false);
      } else {
        Navigator.pushNamedAndRemoveUntil(
            context, '/user/home', (route) => false);
      }
    }

    if (response.statusCode == 400) {
      NetworkService.showErrorDialog(
          context: context, message: 'Не верный код');
    }
    if (response.statusCode == 422) {
      String errorMessage = '';
      jsonDecode(response.body)['errors']
          .forEach((key, value) => errorMessage += '$value \n');

      NetworkService.showErrorDialog(context: context, message: errorMessage);
    }
  }

  Future login({Map<String, dynamic> form, context, bool driver}) async {
    return (await networkService.loginUserRequest(
        form: form, context: context, driver: driver));
  }

  Future checkToken({context}) async {
    await networkService.checkUserToken(context: context);
  }

  Future<User> processUser() async {
    return User.fromJson(await networkService.requestUser());
  }

  Future<List<HelpPhone>> processHelpPhones() async {
    return (await networkService.fetchHelpPhones())
        .map((e) => HelpPhone.fromJson(e))
        .toList()
        .cast<HelpPhone>();
  }

  Future updateProcessUser({form}) async {
    await networkService.updateRequestUser(form: form);
  }

  Future<List<Track>> processTracks({query}) async {
    return (await networkService.fetchTracks(query: query))
        .map((value) => Track.fromJson(value))
        .toList()
        .cast<Track>();
  }

  Future processSaveOrderRequest({form, Track track, context}) async {
    print(form);
    return OrderRequest.fromJson(await networkService.fetchSaveOrderRequest(
        form: form, track: track, context: context));
  }

  Future<List<OrderRequest>> processRequests() async {
    return (await networkService.fetchOrderRequests())
        .map((e) => OrderRequest.fromJson(e))
        .toList()
        .cast<OrderRequest>();
  }

  Future cancelRequestOrder({orderId, context}) async {
    networkService.cancelOrderRequest(orderId: orderId, context: context);
  }

  Future registerDriver({context, form}) async {
    return await networkService.registerDriverRequest(
        context: context, form: form);
  }

  Future fillBalance({price, context}) async {
    await networkService.fillBalance(price: price, context: context);
  }

  Future updateDriverProcess({context, form}) async {
    await networkService.updateDriver(context: context, form: form);
  }

  Future createProcessorder({form}) async {
    return Order.fromJson(await networkService.storeOrder(form: form));
  }

  Future<List<Order>> processOrders({dates}) async {
    return (await networkService.fetchOrders(dates: dates))
        .map((value) => Order.fromJson(value))
        .toList()
        .cast<Order>();
  }

  Future<List<Message>> processMessages() async {
    return (await networkService.fetchMessages())
        .map((value) => Message.fromJson(value))
        .toList()
        .cast<Message>();
  }

  Future processSendMessage(List<Media> images, message) async {
    await networkService.sendRequestMessage(images, message);
  }

  void logoutUser({context}) async {
    (await SharedPreferences.getInstance()).remove('token');
    Navigator.pushNamedAndRemoveUntil(context, '/start', (route) => false);
  }

  becomeDriver({context, form}) async {
    await networkService.becomeDriverRequest(context: context, form: form);
  }

  Future getDirection(userLocation, destination) async {
    final response = await get(Uri.parse(
        'https://maps.googleapis.com/maps/api/directions/json?origin=${userLocation.latitude},${userLocation.longitude}&destination=${destination[0]},${destination[1]}&key=AIzaSyDn2ZI3eKd4mVbvhW87NWZs3kSs9sFDAIE'));
    print(response.body);
    final lines = PolylinePoints()
        .decodePolyline(jsonDecode(response.body)['routes'][0]
            ['overview_polyline']['points'])
        .toList()
        .map((e) => [e.latitude, e.longitude])
        .toList();

    return {
      'lines': lines,
      'bounds': [
        [
          jsonDecode(response.body)['routes'][0]['bounds']['northeast']['lat'],
          jsonDecode(response.body)['routes'][0]['bounds']['northeast']['lng']
        ],
        [
          jsonDecode(response.body)['routes'][0]['bounds']['southwest']['lat'],
          jsonDecode(response.body)['routes'][0]['bounds']['southwest']['lng']
        ]
      ]
    };
  }

  Future<Order> fetchCurrentOrder(Order order) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    final response = await get(
        Uri.parse('${networkService.apiUrl}/orders/${order.id}'),
        headers: {'Authorization': 'Bearer $token'});

    return Order.fromJson(jsonDecode(response.body));
  }

  updateRequest(OrderRequest orderRequest, form) async {
    final token = (await SharedPreferences.getInstance()).get('token');

    final response = await put(
        Uri.parse('${networkService.apiUrl}/order-requests/${orderRequest.id}'),
        body: form,
        headers: {'Authorization': 'Bearer $token'});
  }

  updateOrder(Order order, form) async {
    final token = (await SharedPreferences.getInstance()).get('token');

    final response = await put(
        Uri.parse('${networkService.apiUrl}/orders/${order.id}'),
        body: form,
        headers: {'Authorization': 'Bearer $token'});

    return Order.fromJson(jsonDecode(response.body));
  }

  Future fetchCurrentRequest(OrderRequest orderRequest) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    final response = await get(
        Uri.parse('${networkService.apiUrl}/order-requests/${orderRequest.id}'),
        headers: {'Authorization': 'Bearer $token'});

    return OrderRequest.fromJson(jsonDecode(response.body));
  }

  void rateOrder(int rating, modalContext, driverId) async {
    final token = (await SharedPreferences.getInstance()).get('token');

    final response = await post(
        Uri.parse('${networkService.apiUrl}/rate/$driverId'),
        headers: {'Authorization': 'Bearer $token'},
        body: {'rating': rating.toString()});
    print(response.body);
    if (response.statusCode == 200) {
      Navigator.pop(modalContext);
    }
  }

  Future cancelOrder({Order order}) async {
    final token = (await SharedPreferences.getInstance()).get('token');

    final response = await post(
      Uri.parse('${networkService.apiUrl}/cancel-order/${order.id}'),
      headers: {'Authorization': 'Bearer $token'},
    );
  }

  Future rateOrderUser(User user, int rate) async {
    final token = (await SharedPreferences.getInstance()).get('token');

    final response = await post(
        Uri.parse('${networkService.apiUrl}/rate-user/${user.id}'),
        body: {'rate': rate.toString()},
        headers: {'Authorization': 'Bearer $token'});
  }
}
