import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:images_picker/images_picker.dart';
import 'package:location/location.dart';
import 'package:poputchik/data/models/track.dart';
import 'package:poputchik/data/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NetworkService {
  String apiUrl = 'https://check.it-lead.net/api';

  Future<dynamic> registerUserRequest(
      Map<String, dynamic> form, context) async {
    final token = (await SharedPreferences.getInstance()).get('device_token');

    form['token'] = token != null ? token.toString() : 'sdsd';

    print(form['token']);
    final response = await post(Uri.parse('$apiUrl/user/register'),
        body: form, headers: {'Accept': 'application/json'});
    if (response.statusCode == 201) {
      // (await SharedPreferences.getInstance())
      //     .setString('token', jsonDecode(response.body)['token']);
      Navigator.pushNamed(context, '/user/check',
          arguments: User.fromJson(jsonDecode(response.body)['user']));
    }
    print(response.statusCode);
    if (response.statusCode == 422) {
      String errorMessage = '';
      jsonDecode(response.body)['errors']
          .forEach((key, value) => errorMessage += '$value \n');

      showErrorDialog(context: context, message: errorMessage);
    }
  }

  Future loginUserRequest(
      {Map<String, dynamic> form, context, bool driver}) async {
    final response = await post(Uri.parse('$apiUrl/user/login'),
        body: form, headers: {'Accept': 'application/json'});
    if (response.statusCode == 200) {
      (await SharedPreferences.getInstance())
          .setString('token', jsonDecode(response.body)['token']);
      if (driver != null && driver)
        Navigator.pushNamedAndRemoveUntil(
            context, '/driver/map/index', (route) => false);
      else
        Navigator.pushNamedAndRemoveUntil(
            context, '/user/home', (route) => false);
    }
    print(response.body);
    if (response.statusCode == 400)
      showErrorDialog(
          context: context, message: jsonDecode(response.body)['message']);

    if (response.statusCode == 422) {
      String errorMessage = '';
      jsonDecode(response.body)['errors']
          .forEach((key, value) => errorMessage += '$value \n');

      showErrorDialog(context: context, message: errorMessage);
    }
  }

  showSuccessMessage({context, message}) {
    AwesomeDialog(
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Успешно',
        desc: '$message',
        showCloseIcon: true)
      ..show();
  }

  static showErrorDialog({context, message}) {
    AwesomeDialog(
      context: context,
      showCloseIcon: true,
      dialogType: DialogType.ERROR,
      animType: AnimType.BOTTOMSLIDE,
      title: 'Ошибка',
      desc: '$message',
    )..show();
  }

  checkUserToken({context}) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    if (token == null) {
      Navigator.pushNamedAndRemoveUntil(context, '/start', (route) => false);
    } else {
      final response = (await get(Uri.parse('$apiUrl/user'), headers: {
        'Authorization': 'Bearer $token',
        'Accept': 'application/json'
      }));
      if (response.statusCode == 401)
        Navigator.pushNamedAndRemoveUntil(context, '/start', (route) => false);

      if (response.statusCode == 200) {
        final driver =
            (await SharedPreferences.getInstance()).getBool('driver');

        if (driver != null && driver)
          Navigator.pushNamedAndRemoveUntil(
              context, '/driver/map/index', (route) => false);
        else
          Navigator.pushNamedAndRemoveUntil(
              context, '/user/home', (route) => false);
      }
    }
  }

  Future<Map> requestUser() async {
    final token = (await SharedPreferences.getInstance()).get('token');
    if (token == null) {
      return {};
    } else {
      final response = (await get(Uri.parse('$apiUrl/user'), headers: {
        'Authorization': 'Bearer $token',
        'Accept': 'application/json'
      }));
      print(jsonDecode(response.body));

      if (response.statusCode == 401) {}

      if (response.statusCode == 200) return jsonDecode(response.body);
    }
  }

  Future<List> fetchHelpPhones() async {
    final token = (await SharedPreferences.getInstance()).get('token');
    if (token == null) return [];
    final response = (await get(Uri.parse('$apiUrl/help-phones'), headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    }));
    print(response.body);
    return jsonDecode(response.body).toList();
  }

  updateRequestUser({form}) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    if (form['avatar'] != null) {
      final request =
          MultipartRequest('POST', Uri.parse('$apiUrl/user/update'));
      request.files.add(
        MultipartFile('avatar', File(form['avatar']).readAsBytes().asStream(),
            File(form['avatar']).lengthSync(),
            filename: 'avatar-${DateTime.now()}'),
      );
      request.headers.addAll(
          {'Accept': 'application/json', 'Authorization': "Bearer $token"});

      final response = await request.send();
      print((await response.stream.bytesToString()));
    } else {
      final response = (await put(Uri.parse('$apiUrl/user/update'),
          body: form, headers: {'Authorization': 'Bearer $token'}));

      return response.body;
    }
  }

  Future fetchTracks({query}) async {
    final location = new Location();

    final locationData = await location.getLocation();
    final token = (await SharedPreferences.getInstance()).get('token');

    print(locationData);
    final response = (await get(
        Uri.parse(
            '$apiUrl/tracks/?start_name=$query&latitude=${locationData.latitude}&longitude=${locationData.longitude}'),
        headers: {
          'Authorization': 'Bearer $token',
          'Accept': 'application/json'
        }));

    print(response.body);
    return jsonDecode(response.body).toList();
  }

  Future fetchSaveOrderRequest({form, Track track, context}) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    if (token == null) return {};

    final response = (await post(Uri.parse('$apiUrl/order-requests'),
        headers: {
          'Authorization': 'Bearer $token',
          'Accept': 'application/json'
        },
        body: form));

    if (response.statusCode == 201) {
      print(response.body);
      return jsonDecode(response.body);
    }
    if (response.statusCode == 422) {
      String errorMessage = '';
      jsonDecode(response.body)['errors']
          .forEach((key, value) => errorMessage += '$value \n');

      showErrorDialog(context: context, message: errorMessage);
    }
  }

  Future<List<dynamic>> fetchOrderRequests() async {
    final token = (await SharedPreferences.getInstance()).get('token');
    if (token == null) return [];

    final response = (await get(Uri.parse('$apiUrl/order-requests'),
        headers: {'Authorization': 'Bearer $token'}));

    log(response.body);
    return jsonDecode(response.body)['data'].toList();
  }

  Future cancelOrderRequest({orderId, context}) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    if (token == null) return [];

    final response = (await put(Uri.parse('$apiUrl/order-requests/$orderId'),
        body: {
          'order_request_status_id': '4'
        },
        headers: {
          'Authorization': 'Bearer $token',
          'Accept': 'application/json'
        }));

    Navigator.pop(context);
  }

  Future registerDriverRequest({context, form}) async {
    final request =
        MultipartRequest('POST', Uri.parse('$apiUrl/driver/register'));
    form['driver_id_images'].forEach((image) {
      request.files.add(MultipartFile(
          'driver_id_images[]',
          File(image.path).readAsBytes().asStream(),
          File(image.path).lengthSync(),
          filename: '${DateTime.now().toString()}-driver_image'));
    });

    form['images'].forEach((image) {
      request.files.add(MultipartFile(
          'images[]',
          File(image.path).readAsBytes().asStream(),
          File(image.path).lengthSync(),
          filename: '${DateTime.now().toString()}-driver_image'));
    });

    final deviceToken =
        (await SharedPreferences.getInstance()).get('device_token');

    print(form['images']);
    request.fields.addAll({
      'name': form['name'],
      'password': form['password'],
      'surname': form['surname'],
      'phone': form['phone'],
      'car_color': form['car_color'],
      'car_year': form['car_year'],
      'car_model': form['car_model'],
      'iin': form['iin'],
      'seats': form['seats'],
      'car_id': form['car_id'],
      'token': deviceToken != null ? deviceToken.toString() : '',
      'email': form['email'],
    });

    request.headers.addAll({
      'Accept': 'application/json',
    });

    final response = await request.send();
    final body = await response.stream.bytesToString();
    print(body);
    if (response.statusCode == 201) {
      Navigator.pushNamed(context, '/driver/check',
          arguments: User.fromJson(jsonDecode(body)['user']));
    }

    if (response.statusCode == 422) {
      String errorMessage = '';
      jsonDecode(body)['errors']
          .forEach((key, value) => errorMessage += '$value \n');

      showErrorDialog(context: context, message: errorMessage);
    }
  }

  Future fillBalance({price, context}) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    final response = await post(Uri.parse('$apiUrl/balance'), headers: {
      'Authorization': 'Bearer $token',
      'Accept': 'application/json'
    }, body: {
      'balance': price.toString()
    });

    final link = jsonDecode(response.body);
    print(link);
    Navigator.pushNamed(context, '/payment',
        arguments: link['payment']['pg_redirect_url']);
  }

  updateDriver({context, form}) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    final response = await post(Uri.parse('$apiUrl/driver/update'),
        body: form,
        headers: {
          'Authorization': 'Bearer $token',
          'Accept': 'application/json'
        });

    print(response.body);
  }

  storeOrder({form}) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    if (token == null) return {};
    final response = await post(Uri.parse('$apiUrl/orders'),
        headers: {
          'Authorization': 'Bearer $token',
          'Accept': 'application/json'
        },
        body: form);
    print(response.body);
    return jsonDecode(response.body);
  }

  fetchOrders({dates}) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    if (token == null) return [];
    final response = await get(
      Uri.parse(
          '$apiUrl/orders${dates != null ? '?start_date=${dates[0].toString()}&end_date=${dates[1].toString()}' : ''}'),
      headers: {'Authorization': 'Bearer $token', 'Accept': 'application/json'},
    );
    print(response.body);
    return jsonDecode(response.body)['data'];
  }

  Future<List<dynamic>> fetchMessages() async {
    final token = (await SharedPreferences.getInstance()).get('token');
    if (token == null) return [];

    final response = await get(Uri.parse('$apiUrl/chat'), headers: {
      'Authorization': "Bearer $token",
      'Accept': 'application/json'
    });

    print(response.body);
    return jsonDecode(response.body)['data'];
  }

  sendRequestMessage(List<Media> images, message) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    if (token == null) return [];

    final request = MultipartRequest('POST', Uri.parse('$apiUrl/chat'));
    if (images != null)
      images.forEach((image) {
        request.files.add(MultipartFile(
            'message_content[images][]',
            File(image.path).readAsBytes().asStream(),
            File(image.path).lengthSync(),
            filename: '${DateTime.now().toString()}-driver_image'));
      });

    request.fields.addAll({'message_content[text]': message});

    request.headers.addAll(
        {'Authorization': 'Bearer $token', 'Accept': 'application/json'});

    final response = await request.send();

    final body = await response.stream.bytesToString();

    print(body);

    if (response.statusCode == 201) {
    } else {}
  }

  becomeDriverRequest({context, form}) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    final request =
        MultipartRequest('POST', Uri.parse('$apiUrl/driver/become'));
    form['driver_id_images'].forEach((image) {
      request.files.add(MultipartFile(
          'driver_id_images[]',
          File(image.path).readAsBytes().asStream(),
          File(image.path).lengthSync(),
          filename: '${DateTime.now().toString()}-driver_image'));
    });

    form['images'].forEach((image) {
      request.files.add(MultipartFile(
          'images[]',
          File(image.path).readAsBytes().asStream(),
          File(image.path).lengthSync(),
          filename: '${DateTime.now().toString()}-driver_image'));
    });

    final deviceToken =
        (await SharedPreferences.getInstance()).get('device_token');

    print(form['images']);
    request.fields.addAll({
      'car_color': form['car_color'],
      'car_year': form['car_year'],
      'car_model': form['car_model'],
      'iin': form['iin'],
      'seats': form['seats'],
      'car_id': form['car_id'],
      'token': deviceToken != null ? deviceToken.toString() : ''
    });

    request.headers.addAll(
        {'Accept': 'application/json', 'Authorization': "Bearer $token"});

    final response = await request.send();
    final body = await response.stream.bytesToString();
    print(body);
    if (response.statusCode == 200) {
      // (await SharedPreferences.getInstance())
      //     .setString('token', jsonDecode(body)['token']);
      Navigator.pushNamedAndRemoveUntil(
          context, '/driver/map/index', (route) => false);
    }

    if (response.statusCode == 422) {
      String errorMessage = '';
      jsonDecode(body)['errors']
          .forEach((key, value) => errorMessage += '$value \n');

      showErrorDialog(context: context, message: errorMessage);
    }
  }
}
