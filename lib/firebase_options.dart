// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    // ignore: missing_enum_constant_in_switch
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for macos - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
    }

    throw UnsupportedError(
      'DefaultFirebaseOptions are not supported for this platform.',
    );
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyCDjddD9fOJVPPKxXGHXuxpiy5p6SpAdF4',
    appId: '1:849880845715:web:250a428aa7efea56292c66',
    messagingSenderId: '849880845715',
    projectId: 'poputchic-1f1b2',
    authDomain: 'poputchic-1f1b2.firebaseapp.com',
    storageBucket: 'poputchic-1f1b2.appspot.com',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyB--OUbKnq4imRDf4Iuu-psut82avX-2Bg',
    appId: '1:849880845715:android:7a15800e6a2d67b8292c66',
    messagingSenderId: '849880845715',
    projectId: 'poputchic-1f1b2',
    storageBucket: 'poputchic-1f1b2.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyA0MsBb_aEVAGSX6Ph4OXgHwBj1iiewtPM',
    appId: '1:849880845715:ios:d800a64c87ef4517292c66',
    messagingSenderId: '849880845715',
    projectId: 'poputchic-1f1b2',
    storageBucket: 'poputchic-1f1b2.appspot.com',
    iosClientId: '849880845715-vjk5vetboqe2tc0esqf9525uvbuniecq.apps.googleusercontent.com',
    iosBundleId: 'kz.itLead.poputchik',
  );
}
