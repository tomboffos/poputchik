import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poputchik/cubit/app/cubit/app_cubit.dart';
import 'package:poputchik/data/service/notification.dart';
import 'package:poputchik/router.dart';
import 'package:screen_size_test/screen_size_test.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  FirebaseService().init();

  runApp(MyApp(
    router: AppRouter(),
  ));
}

class MyApp extends StatelessWidget {
  final AppRouter router;

  const MyApp({Key key, this.router}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AppCubit(),
      child: BlocBuilder<AppCubit, AppState>(
        builder: (context, state) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            builder: (context, child) => MediaQuery(
                data: MediaQuery.of(context).copyWith(
                  textScaleFactor: 1.0,
                ),
                child: child),
            title: 'Poputchik',
            theme: ThemeData(
              hintColor: Color(0xffBFDAF3),
              primarySwatch: Colors.blue,
              dividerColor: Color(0xffCBCBCB),
              errorColor: Color(0xffE22D2D),
              primaryColor: Color(0xff2DA1E2),
              selectedRowColor: Color(0xff424242),
              accentColor: Color(0xff482593),
              textTheme: TextTheme(
                headline5: GoogleFonts.montserrat(
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
                bodyText1: GoogleFonts.roboto(
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
                subtitle1: GoogleFonts.montserrat(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
            onGenerateRoute: router.generateRouter,
          );
        },
      ),
    );
  }
}
